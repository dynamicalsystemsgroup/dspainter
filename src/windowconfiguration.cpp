#include "windowconfiguration.h"
#include "ui_windowconfiguration.h"

windowConfiguration::windowConfiguration(uint32_t maxWindows, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::windowConfiguration)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::WindowStaysOnTopHint);
    this->setAttribute(Qt::WA_DeleteOnClose);
    this->setWindowTitle(tr("Set maximum window number."));
    ui->windowNumberBox->setValue(maxWindows);
}
windowConfiguration::~windowConfiguration()
{
    delete ui;
}
void windowConfiguration::on_setBtn_clicked()
{
    emit maxWindowsChanged(ui->windowNumberBox->value());
    this->close();
}
void windowConfiguration::on_cancelBtn_clicked()
{
    this->close();
}
