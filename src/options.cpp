#include "inc/options.h"
#include "ui_generated/ui_options.h"

Options::Options(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Options)
{
    ui->setupUi(this);
    this->setWindowTitle(tr("Options"));
    this->setAttribute(Qt::WA_DeleteOnClose);
    connect(ui->cancelBtn,SIGNAL(clicked()),this,SLOT(close()));
    read_settings();
}
Options::~Options()
{
    write_settings();
    delete ui;
}
void Options::read_settings(void)
{
    QSettings settings(QApplication::applicationDirPath() + "\\settings\\GeneralSettings.ini",
                       QSettings::IniFormat,
                       this);
    ui->libBuilderEdit->setText(settings.value("lib_builder_path", "").toString());
    ui->libEdit->setText(settings.value("library_path", "").toString());

}
void Options::write_settings(void)
{
    QSettings settings(QApplication::applicationDirPath() + "\\settings\\GeneralSettings.ini",
                       QSettings::IniFormat,
                       this);
    settings.setValue("lib_builder_path", ui->libBuilderEdit->text());
    settings.setValue("library_path", ui->libEdit->text());
}
void Options::on_OkBtn_clicked()
{
    emit generalSettingsChanged(GeneralSettings(ui->stepSB->value(),
                                                ui->maxTSB->value(),
                                                ui->maxValSB->value(),
                                                false,
                                                ui->intBufferSizeBox->value()
                                                ));
    emit pathSettingsChanged(pathSettings(ui->libEdit->text(),
                                          ui->libBuilderEdit->text()));
    this->close();
}
void Options::setGeneralSettings(const GeneralSettings &settings)
{
    ui->stepSB->setValue(settings.getIntegrationStep());
    ui->maxTSB->setValue(settings.getMaximumTime());
    ui->maxValSB->setValue(settings.getMaximumCoordinateValue());
}
void Options::setPathSettings(pathSettings set)
{
    ui->libEdit->setText(set.libraryPath);
    ui->libBuilderEdit->setText(set.libBuilderPath);
}
void Options::on_browseBtn_clicked()
{
    ui->libBuilderEdit->setText(
                QFileDialog::getOpenFileName(
                    0,tr("Change LibBuilder path"),
                    QApplication::applicationDirPath(),"*.exe"));
}
