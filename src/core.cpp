﻿#include "../inc/core.h"

Core::Core(QObject *parent) : QObject(parent)
{
    LOG(INFO)<<"Start logging...";
    qRegisterMetaType<std::vector<std::vector<double>>>("std::vector<std::vector<double>>&");
}
Core::~Core()
{
    this->closeOpenedWidgets();
    if(DS->isLoaded())
    {
        saveConfiguration();
    }
    // TODO: Check all modules are closed
    LOG(INFO)<<"End of logging session.";
}
void Core::saveConfiguration(void)
{
    LOG(INFO) << "Saving application configuration...";
    QList<WindowHolder*> list = graphicalArea->getWindowsList();
    std::vector<WindowSettings> windows;
    foreach (WindowHolder *wH, list)
    {
        windows.push_back(static_cast<TemplateWindow*>(wH->widget())->getSettings());
    }
    loader->save(DS->Coordinates(),parameterWidget->getParameterSettings(),windows);
    loader->saveGeneral(localGeneralSettings);
    loader->savePathSettings();
    LOG(INFO) << "Application configuration saved.";
}

// TODO: 1) resize graphical area while moving windows
void Core::InitializeCore(void)
{
    initMainWindow();
    initKeyboardHandler();
    initGraphicalArea();
    initSettingsLoader();
    initDynamicalSystem();
    initParameterWidget();
    initWindowSettingsDock();
    makeConnections();
    mainWindow->setGraphicalArea(graphicalArea);
    mainWindow->setParameterDockWidget(parameterWidget);
    mainWindow->showMaximized();
    graphicalArea->selectionChanged();
    DS->reload(loader->getLibraryPath());
    graphicalArea->setLockMode();
    autoSaveTimer.start(300000);// Autosave every 5 minuts
    LOG(INFO)<<"Initialization finished.";
}
void Core::initWindowSettingsDock(void)
{
    windowSettingsDock = new CollapsibleDockWidget(QString("Settings"));
    windowSettingsWidget = new WindowSettingsWidget(windowSettingsDock);
    mainWindow->setWindowSettingsDockWidget(windowSettingsDock);
    windowSettingsDock->setNewWidget(windowSettingsWidget);
    windowSettingsDock->show();
    windowSettingsDock->hideWidget();
}
void Core::initParameterWidget(void)
{
    parameterWidget = new ParameterWidget(std::vector<QString>());
    LOG(INFO)<<"Parameter widget initialized.";
}
void Core::initSettingsLoader(void)
{
    loader = new SettingsLoader();
    loader->loadPathSettings();
    loader->loadGeneral();
    LOG(INFO)<<"Settings loader initialized.";
}
void Core::initMainWindow(void)
{
    mainWindow = new DSPainter();
    LOG(INFO)<<"Main window initialized.";
    mainWindow->show();
}
void Core::initKeyboardHandler(void)
{
    keyboard = new KeyboardHandler();
    LOG(INFO)<<"Keyboard handler initialized.";
}
void Core::initGraphicalArea(void)
{
    graphicalArea = new GraphicalAreaNS::GraphicalArea();
    LOG(INFO)<<"Graphical area initialized.";
}
void Core::initDynamicalSystem(void)
{
    DS = new DynamicalSystem();
    LOG(INFO)<<"Dynamical system initialized.";
}
void Core::showEqullibriumFinder(void)
{
    if(!DS->isLoaded())
    {
        LOG(WARNING)<<"Trying to start Equillibrium finder with no DS loaded.";
        return;
    }
    if(openedWidgets.contains(FloatingWidgetsContainer(EQ_WIDGET,equllibriumFinder)))
    {
        equllibriumFinder->activateWindow();
        equllibriumFinder->raise();// To be sure, that widget is on top
        LOG(INFO)<<"Core: EqFinder set on top.";
        return;
    }
    equllibriumFinder = new EqFinder(DS->Dimension(),DS->Coordinates(),
                                     DS->CoordinateIDs(),DS);
    openedWidgets<<FloatingWidgetsContainer(EQ_WIDGET,equllibriumFinder);
    connect(equllibriumFinder,&QObject::destroyed,[this](){
        openedWidgets.removeOne(FloatingWidgetsContainer(EQ_WIDGET,equllibriumFinder));
        LOG(INFO)<<"Core: EqFinder removed.";
    });
    connect(equllibriumFinder,SIGNAL(keyPressed(QKeyEvent*)),
            keyboard,SLOT(processPressedKey(QKeyEvent*)));
    connect(equllibriumFinder,&EqFinder::newSingleCoordinates,[this](specialTrajectoryEntry entry){
        if(entry.coordinates.size() != 2*DS->Dimension() + 1)
        {
            LOG(WARNING)<<"Core: set special initial conditions dimension mismatch!";
        }
        DS->setCoordinates(entry.coordinates);
        if(entry.typeOfTime == typeTime::DIRECT)
        {
            DS->startIntegrating(entry.step,entry.integratingTime);
        }
        else
        {
            DS->startIntegrating(-entry.step,entry.integratingTime);
        }
    });
    connect(equllibriumFinder,&EqFinder::newSetOfCoordinates,[this](std::vector<specialTrajectoryEntry> newSet){
        handleMultipleInitialConditions(newSet);
    });
    equllibriumFinder->show();
    LOG(INFO)<<"Equillibrium finder created.";
}
void Core::showICHandler(void)
{
    if(!DS->isLoaded())
    {
        LOG(WARNING)<<"Trying to start ICHandler with no DS loaded.";
        return;
    }
    if(openedWidgets.contains(FloatingWidgetsContainer(IC_WIDGET,initialConditionsHandler)))
    {
        initialConditionsHandler->activateWindow();
        initialConditionsHandler->raise();// To be sure, that widget is on top
        LOG(INFO)<<"Core: ICHandler set on top.";
        return;
    }
    initialConditionsHandler = new ICHandler(DS->Dimension(),DS->Coordinates(),
                                             DS->CoordinateIDs());
    openedWidgets<<FloatingWidgetsContainer(IC_WIDGET,initialConditionsHandler);
    connect(initialConditionsHandler,&ICHandler::icChanged,[this](std::vector<double> newCoordinates){
        if(newCoordinates.size() != 2*DS->Dimension() + 1){
            LOG(WARNING)<<"Core: wrong size of coordinate array in icChanged handler.";
        }
        DS->setCoordinates(newCoordinates);
    });
//    connect(initialConditionsHandler,&ICHandler::icFixed,[this](bool fixed){
//        if(fixed) qDebug()<<"icFixed = true";
//        else qDebug()<<"icFixed = false";
//    });
    connect(initialConditionsHandler,&QObject::destroyed,[this](){
        openedWidgets.removeOne(FloatingWidgetsContainer(IC_WIDGET,initialConditionsHandler));
        LOG(INFO)<<"Core: ICHandler removed.";
    });
    initialConditionsHandler->show();
    LOG(INFO)<<"Initial conditions handler created.";
}
void Core::showSetParameterWidget(void)
{
    if(!DS->isLoaded())
    {
        LOG(WARNING)<<"Trying to start ParameterWidget with no DS loaded.";
        return;
    }
    if(openedWidgets.contains(FloatingWidgetsContainer(PARAM_WIDGET,setParametersWidget)))
    {
        setParametersWidget->activateWindow();
        setParametersWidget->raise();// To be sure, that widget is on top
        LOG(INFO)<<"Core: ParameterWidget set on top.";
        return;
    }
    setParametersWidget = new SetParametersWidget(DS->ParamDimension(),
                                                  DS->Parameters(),
                                                  DS->ParameterIDs());
    openedWidgets<<FloatingWidgetsContainer(PARAM_WIDGET,setParametersWidget);
    connect(setParametersWidget,&SetParametersWidget::parametersChanged,[this](std::vector<double> newParameters)
    {
        parameterWidget->setParameters(newParameters);
        LOG(INFO)<<"Parameter widget: parameter changed slot activated.";
    });
    connect(setParametersWidget,&QObject::destroyed,[this](){
        openedWidgets.removeOne(FloatingWidgetsContainer(PARAM_WIDGET,setParametersWidget));
        LOG(INFO)<<"Core: SetParameters removed.";
    });
    setParametersWidget->show();
    LOG(INFO)<<"SetParametersWidget created.";
}
void Core::showOptionsWidget(void)
{
//    if(!DS->isLoaded())
//    {
//        LOG(WARNING)<<"showOptionsWidget(): no DS loaded.";
//        return;
//    }
    if(openedWidgets.contains(FloatingWidgetsContainer(OPTIONS_WIDGET,optionsWidget)))
    {
        optionsWidget->activateWindow();
        optionsWidget->raise();// To be sure, that widget is on top
        LOG(INFO)<<"Core: OptionsWidget set on top.";
        return;
    }
    optionsWidget = new Options();
    openedWidgets << FloatingWidgetsContainer(OPTIONS_WIDGET, optionsWidget);
    connect(optionsWidget,&QObject::destroyed,[this](){
        openedWidgets.removeOne(FloatingWidgetsContainer(OPTIONS_WIDGET, optionsWidget));
        LOG(INFO)<<"Core: SetParameters removed.";
    });
    connect(optionsWidget, &Options::pathSettingsChanged, [this](const pathSettings& settings){
        this->loader->set_path_settings(settings);
    });
    optionsWidget->show();
    LOG(INFO)<<"OptionsWidget created.";
}
void Core::closeOpenedWidgets(void)
{
    foreach (FloatingWidgetsContainer container, openedWidgets) {
        container.getWidget()->close();
    }
    openedWidgets.clear();
    LOG(INFO)<<"All floating widgets are closed.";
}
void Core::makeConnections(void)
{
    // Autosave timer
    connect(&autoSaveTimer,&QTimer::timeout,[this](void)
    {
        if(DS->isLoaded())
        {
            QLabel label;
            label.setText(tr("Autosaving parameters..."));
            label.show();
            saveConfiguration();
        }
    });
    // main window group
    connect(mainWindow,SIGNAL(keyPressed(QKeyEvent*)),keyboard,SLOT(processPressedKey(QKeyEvent*)));
    connect(mainWindow,SIGNAL(newWindowModeTriggered()),graphicalArea,SLOT(setNewWindowMode()));
    connect(mainWindow,SIGNAL(editModeTriggered()),graphicalArea,SLOT(setEditMode()));
    connect(mainWindow,SIGNAL(lockModeTriggered()),graphicalArea,SLOT(setLockMode()));
    connect(mainWindow,SIGNAL(alignLeftTriggered()),graphicalArea,SLOT(alignLeft()));
    connect(mainWindow,SIGNAL(alignTopTriggered()),graphicalArea,SLOT(alignTop()));
    connect(mainWindow,SIGNAL(deleteSelectedTriggered()),graphicalArea,SLOT(deleteSelected()));
    connect(mainWindow,SIGNAL(closeAllWindowsTriggered()),graphicalArea,SLOT(closeAllWindows()));
    connect(mainWindow,SIGNAL(equillibriumFinderTriggered()),this,SLOT(showEqullibriumFinder()));
    connect(mainWindow,SIGNAL(setPhaseCoordinatesTriggered()),this,SLOT(showICHandler()));
    connect(mainWindow,SIGNAL(showOptionsTriggered()),this,SLOT(showOptionsWidget()));
    connect(mainWindow,&DSPainter::createColormapTriggered, [this](){
        ColormapDrawer *cm_drawer = new ColormapDrawer(this->DS);
        cm_drawer->show();
        LOG(INFO) << "Colormap drawer created.";
    });
    connect(mainWindow, &DSPainter::createWaveDrawerTriggered, [this](){
        WaveDrawer *wave_drawer = new WaveDrawer(this->DS);
        connect(DS, SIGNAL(bufferCollected(std::vector<std::vector<double> >)),
                wave_drawer, SLOT(draw(std::vector<std::vector<double> >)));
        wave_drawer->show();
        LOG(INFO) << "Wave drawer created.";
    });
    connect(mainWindow,&DSPainter::startIntegrationTriggered,[this](){
        graphicalArea->init_before_drawing(DS->Coordinates());
        DS->stopIntegrating();
        DS->startIntegrating(localGeneralSettings.getIntegrationStep(),
                             localGeneralSettings.getMaximumTime());
    });
    connect(mainWindow,&DSPainter::stopIntegrationTriggered,[this](){
        DS->stopIntegrating();
    });
    connect(mainWindow,&DSPainter::loadNewLibraryTriggered,[this](){
        if(DS->isLoaded()){
            QList<WindowHolder*> list = graphicalArea->getWindowsList();
            std::vector<WindowSettings> windows;
            foreach (WindowHolder *wH, list) {
                windows.push_back(static_cast<TemplateWindow*>(wH->widget())->getSettings());
            }
            loader->save(DS->Coordinates(),parameterWidget->getParameterSettings(),windows);
            loader->saveGeneral(localGeneralSettings);
        }
        QString applicationDir = QApplication::applicationDirPath();
        QString newLibraryName = QFileDialog::getOpenFileName(0,tr("Select library *.dll"),
                                                              applicationDir,"*.dll");
        if(newLibraryName.isEmpty()) return;
        graphicalArea->closeAllWindows();
        closeOpenedWidgets();
        loader->setLibraryPath(newLibraryName);
        DS->reload(newLibraryName);
    });
    connect(mainWindow,&DSPainter::setParametersTriggered,[this](){
        if(DS->isLoaded()) showSetParameterWidget();
    });
    connect(mainWindow, &DSPainter::loadLibraryBuilderTriggered, [this]() {
        LOG(INFO) << "Starting new process for lib_builder";
        LOG(INFO) << QString("Library path = " + loader->getLibraryPath()).toStdString();
        if(QProcess::startDetached(loader->get_path_settings().libBuilderPath,
                                   (QStringList() << DS->SystemName())))
        {
            LOG(INFO) << "lib_builder process is successfully started and detached.";
        }
        else
        {
            LOG(INFO) << "lib_builder process cannot be started.";
        }
    });

    // keyboard group
    connect(keyboard,SIGNAL(deleteSelectedWindows()),graphicalArea,SLOT(deleteSelected()));
    connect(keyboard,SIGNAL(alignLeft()),graphicalArea,SLOT(alignLeft()));
    connect(keyboard,SIGNAL(alignTop()),graphicalArea,SLOT(alignTop()));
    connect(keyboard,SIGNAL(dublicate()),graphicalArea,SLOT(dublicateSelectedWindow()));
    connect(keyboard,SIGNAL(reset()),graphicalArea,SLOT(reset()));
    connect(keyboard,&KeyboardHandler::reset,[this](){
        DS->setNullTime();
    });
    connect(keyboard,SIGNAL(scale()),graphicalArea,SLOT(rescale()));
    connect(keyboard,&KeyboardHandler::scale,[this] () {
        DS->setNullTime();
    });
    connect(keyboard,&KeyboardHandler::showMaximized,[this] () {
        mainWindow->showFullScreen();
        LOG(INFO)<<"Application showed fullscreen.";
    });
    connect(keyboard,&KeyboardHandler::cancel,[this] () {
        mainWindow->showMaximized();
        LOG(INFO)<<"Application showed maximized.";
    });
    connect(keyboard, &KeyboardHandler::parameter1ShiftUp, [this] () {
        parameterWidget->shiftParameter(ParameterWidget::First, ParameterWidget::Positive);
        qDebug()<<"Parameter 1 shift up hotkey handled";
    });
    connect(keyboard, &KeyboardHandler::parameter1ShiftDown, [this] () {
        parameterWidget->shiftParameter(ParameterWidget::First, ParameterWidget::Negative);
        qDebug()<<"Parameter 1 shift down hotkey handled";
    });
    connect(keyboard, &KeyboardHandler::parameter1StepShiftUp, [this] () {
        parameterWidget->multiplyStep(ParameterWidget::First, ParameterWidget::Positive);
        qDebug()<<"Parameter 1 step shift up hotkey handled";
    });
    connect(keyboard, &KeyboardHandler::parameter1StepShiftDown, [this] () {
        parameterWidget->multiplyStep(ParameterWidget::First, ParameterWidget::Negative);
        qDebug()<<"Parameter 1 step shift down hotkey handled";
    });
    connect(keyboard, &KeyboardHandler::changeParameter1, [this] () {
        parameterWidget->incrementActiveParameter(ParameterWidget::First);
        qDebug()<<"Parameter 1 number increment hotkey handled";
    });
    connect(keyboard, &KeyboardHandler::parameter2ShiftUp, [this] () {
        parameterWidget->shiftParameter(ParameterWidget::Second, ParameterWidget::Positive);
        qDebug()<<"Parameter 2 shift up hotkey handled";
    });
    connect(keyboard, &KeyboardHandler::parameter2ShiftDown, [this] () {
        parameterWidget->shiftParameter(ParameterWidget::Second, ParameterWidget::Negative);
        qDebug()<<"Parameter 2 shift down hotkey handled";
    });
    connect(keyboard, &KeyboardHandler::parameter2StepShiftUp, [this] () {
        parameterWidget->multiplyStep(ParameterWidget::Second, ParameterWidget::Positive);
        qDebug()<<"Parameter 2 step shift up hotkey handled";
    });
    connect(keyboard, &KeyboardHandler::parameter2StepShiftDown, [this] () {
        parameterWidget->multiplyStep(ParameterWidget::Second, ParameterWidget::Negative);
        qDebug()<<"Parameter 2 step shift down hotkey handled";
    });
    connect(keyboard, &KeyboardHandler::changeParameter2, [this] () {
        parameterWidget->incrementActiveParameter(ParameterWidget::Second);
        qDebug()<<"Parameter 2 number increment hotkey handled";
    });
    connect(keyboard, &KeyboardHandler::makeNewArchiveEntry, [this]() {
        archiveWidget = new Archive();
        archiveWidget->show();
    });

    // graphical area group
    connect(graphicalArea,SIGNAL(graphicalModeChanged(GraphicalAreaNS::graphicalMode)),mainWindow,SLOT(changeGraphicalMode(GraphicalAreaNS::graphicalMode)));
    connect(graphicalArea,SIGNAL(keyPressed(QKeyEvent*)),keyboard,SLOT(processPressedKey(QKeyEvent*)));
    connect(graphicalArea,SIGNAL(paintFinished()),DS,SLOT(resume()));
    connect(graphicalArea,&GraphicalAreaNS::GraphicalArea::selectionChanged,[this](){
        if(graphicalArea->getSelectionList().size() != 1)
        {
            windowSettingsWidget->setEnabled(false);
        }
        else
        {
            if(graphicalArea->getSelectionList().last()->WindowType() == noType)
            {
                return;
            }
            windowSettingsWidget->setEnabled(true);
            windowSettingsWidget->setSettings(
                        static_cast<TemplateWindow*>(
                            graphicalArea->getSelectionList().last()->widget())->getSettings()
                        );
            if(graphicalArea->getSelectionList().last()->WindowType() == cascadeDrawer)
            {
                ;
            }
        }
    });
    // dynamical system
    connect(DS,SIGNAL(loaded(bool)),mainWindow,SLOT(reconfigureActions(bool)));
    connect(DS,&DynamicalSystem::loaded,[this](bool loaded){
        parameterWidget->setEnabled(loaded);
        if(loaded == true)
        {
            commonFunctions::set_dynamical_system(DS);
            graphicalArea->setDSProperties(DS->Dimension(),DS->CoordinateIDs());
            parameterWidget->setParameterNames(DS->ParameterIDs());
            windowSettingsWidget->config(DS->Dimension(),DS->CoordinateIDs());
            loader->configureLoader(DS->Dimension(),DS->ParamDimension());
            loader->load(DS->SystemName());
            DS->setParameters(loader->getParameters());
            DS->setCoordinates(loader->getCoordinates());
            parameterWidget->setParameters(loader->getParameters());
            parameterWidget->setSettings(loader->getParameterSettings());
            localGeneralSettings = loader->getGeneralSettings();
            foreach (WindowSettings settings, loader->getWindows())
            {
                graphicalArea->createWindow(settings);
            }
        }
    });
    connect(DS,SIGNAL(operationStatusChanged(bool)),mainWindow,SLOT(reconfigStartStopIcons(bool)));
    connect(DS,SIGNAL(bufferCollected(std::vector<std::vector<double> > &)),
            graphicalArea,SLOT(drawBuffer(std::vector<std::vector<double> > &)));
    // Parameter widget
    connect(parameterWidget,&ParameterWidget::valueChanged,[this](uint32_t number, double value){
        if(DS->isLoaded())
        {
            DS->setParameter(number,value);
        }
    });
    // Collapsible Dock Widget
    connect(windowSettingsDock,SIGNAL(keyPressed(QKeyEvent*)),keyboard,SLOT(processPressedKey(QKeyEvent*)));
    // Window Settings Widget
    connect(windowSettingsWidget,SIGNAL(keyPressed(QKeyEvent*)),keyboard,SLOT(processPressedKey(QKeyEvent*)));
    connect(windowSettingsWidget,&WindowSettingsWidget::settingsChanged,[this](WindowSettings settings){
        if(graphicalArea->getSelectionList().isEmpty() == false)
        {
            static_cast<TemplateWindow*>(graphicalArea->getSelectionList().last()->widget())->setSettings(settings);
        }
    });
    connect(windowSettingsWidget, &WindowSettingsWidget::secant_changed, [this](std::vector<double> secant)
    {
        if(graphicalArea->getSelectionList().empty()) return;
        WindowHolder *w = static_cast<WindowHolder*>(graphicalArea->getSelectionList().last());
        if(w->WindowType() == cascadeDrawer)
        {
            static_cast<PoincarePainter*>(graphicalArea->getSelectionList().last()->widget())->set_secant(secant);
        }
    });
    connect(windowSettingsWidget, &WindowSettingsWidget::periodic_index_changed, [this](int number){
        if(graphicalArea->getSelectionList().empty()) return;
        WindowHolder *w = static_cast<WindowHolder*>(graphicalArea->getSelectionList().last());
        if(w->WindowType() == cascadeDrawer)
        {
            static_cast<PoincarePainter*>(graphicalArea->getSelectionList().last()->widget())->set_periodic_index(number);
        }
    });
    // Options
}
void Core::handleMultipleInitialConditions(std::vector<specialTrajectoryEntry> &newSet)
{
    QProgressDialog dialog("Processing data...", QString(), 0, newSet.size());
    dialog.show();
    uint32_t entryCounter = 0;
    std::vector<std::vector<double>> buffer;
    if(newSet.size() == 0) {
        LOG(WARNING) << "Core::handleMultipleInitialConditions : set is empty.";
        return;
    }
    std::for_each(newSet.begin(), newSet.end(), [&, this](specialTrajectoryEntry entry){
        if(entry.typeOfTime == DIRECT) {
            buffer = DS->getIntegralCurve(entry.coordinates, entry.integratingTime, entry.step);
        } else {
            buffer = DS->getIntegralCurve(entry.coordinates, entry.integratingTime, -entry.step);
        }
        graphicalArea->drawBuffer(buffer);
        entryCounter++;
        dialog.setValue(entryCounter);
        QApplication::processEvents();
    });
}
