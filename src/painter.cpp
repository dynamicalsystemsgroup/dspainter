#include "../inc/painter.h"
Painter::Painter(uint32_t newDimension,
                 std::vector<QString> newCoordinateIDs,
                 QWidget *parent):TemplateWindow(newDimension, newCoordinateIDs,parent){
    this->initialize();
    LOG(INFO)<<"FlowDrawer window created successfully.";
}
void Painter::initialize(void)
{
    resetAttach = true;
    isRunningX = false;
    isRunningY = false;
    settings.setScaleX(0,-10);
    settings.setScaleY(0,-10);
    settings.setScaleX(1,10);
    settings.setScaleY(1,10);
    settings.setRangeX(settings.getScalesX());
    settings.setRangeY(settings.getScalesY());
    settings.setPeriodicRangeX(settings.getRangeX());
    settings.setPeriodicRangeY(settings.getRangeY());
    settings.setMultiplier(0,(this->getImage()->width())/
                           fabs(settings.getScalesX()[0]-settings.getScalesX()[1]));
    settings.setMultiplier(1,(this->getImage()->height())/
                           fabs(settings.getScalesY()[0]-settings.getScalesY()[1]));
}
void Painter::doscalePrivate(void)
{
    reset();
    this->updateMultipliers();
    this->updateLabels();    
    settings.setScaleX(0,processPhaseH(oldCoordinates[settings.getCoordinateNumbers()[0]]) - 1e-6);
    settings.setScaleX(1,processPhaseH(oldCoordinates[settings.getCoordinateNumbers()[0]]) + 1e-6);
    settings.setScaleY(0,processPhaseV(oldCoordinates[settings.getCoordinateNumbers()[1]]) - 1e-6);
    settings.setScaleY(1,processPhaseV(oldCoordinates[settings.getCoordinateNumbers()[1]]) + 1e-6);
}
void Painter::doscale(void)
{
    updateRange();
    doscalePrivate();
}
void Painter::draw(const std::vector<std::vector<double> > &buffer)
{
    if(buffer.empty())
    {
        emit paintFinished();
        return;
    }
    std::vector<double> tempScale;
    tempScale.assign(2,0);
    oldCoordinates = buffer[0];
    previous.rx() = settings.getMultipliers()[0]*(processPhaseH(buffer[0][settings.getCoordinateNumbers()[0]])
            + settings.getScaleFactors()[0]) + this->getImage()->width()/2;
    previous.ry() = -settings.getMultipliers()[1]*(processPhaseV(buffer[0][settings.getCoordinateNumbers()[1]])
            + settings.getScaleFactors()[1]) + this->getImage()->height()/2;
    for(uint32_t i = 1; i < buffer.size(); ++i)
    {
        uint32_t boundaryState = 0;
        QPointF temp(0,0);
        // Window bounds intersection check
        if(settings.getHorizontalProcessing() == "Periodic")
        {
            double integerPartBefore,integerPartAfter;
            if(buffer[i][settings.getCoordinateNumbers()[0]] > 0)
            {
                modf((oldCoordinates[settings.getCoordinateNumbers()[0]] - settings.getRangeX()[0])/(settings.getHorizontalPeriod()),&integerPartBefore);
                modf((buffer[i][settings.getCoordinateNumbers()[0]] - settings.getRangeX()[0])/(settings.getHorizontalPeriod()),&integerPartAfter);
            }
            else
            {
                modf((oldCoordinates[settings.getCoordinateNumbers()[0]] + settings.getRangeX()[0])/(settings.getHorizontalPeriod()),&integerPartBefore);
                modf((buffer[i][settings.getCoordinateNumbers()[0]] + settings.getRangeX()[0])/(settings.getHorizontalPeriod()),&integerPartAfter);
            }
            if(integerPartAfter > integerPartBefore) boundaryState |= H_RIGHT;
            else if(integerPartAfter < integerPartBefore) boundaryState |= H_LEFT;
        }
        if(settings.getVerticalProcessing() == "Periodic")
        {
            double integerPartBefore,integerPartAfter;
            if(buffer[i][settings.getCoordinateNumbers()[1]] > 0)
            {
                modf((oldCoordinates[settings.getCoordinateNumbers()[1]] - settings.getRangeY()[0])/(settings.getVerticalPeriod()),&integerPartBefore);
                modf((buffer[i][settings.getCoordinateNumbers()[1]] - settings.getRangeY()[0])/(settings.getVerticalPeriod()),&integerPartAfter);
            }
            else
            {
                modf((oldCoordinates[settings.getCoordinateNumbers()[1]] + settings.getRangeY()[0])/(settings.getVerticalPeriod()),&integerPartBefore);
                modf((buffer[i][settings.getCoordinateNumbers()[1]] + settings.getRangeY()[0])/(settings.getVerticalPeriod()),&integerPartAfter);
            }
            if(integerPartAfter > integerPartBefore) boundaryState |= V_UPPER;
            else if(integerPartAfter < integerPartBefore) boundaryState |= V_BOTTOM;
        }
        if( (boundaryState & H_RIGHT) || (boundaryState & H_LEFT) )
        {
            this->setRunningX(true);
        }
        else this->setRunningX(false);
        if( (boundaryState & V_UPPER) || (boundaryState & V_BOTTOM) )
        {
            this->setRunningY(true);
        }
        else this->setRunningY(false);
        current.rx() = settings.getMultipliers()[0]*(processPhaseH(buffer[i][settings.getCoordinateNumbers()[0]]) + settings.getScaleFactors()[0]) + this->getImage()->width()/2;
        current.ry() = -settings.getMultipliers()[1]*(processPhaseV(buffer[i][settings.getCoordinateNumbers()[1]]) + settings.getScaleFactors()[1]) + this->getImage()->height()/2;
        if(!resetAttach) resetAttach = true;
        else if( (boundaryState == NO_INT) )
        {
            drawLine(previous,current);// no intersection - just draw
        }
        else
        {
            // Cases when 2 bounds are intersected simultaniously
            if(boundaryState == (H_RIGHT | V_UPPER))
            {
                temp.rx() = this->getImage()->width();
                temp.ry() = 0;
                drawLine(previous,temp);
                temp.rx() = 0;
                temp.ry() = this->getImage()->height();
                drawLine(temp,current);
            }
            else if(boundaryState == (H_LEFT|V_UPPER))
            {
                temp.rx() = 0;
                temp.ry() = 0;
                drawLine(previous,temp);
                temp.rx() = this->getImage()->width();
                temp.rx() = this->getImage()->height();
                drawLine(temp,current);
            }
            else if(boundaryState == (H_RIGHT|V_BOTTOM))
            {
                temp.rx() = this->getImage()->width();
                temp.ry() = this->getImage()->height();
                drawLine(previous,temp);
                temp.rx() = 0;
                temp.ry() = 0;
                drawLine(temp,current);
            }
            else if(boundaryState == (H_LEFT|V_BOTTOM))
            {
                temp.rx() = 0;
                temp.ry() = this->getImage()->height();
                drawLine(previous,temp);
                temp.rx() = 0;
                temp.ry() = this->getImage()->width();
                drawLine(temp,current);
            }
            else//Only one boundary intersected
            {
                if(boundaryState == H_RIGHT)
                {
                    temp.rx() = this->getImage()->width();
                    temp.ry() = (temp.rx() - previous.rx())/(current.rx() + this->getImage()->width() - previous.rx())*
                            (current.ry() - previous.ry()) + previous.ry();
                    drawLine(previous,temp);
                    temp.rx() = 0;
                    drawLine(temp,current);
                }
                if(boundaryState == H_LEFT)
                {
                    temp.rx() = 0;
                    temp.ry() = (temp.rx() - previous.rx())/(current.rx() - this->getImage()->width() - previous.rx())*
                            (current.ry() - previous.ry()) + previous.ry();
                    drawLine(previous,temp);
                    temp.rx() = this->getImage()->width();
                    drawLine(temp,current);
                }
                if(boundaryState == V_UPPER)
                {
                    temp.ry() = 0;
                    temp.rx() = (temp.ry() - previous.ry())/(current.ry() - this->getImage()->height() - previous.ry())*
                            (current.rx() - previous.rx()) + previous.rx();
                    drawLine(previous,temp);
                    temp.ry() = this->getImage()->height();
                    drawLine(temp,current);
                }
                if(boundaryState == V_BOTTOM)
                {
                    temp.ry() = this->getImage()->height();
                    temp.rx() = (temp.ry() - previous.ry())/(current.ry() + this->getImage()->height() - previous.ry())*
                            (current.rx() - previous.rx()) + previous.rx();
                    drawLine(previous,temp);
                    temp.ry() = 0;
                    drawLine(temp,current);
                }
            }
        }
        // Update max and min ranges
        tempScale[0] = processPhaseH(buffer[i][settings.getCoordinateNumbers()[0]]);
        tempScale[1] = processPhaseV(buffer[i][settings.getCoordinateNumbers()[1]]);
        updateScales(tempScale);
        previous = current;
        oldCoordinates = buffer[i];
    }
    emit paintFinished();
}
void Painter::updateScales(std::vector<double> scales)
{
    if(settings.getScalesX()[1] < scales[0])
        settings.setScaleX(1,scales[0]);
    if(settings.getScalesX()[0] > scales[0])
        settings.setScaleX(0,scales[0]);
    if(settings.getScalesY()[1] < scales[1])
        settings.setScaleY(1,scales[1]);
    if(settings.getScalesY()[0] > scales[1])
        settings.setScaleY(0,scales[1]);
}
void Painter::updateLabels(void)
{
    ui->xLabel->setText(coordinateIDs[settings.getCoordinateNumbers()[0]]);
    ui->yLabel->setText(coordinateIDs[settings.getCoordinateNumbers()[1]]);
    if(settings.getHorizontalProcessing() == "Periodic")
    {
        ui->hMin->setText(QString::number(settings.getPeriodicRangeX()[0],'f',3));
        ui->hMax->setText(QString::number(settings.getPeriodicRangeX()[1],'f',3));
    }
    else
    {
        ui->hMin->setText(QString::number(settings.getRangeX()[0],'f',3));
        ui->hMax->setText(QString::number(settings.getRangeX()[1],'f',3));
    }
    if(settings.getVerticalProcessing() == "Periodic")
    {
        ui->vMin->setText(QString::number(settings.getPeriodicRangeY()[0],'f',3));
        ui->vMax->setText(QString::number(settings.getPeriodicRangeY()[1],'f',3));
    }
    else
    {
        ui->vMin->setText(QString::number(settings.getRangeY()[0],'f',3));
        ui->vMax->setText(QString::number(settings.getRangeY()[1],'f',3));
    }
    this->update();
}
void Painter::updateMultipliers(void)
{
    //Horizontal
    if(settings.getCoordinateNumbers()[0] == 2*dimension)
    {//If Time - simply update multipliers
        settings.setMultiplier(0,(this->getImage()->width())/fabs(settings.getRangeX()[1] - settings.getRangeX()[0]));
        settings.setScaleFactor(0,-(settings.getRangeX()[1] + settings.getRangeX()[0])/2);
    }
    else
    {
        if(settings.getHorizontalProcessing() == "Periodic")
        {
            if((fabs(settings.getScalesX()[1]-settings.getScalesX()[0])>1e-8)){
                if(fabs(settings.getPeriodicRangeX()[1] - settings.getPeriodicRangeX()[0]) > 0)
                {
                    settings.setMultiplier(0,(this->getImage()->width())/fabs(settings.getPeriodicRangeX()[1] - settings.getPeriodicRangeX()[0]));
                }
                else
                {
                    settings.setMultiplier(0,1);
                    LOG(WARNING)<<"Horizontal multiplicator tends to infinity";
                }
                settings.setScaleFactor(0,-(settings.getPeriodicRangeX()[1] + settings.getPeriodicRangeX()[0])/2);
                if(fabs(settings.getPeriodicRangeX()[1] - settings.getPeriodicRangeX()[0]) < 0.95*settings.getHorizontalPeriod())
                    settings.setMultiplier(0,settings.getMultipliers()[0]*0.9);
            }
            else
            {
                settings.setMultiplier(0,(this->getImage()->width())* 1e8);
                settings.setScaleFactor(0,-processPhaseH(oldCoordinates[settings.getCoordinateNumbers()[0]]));
            }
        }
        else
        {
            if((fabs(settings.getScalesX()[1] - settings.getScalesX()[0]) > 1e-8))
            {
                if(fabs(settings.getRangeX()[1] - settings.getRangeX()[0]) > 0)
                {
                    settings.setMultiplier(0, (this->getImage()->width()) / fabs(settings.getRangeX()[1] - settings.getRangeX()[0]));
                }
                else
                {
                    settings.setMultiplier(0,1);
                    LOG(WARNING)<<"Horizontal multiplicator tends to infinity";
                }
                settings.setScaleFactor(0,-(settings.getRangeX()[1] + settings.getRangeX()[0])/2);
                settings.setMultiplier(0,settings.getMultipliers()[0]*0.9);
            }
            else
            {
                settings.setMultiplier(0,(this->getImage()->width())* 1e8);
                settings.setScaleFactor(0,-processPhaseH(oldCoordinates[settings.getCoordinateNumbers()[0]]));
            }
        }
    }
//    //Vertical
    if(settings.getCoordinateNumbers()[1] == 2*dimension)
    {//If Time - simply update multipliers
        settings.setMultiplier(1,(this->getImage()->height())/fabs(settings.getRangeY()[1] - settings.getRangeY()[0]));
        settings.setScaleFactor(1,-(settings.getRangeY()[1] + settings.getRangeY()[0])/2);
    }
    else
    {
        if(settings.getVerticalProcessing() == "Periodic")
        {
            if((fabs(settings.getScalesY()[1]-settings.getScalesY()[0])>1e-8))
            {
                if(fabs(settings.getPeriodicRangeY()[1] - settings.getPeriodicRangeY()[0]) > 0)
                {
                    settings.setMultiplier(1,(this->getImage()->height())/fabs(settings.getPeriodicRangeY()[1] - settings.getPeriodicRangeY()[0]));
                }
                else
                {
                    settings.setMultiplier(1,1);
                    LOG(WARNING)<<"Vertical multiplicator tends to infinity";
                }
                settings.setScaleFactor(1,-(settings.getPeriodicRangeY()[1] + settings.getPeriodicRangeY()[0])/2);
                if(fabs(settings.getPeriodicRangeY()[1] - settings.getPeriodicRangeY()[0]) < 0.95*settings.getVerticalPeriod())
                    settings.setMultiplier(1,settings.getMultipliers()[1]*0.9);
            }
            else
            {
                settings.setMultiplier(1,(this->getImage()->height())* 1e8);
                settings.setScaleFactor(1,-processPhaseV(oldCoordinates[settings.getCoordinateNumbers()[1]]));
            }
        }
        else
        {
            if((fabs(settings.getScalesY()[1]-settings.getScalesY()[0])>1e-8))
            {
                if(fabs(settings.getRangeY()[1] - settings.getRangeY()[0]) > 0)
                {
                    settings.setMultiplier(1,(this->getImage()->height())/fabs(settings.getRangeY()[1] - settings.getRangeY()[0]));
                }
                else
                {
                    settings.setMultiplier(1,1);
                    LOG(WARNING)<<"Vertical multiplicator tends to infinity";
                }
                settings.setScaleFactor(1,-(settings.getRangeY()[1] + settings.getRangeY()[0])/2);
                settings.setMultiplier(1,settings.getMultipliers()[1]*0.85);
            }
            else
            {
                settings.setMultiplier(1,(this->getImage()->height())* 1e8);
                settings.setScaleFactor(1,-processPhaseV(oldCoordinates[settings.getCoordinateNumbers()[1]]));
            }
        }
    }
}
void Painter::updateRange(void)
{
    if(settings.getCoordinateNumbers()[0] == 2*dimension)
    {
        ;//We will not update range for oscillograms
    }
    else
    {
        if(settings.getHorizontalProcessing() != "Periodic")
        {
            settings.setRangeX(0, settings.getScalesX()[0]);//If identical processing - set maxmin scales as range
            settings.setRangeX(1, settings.getScalesX()[1]);
        }
        else
        {
            if(isRunningX)
            {
                settings.setPeriodicRangeX(0, settings.getRangeX()[0]);
                settings.setPeriodicRangeX(1, settings.getRangeX()[1]);
            }
            else
            {
                settings.setPeriodicRangeX(0,settings.getScalesX()[0]);
                settings.setPeriodicRangeX(1,settings.getScalesX()[1]);
            }
        }
    }
    if(settings.getCoordinateNumbers()[1] == 2*dimension)
    {
        ;//We will not update range for oscillograms
    }
    else
    {
        if(settings.getVerticalProcessing() != "Periodic")
        {
            settings.setRangeY(0,settings.getScalesY()[0]);//If identical processing - set maxmin scales as range
            settings.setRangeY(1,settings.getScalesY()[1]);
        }
        else
        {
            if(isRunningY)
            {
                settings.setPeriodicRangeY(0,settings.getRangeY()[0]);
                settings.setPeriodicRangeY(1,settings.getRangeY()[1]);
            }
            else
            {
                settings.setPeriodicRangeY(0,settings.getScalesY()[0]);
                settings.setPeriodicRangeY(1,settings.getScalesY()[1]);
            }
        }
    }
}
void Painter::resizeEvent(QResizeEvent *event)
{
    delete pntr;
    ui->gridLayoutWidget->setFixedSize(event->size().width() - SIZE_SHIFT_X,
                                       event->size().height() - SIZE_SHIFT_Y);
    ui->imageLabel->setFixedSize(ui->gridLayoutWidget->size().width() - ui->vMin->width() - 6,
                                 ui->gridLayoutWidget->size().height() - ui->hMin->height() - 6);
    pntr = new QImage(ui->imageLabel->size(), QImage::Format_RGB32);
    pntr->fill(settings.getBackgroundColor());
    lineBuffer.clear();
    lineBufferFast.clear();
    pointBuffer.clear();
    pointBufferFast.clear();
    updateImage();
    this->doscalePrivate();
    this->updateLabels();
}
void Painter::setSettings(const WindowSettings &set)
{
    settings = set;
    settings.setPeriodicRangeX(settings.getRangeX());
    settings.setPeriodicRangeY(settings.getRangeY());
    if(settings.getHorizontalPeriod() == 0){
        LOG(WARNING)<<"Horizontal period is equal to zero";
    }
    if(settings.getVerticalPeriod() == 0){
        LOG(WARNING)<<"Vertical period is equal to zero";
    }
    this->doscalePrivate();
}
