#include "aboutapplication.h"
#include "ui_aboutapplication.h"
#include <QPixmap>
#include <QMouseEvent>
aboutApplication::aboutApplication(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::aboutApplication){
    ui->setupUi(this);
    ui->imageLabel->setPixmap(QPixmap(QApplication::applicationDirPath() + "\\Icons\\Logo.png"));
    this->setWindowTitle(tr("About application"));
    this->setWindowFlags(Qt::FramelessWindowHint);
    this->setAutoFillBackground(true);
    this->setWindowFlags(Qt::WindowStaysOnTopHint);
}
aboutApplication::~aboutApplication(){
    delete ui;
}
void aboutApplication::mousePressEvent(QMouseEvent *){
    this->close();
}
void aboutApplication::keyPressEvent(QKeyEvent *){
    this->close();
}