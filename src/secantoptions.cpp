﻿#include "../inc/secantoptions.h"
#include "ui_secantoptions.h"
#define SPACING 10
#define MAX_HEIGHT 25

SecantOptions::SecantOptions(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SecantOptions)
{
    ui->setupUi(this);
//    configure();
}
void SecantOptions::configure(const uint32_t &dim, const std::vector<QString> &IDs)
{
    std::for_each(secantEdits.begin(),secantEdits.end(),[](piLineEdit *edt){
        delete edt;
    });
    std::for_each(secantLabels.begin(),secantLabels.end(),[](QLabel *label){
        delete label;
    });
    secantEdits.clear();
    secantLabels.clear();
    QStringList listOfCoordinatesIDs;
    listOfCoordinatesIDs<<tr("None");
    std::for_each(IDs.begin(),IDs.end(),[&listOfCoordinatesIDs](QString ID)
    {
        listOfCoordinatesIDs << ID;
    });
    ui->cyclicNumberComboBox->addItems(listOfCoordinatesIDs);
    QStringList listOfMappingTypes;
    listOfMappingTypes << tr("Onesided");
    listOfMappingTypes << tr("Twosided");
    ui->typeComboBox->addItems(listOfMappingTypes);
    for(uint32_t i = 0; i < dim + 1; ++i)
    {
        secantEdits.push_back(new piLineEdit(0, &scrollWidget));
        secantLabels.push_back(new QLabel("a<span style=\" vertical-align:sub;\">"
                                          + QString::number(i) + "</span>"));
        mainLayout.addWidget(secantLabels[i], i, 0);
        mainLayout.addWidget(secantEdits[i], i, 1);
        connect(secantEdits[i], &piLineEdit::newValue, [this](double)
        {
            std::vector<double> secant;
            for(int i = 1; i < secantEdits.size(); ++i)
            {
                secant.push_back(secantEdits[i]->getValue());
            }
            secant.push_back(secantEdits[0]->getValue());
            emit secant_changed(secant);
        });
//        connect(secantEdits[i], SIGNAL(newValue(double)), this, SLOT(on_secant_changed(double)));
    }
    scrollWidget.setLayout(&mainLayout);
    scrollWidget.setFixedSize(ui->secantScrollArea->width(),
                              (dim + 1) * MAX_HEIGHT + dim * SPACING);
    ui->secantScrollArea->setWidget(&scrollWidget);
    scrollWidget.show();
    ui->gridLayout->addWidget(&cyclicPeriodEdit, 2, 1);
//    settings.secant = std::vector<double>(DS->Dimension(),0);
}
SecantOptions::~SecantOptions()
{
    delete ui;
    std::for_each(secantEdits.begin(),secantEdits.end(),[](piLineEdit *fromContainer)
    {
        delete fromContainer;
    });
    std::for_each(secantLabels.begin(),secantLabels.end(),[](QLabel *fromContainer)
    {
        delete fromContainer;
    });
}
void SecantOptions::on_typeComboBox_currentIndexChanged(const QString &arg1)
{
//    if(arg1 == "Onesided") settings.mappingType = Onesided;
//    else settings.mappingType = Twosided;
}
void SecantOptions::on_cyclicNumberComboBox_currentIndexChanged(int index)
{
    emit periodic_coordinate_changed(index);
}
//void SecantOptions::on_secant_changed(double)
//{
//    std::vector<double> secant;
//    for(int i = 0; i < secantEdits.size(); ++i)
//    {
//        secant.push_back(secantEdits[i]->getValue());
//    }
//    emit secant_changed(secant);
//}
