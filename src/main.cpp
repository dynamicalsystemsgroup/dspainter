#include <QtWidgets/QApplication>
#include "../inc/core.h"
#include <QSplashScreen>
#define ELPP_QT_LOGGING
INITIALIZE_EASYLOGGINGPP

/// All the modules are supposed to make logging by easylogging++ tool.
/// Log-file is located in /Logs/ dir. Sintax: LOG(ID)<<"%Some_message%";
/// Where ID stands for INFO,WARNING,FATAL and so on (i.e. to indicate the status
/// of the message). Its handy to us for debbuging purposes.

int main(int argc, char *argv[]){
    START_EASYLOGGINGPP(argc, argv);// Initialize logging tool
    QApplication a(argc, argv);
    /// Following trick is essential to start application with no Qt libs
    /// provided by PATH variable (assuming that user has folder /platforms/ and
    /// right dynamic libraries in it)
    QStringList paths = QCoreApplication::libraryPaths(); paths.append(".");
    paths.append(QApplication::applicationDirPath() + "\\platforms");
    QCoreApplication::setLibraryPaths(paths);
    /// Working name: "DSPainter". Any suggestions?
    QApplication::setApplicationName("DSPainter");
    QApplication::setApplicationVersion("1.0");
    QApplication::setOrganizationName("NNSU");// Lobachevsky State University  of
    //Nizhny Novgorod
    /// Here we show the splash screen for a couple of seconds fith some info about app
    QSplashScreen screen(QPixmap(QApplication::applicationDirPath() + "\\Icons\\startLogo.png"));
    screen.show();
    QTime timer;
    timer.start();
    for(uint32_t i = 0; i < 2;){
        if(timer.elapsed() > 1000){
            i++;
        }
    }
    screen.close();
    QFile styleFile("styles/default.qss");
    if (styleFile.open(QIODevice::ReadOnly)) {
        QString fileContents = QLatin1String(styleFile.readAll());
        qApp->setStyleSheet(fileContents);
    } else {
        qDebug()<<"Can not open qss file.";
    }
    /// The main module starts here with Core() constructor. See core.h for details.
    Core core;
    core.InitializeCore();
    /// Core object exists while Qt event loop is running.
    return a.exec();
}
