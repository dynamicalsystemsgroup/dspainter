#include "../inc/windowholder.h"

WindowHolder::WindowHolder(uint32_t newDimension, std::vector<QString> newCoordinateIDs,
                           localWindowType type, QWidget *parent): QFrame(parent)
{
    this->setAttribute(Qt::WA_DeleteOnClose);
    windowType = type;
    dimension = newDimension;
    coordinateIDs = newCoordinateIDs;
    configure();
    switch (windowType)
    {
    case noType:
        ;
        break;
    case flowDrawer:
        setAsPhasePortrait();
        break;
    case cascadeDrawer:
        setAsPoincareSection();
        break;
        //    case spatialDrawer:
        //        setAsSpatial();
        //        break;
    case poincareDiagram:
        setAsPoincareDiagram();
        break;
    default:
        break;
    }
}
void WindowHolder::paintFinishedSlot(void)
{
    emit paintFinished();
}
void WindowHolder::configureGeometry(void)
{
    if(interriorWidget != 0)
    {
        static_cast<TemplateWindow*>
                (interriorWidget)->updateGeometryFromHolder(
                    QRectF (
                        this->pos().x(),
                        this->pos().y(),
                        this->width(),
                        this->height()
                        ));
    }
}
void WindowHolder::configure(void)
{
    PoincareSectionButton.setText(tr("Poincare section"));
    PoincareSectionButton.setFont(QFont("Times New Roman",20));
    //    spatialButton.setText(tr("Spatial portrait"));
    //    spatialButton.setFont(QFont("Times New Roman",20));
    phasePortraitButton.setText(tr("Phase Portrait"));
    phasePortraitButton.setFont(QFont("Times New Roman",20));
    connect(&phasePortraitButton,SIGNAL(clicked()),this,SLOT(setAsPhasePortrait()));
    //    connect(&spatialButton,SIGNAL(clicked()),this,SLOT(setAsSpatial()));
    connect(&PoincareSectionButton,SIGNAL(clicked()),this,SLOT(setAsPoincareSection()));
    this->setLayout(&layout);
    interriorWidget = 0;
    layout.addWidget(&phasePortraitButton);
    layout.addWidget(&PoincareSectionButton);
//    layout.addWidget(&spatialButton);
    PoincareSectionButton.setEnabled(true);
//    spatialButton.setEnabled(false);
    this->setAttribute(Qt::WA_DeleteOnClose);
}
localWindowType WindowHolder::WindowType(void)
{
    return windowType;
}
WindowHolder::~WindowHolder()
{
}
QWidget* WindowHolder::widget(void)
{
    return interriorWidget;
}
void WindowHolder::resizeEvent(QResizeEvent *event)
{
    if(interriorWidget != 0)
    {
        interriorWidget->setFixedSize(event->size().width() + 10,
                                      event->size().height() + 10);
    }
}
void WindowHolder::setAsPoincareSection(void)
{
    windowType = cascadeDrawer;
    clearLayout();
    interriorWidget = static_cast<QWidget*>(new PoincarePainter(dimension, coordinateIDs, this));
    interriorWidget->setGeometry(QRect(QPoint(1,2),this->size()));
    interriorWidget->show();
    static_cast<TemplateWindow*>(interriorWidget)->setWindowType("CascadeDrawer");
    connect(static_cast<TemplateWindow*>(interriorWidget),SIGNAL(paintFinished()),
            this,SLOT(paintFinishedSlot()));
//    this->configureGeometry();
}
void WindowHolder::setAsPhasePortrait(void)
{
    windowType = flowDrawer;
    clearLayout();
    interriorWidget = static_cast<QWidget*>(new Painter(dimension, coordinateIDs, this));
    interriorWidget->setGeometry(QRect(QPoint(1,2),this->size()));
    interriorWidget->show();
    static_cast<TemplateWindow*>(interriorWidget)->setWindowType("FlowDrawer");
    connect(static_cast<TemplateWindow*>(interriorWidget),SIGNAL(paintFinished()),
            this,SLOT(paintFinishedSlot()));
//    this->configureGeometry(); // Dafuq? Why i wrote this?
}
void WindowHolder::setAsPoincareDiagram(void)
{
    ;
}
void WindowHolder::setAsSpatial()
{
    windowType = spatialDrawer;
    clearLayout();
    interriorWidget = static_cast<QWidget*>(new SpatialPainter(dimension,coordinateIDs,this));
    interriorWidget->setGeometry(QRect(QPoint(1,2),this->size()));
    interriorWidget->show();
    static_cast<TemplateWindow*>(interriorWidget)->setWindowType("SpatialDrawer");
    connect(static_cast<TemplateWindow*>(interriorWidget),SIGNAL(paintFinished()),
            this,SLOT(paintFinishedSlot()));
    this->configureGeometry();
}
void WindowHolder::clearLayout(void)
{
    for(int i = 0; i < layout.count(); ++i){
        layout.removeItem(layout.itemAt(i));
    }
    layout.activate();
    spatialButton.setVisible(false);
    phasePortraitButton.setVisible(false);
    PoincareSectionButton.setVisible(false);
}
