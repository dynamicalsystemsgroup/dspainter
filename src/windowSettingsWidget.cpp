﻿#include "../inc/windowSettingsWidget.h"

WindowSettingsWidget::WindowSettingsWidget(QWidget *parent) : QTabWidget(parent)
{
    this->addTab(&windowSettingsTab,tr("Properties"));
    connect(&windowSettingsTab,&visualSettings::propertiesChanged,[this](WindowSettings settings)
    {
        emit settingsChanged(settings);
    });
    this->addTab(&secantTab,tr("Secant"));
    connect(&secantTab, &SecantOptions::secant_changed, [this](std::vector<double> secant)
    {
        emit secant_changed(secant);
    });
    connect(&secantTab, &SecantOptions::periodic_coordinate_changed, [this](int number){
        emit periodic_index_changed(number);
    });
    this->setCurrentIndex(0);
}
void WindowSettingsWidget::keyPressEvent(QKeyEvent *keyEvent)
{
    emit keyPressed(keyEvent);
}
void WindowSettingsWidget::config(const uint32_t &newDimension, const std::vector<QString> &IDs)
{
    windowSettingsTab.setDimensionAndIDs(newDimension,IDs);
    secantTab.configure(newDimension, IDs);
}
void WindowSettingsWidget::setSettings(const WindowSettings &settings)
{
    windowSettingsTab.setSettings(settings);
}
void WindowSettingsWidget::config_secant_tab(const uint32_t &newDimension, const std::vector<QString> &IDs)
{
    secantTab.configure(newDimension, IDs);
}
