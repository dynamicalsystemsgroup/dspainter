﻿#include "../inc/dynamicalsystem.h"
#include "commonfunctions.h"

DynamicalSystem::DynamicalSystem(QObject *parent) : QThread(parent){
    time = 0;
    maxTime = 1000;
    step = 0.01;
    maxBufferSize = 5;
    stopCondition = true;
    debugMode = false;
    LOG(INFO)<<"Dynamical system object successfully created.";
}
void DynamicalSystem::setLibName(const QString &name){
    libraryName = name;
    LOG(INFO)<<"DynamicalSystem: library name is changed.";
}
bool DynamicalSystem::reload(const QString &name){
    setLibName(name);
    stopIntegrating();
    initializeDynamicalSystem();
    return isLoaded();
}
double DynamicalSystem::Step(void){
    return step;
}
uint32_t DynamicalSystem::ParamDimension(void){
    return paramDimension;
}
QString DynamicalSystem::SystemType(){
    return systemType;
}
DynamicalSystem::~DynamicalSystem(){
    if(library.isLoaded()) library.unload();
    if(this->isRunning()){
        this->terminate();
        this->wait(ULONG_MAX);
    }
    LOG(INFO)<<"Dynamical system deleted.";
}
functionPrototype DynamicalSystem::Function(void) const{
    return function;
}
std::vector<double> DynamicalSystem::Parameters(void) const{
    return parameters;
}
std::vector<double> DynamicalSystem::Coordinates(void) const{
    return coordinates;
}
void DynamicalSystem::setCoordinates(const std::vector<double> &newCoordinates){
    if(coordinates.size() != newCoordinates.size()){
        LOG(WARNING)<<"Dynamical system class: coordinates assignment - vectors have different size.";
        return;
    }
    coordinates = newCoordinates;
}
void DynamicalSystem::setParameter(uint32_t number, double newValue){
    if(number > parameters.size() - 1){
        LOG(WARNING)<<"Dynamical system class: parameters assignment - index out of bounds (set).";
        return;
    }
    parameters[number] = newValue;
}
void DynamicalSystem::setCoordinate(uint32_t number, double newValue){
    if(number > coordinates.size() - 1){
        LOG(WARNING)<<"Dynamical system class: coordinates assignment - index out of bounds (set).";
        return;
    }
    coordinates[number] = newValue;
}
double DynamicalSystem::Parameter(uint32_t number){
    if(number > parameters.size() - 1){
        LOG(WARNING)<<"Dynamical system class: parameters assignment - index out of bounds.";
        return 0;
    }
    return parameters[number];
}
double DynamicalSystem::Coordinate(uint32_t number){
    if(number > coordinates.size() - 1){
        LOG(WARNING)<<"Dynamical system class: coordinates assignment - index out of bounds.";
        return 0;
    }
    return coordinates[number];
}
std::vector<double> DynamicalSystem::Derivatives(std::vector<double> &coordinate){
    if(coordinates.size() < this->dimension){
        LOG(WARNING)<<"Dynamical system class: trying to get derivatives with wrong size of coordinates.";
        return std::vector<double>(2*this->dimension + 1,0);
    }
    function(coordinate.data(),derivatives.data(),parameters.data());
    return derivatives;
}
std::vector<QString> DynamicalSystem::ParameterIDs(void) const{
    return parameterIDs;
}
std::vector<QString> DynamicalSystem::CoordinateIDs(void) const{
    return coordinateIDs;
}
QString DynamicalSystem::SystemName(void){
    return systemName;
}
uint32_t DynamicalSystem::Time(void){
    return time;
}
uint32_t DynamicalSystem::Dimension(void){
    return dimension;
}
uint32_t DynamicalSystem::LineNumber(void){
    return lineNumber;
}
uint32_t DynamicalSystem::GridNumber(void){
    return gridNumber;
}
uint32_t DynamicalSystem::CubeNumber(void){
    return cubeNumber;
}
uint32_t DynamicalSystem::PatternPerNumber(void){
    return patternsPerNumber;
}
bool DynamicalSystem::isLoaded(void){
    return library.isLoaded();
}
void DynamicalSystem::setParameters(const std::vector<double> &newParameters){
    if(newParameters.size() < this->paramDimension){
        LOG(WARNING)<<"Dynamical system class: wrong size of parameters.";
        return;
    }
    parameters = newParameters;
}
void DynamicalSystem::initializeDynamicalSystem(){
    //resolve symbols from the library
    library.setFileName(libraryName);
    library.load();
    if(!library.isLoaded()){
        LOG(WARNING)<<"Dynamical system class: wrong name of library or could not open provided library.";
        emit loaded(false);
        return;
    }
    getNPrototype getDim = (getNPrototype) library.resolve("getSysDim");
    function = (functionPrototype) library.resolve("function");
    if(function == 0){
        library.unload();
        LOG(WARNING)<<"Dynamical system class: library loaded but function is not resolved.";
        emit loaded(false);
        return;
    }
    parameters.clear();
    parameterIDs.clear();
    coordinates.clear();
    coordinateIDs.clear();
    derivatives.clear();
    getParPrototype     getPar     = (getParPrototype)     library.resolve("getParamNum");
    getPhNamePrototype  getPhName  = (getPhNamePrototype)  library.resolve("getPeremName");
    getParNamePrototype getParName = (getParNamePrototype) library.resolve("getParamName");
    getSysTypePrototype getSysType = (getSysTypePrototype) library.resolve("getSysType");
    getLineNumPrototype getLineNum = (getLineNumPrototype) library.resolve("getLineNum");
    getGridNumPrototype getGridNum = (getGridNumPrototype) library.resolve("getGridNum");
    getCubeNumPrototype getCubeNum = (getCubeNumPrototype) library.resolve("getCubeNum");
    getSysNamePrototype getSysName = (getSysNamePrototype) library.resolve("getSysName");
    getPatPerNumPrototype getPatPerNum = (getPatPerNumPrototype) library.resolve("getPatPerNum");
    dimension = (uint32_t) getDim();
    paramDimension = (uint32_t) getPar();// could be 0
    parameters.assign(paramDimension,0);
    coordinates.assign(2*dimension + 1,0);
    derivatives.assign(dimension,0);
    for(uint32_t i = 0; i<dimension ; ++i){
        coordinateIDs.push_back(getPhName(i));
    }
    for(uint32_t i = dimension; i<2*dimension; ++i){
        coordinateIDs.push_back(coordinateIDs[i-dimension] + "'");
    }
    coordinateIDs.push_back("T");
    if(paramDimension != 0){
        for(uint32_t i = 0; i<paramDimension; ++i){
            parameterIDs.push_back(getParName(i));
        }
    }
    systemType = getSysType();
    if(systemType != "direct")
    {
        patternsPerNumber = (uint32_t) getPatPerNum();
    }
    if(systemType == "patternLine")
    {
        lineNumber = (uint32_t) getLineNum();
    }
    if(systemType == "patternGrid")
    {
        lineNumber = (uint32_t) getLineNum();
        gridNumber = (uint32_t) getGridNum();
    }
    if(systemType == "patternCube")
    {
        lineNumber = (uint32_t) getLineNum();
        gridNumber = (uint32_t) getGridNum();
        cubeNumber = (uint32_t) getCubeNum();
    }
    systemName = getSysName();
    emit loaded(true);
    LOG(INFO)<<"System library is loaded.";
}
void DynamicalSystem::setBufferSize(const uint32_t &size){
    if(maxBufferSize == 0){
        LOG(WARNING)<<"Buffer size is equal to zero.";
    }
    if(maxBufferSize > 10000){
        LOG(WARNING)<<"Buffer size is too large.";
    }
    maxBufferSize = size;
}
//mathematics::matrix DynamicalSystem::getJacobian(std::vector<double> point,
//                                                 double tolerance){
//    if(point.size() < this->dimension){
//        LOG(WARNING)<<"Dynamical system class: trying to get jacobian with wrong sized vector.";
//        return matrix(this->dimension,this->dimension);
//    }
//    using namespace mathematics;
//    matrix jacobian(this->dimension,this->dimension);
//    double reference;
//    double referenceF;
//    std::vector<double> f(this->dimension,0);
//    for(uint32_t i = 0; i < this->dimension; ++i)
//    {
//        for(uint32_t j = 0; j<this->dimension; ++j)
//        {
//            reference = point[j];
//            this->function(point.data(),f.data(),this->parameters.data());
//            referenceF = f[i];
//            point[j] += tolerance;
//            this->function(point.data(),f.data(),this->parameters.data());
//            jacobian(i,j) = (f[i] - referenceF)/tolerance;
//            point[j] = reference;
//        }
//    }
//    return jacobian;
//}
void DynamicalSystem::makeStep(void){
    if(step != 0) {
        try{
            commonFunctions::stepmake4(this->coordinates.data(),this->function,
                                       this->step,this->dimension,this->parameters.data());
        }
        catch(...) {
            LOG(WARNING)<<"Caught critical exeption in dynamical system`s makeStep()";
        }
    }
}
void DynamicalSystem::setDebugMode(bool value){
    this->debugMode = value;
}
std::vector<std::vector<double>> DynamicalSystem::getBuffer(void) const{
    return this->buffer;
}
void DynamicalSystem::setNullTime(void){
    time = 0;
}
void DynamicalSystem::collectBuffer(void)
{
    for(uint32_t i = 0; i < maxBufferSize; ++i)
    {
        if(this->stopCondition) break;
        if(this->time >= this->maxTime)
        {
            LOG(INFO)<<"Dynamical system class: maximum time reached.";
            this->stopCondition = true;
            emit operationStatusChanged(false);
            break;
        }
        else
        {
            if(this->step >= 0) this->time += this->step;
            else this->time -= this->step;
        }
        makeStep();
        this->coordinates[2*this->dimension] = this->time;
        this->buffer.push_back(this->coordinates);
    }
}
void DynamicalSystem::run()
{
    if(!this->isLoaded()) return;
    if(debugMode) return;
    while(this->time < this->maxTime)
    {
        this->mutex.lock();
        collectBuffer();
        this->mutex.unlock();
        if(this->buffer.size() > 0)
        {
            emit bufferCollected(this->buffer);
            this->buffer.clear();
        }
        msleep(1);
    }
}
void DynamicalSystem::setStep(const double& newStep)
{
    step = newStep;
    emit stepChanged(step);
}
void DynamicalSystem::startIntegrating(const double &Step, const double &MaxTime)
{
    if(!this->isLoaded()) return;
    if(Step == 0)
    {
        LOG(WARNING)<<"Dynamical system class: trying to start integrating with zero step.";
        return;
    }
    this->time = 0;
    this->setStep(Step);
    this->maxTime = MaxTime;
    this->stopCondition = false;
    if(!this->isRunning()) this->start();
    emit operationStatusChanged(true);
}
void DynamicalSystem::stopIntegrating(void)
{
    if(!this->isLoaded()) return;
    this->stopCondition = true;
    emit operationStatusChanged(false);
}
void DynamicalSystem::resume(void)
{
    if(!this->isLoaded()) return;
    if(!this->stopCondition)
    {
        if(this->time >= this->maxTime)
        {
            LOG(INFO)<<"Dynamical system class: maximum time reached.";
            emit operationStatusChanged(false);
            return;
        }
        this->start();
    }
}
/// getIntegralCurve(const std::vector<double> &initialConditions, const uint32_t &maximumTime, const double &step)
/// is implemented to collect points on integral curve with initial conditions in
/// initialConditions and in time interval [0, maximumTime] with time step in localStep
std::vector<std::vector<double>> DynamicalSystem::getIntegralCurve(std::vector<double> initialConditions, const double &maximumTime,
                                     const double &localStep)
{
    std::vector<std::vector<double>> integralCurve;
    uint32_t numberOfSteps = 0;
    if(localStep == 0)
    {
        LOG(WARNING) << "DynamicalSystem::getIntegralCurve : local step is equal to zero.";
    } else {
        numberOfSteps = fabs(floor(maximumTime / localStep));
    }
    for(uint32_t i = 0; i < numberOfSteps; ++i)
    {
        ;
        try{
            commonFunctions::stepmake4(initialConditions.data(),this->function,
                                       localStep,this->dimension,this->parameters.data());
        }
        catch(...) {
            LOG(FATAL)<<"Caught critical exeption in dynamical system`s getIntegralCurve().";
        }
        integralCurve.push_back(initialConditions);
    }
    return integralCurve;
}
