#include "../inc/setparameterswidget.h"
#include "ui_setparameterswidget.h"

SetParametersWidget::SetParametersWidget(uint32_t parameterDimension,
                                         std::vector<double> parameters,
                                         std::vector<QString> parameterIDs,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SetParametersWidget)
{
    ui->setupUi(this);
    this->setAttribute(Qt::WA_DeleteOnClose);
    this->setWindowTitle(tr("Set control parameters"));
    if( (parameters.size() != parameterDimension) ||
            (parameterIDs.size() != parameterDimension))
    {
        LOG(WARNING) << "SetParametersWidget: wrong parameters size.";
        return;
    }
    connect(ui->closeButton,SIGNAL(clicked()),this,SLOT(close()));
    expandingWidget = new QWidget(ui->scrollArea);
    layout = new QGridLayout(expandingWidget);
    for(uint32_t i = 0; i < parameterDimension; ++i)
    {
        parameterNameLabel.push_back(new QLabel(parameterIDs[i]));
        parameterValueBoxes.push_back(new QDoubleSpinBox());
        parameterValueBoxes.back()->setDecimals(15);
        parameterValueBoxes.back()->setRange(-1e10,1e10);
        parameterValueBoxes.back()->setValue(parameters[i]);
        layout->addWidget(parameterNameLabel.back(),i,0);
        layout->addWidget(parameterValueBoxes.back(),i,1);
    }
    std::for_each(parameterValueBoxes.begin(),parameterValueBoxes.end(),[this](QDoubleSpinBox *dsb){
        connect(dsb,SIGNAL(valueChanged(double)),this,SLOT(indicateParameterChange(double)));
    });
    expandingWidget->setFixedSize(static_cast<int>(0.9 * ui->scrollArea->width()),
                                  2*(parameterDimension+1)*INTERWIDGET_SPACE);
    expandingWidget->setLayout(layout);
    ui->scrollArea->setWidget(expandingWidget);
    expandingWidget->show();
}
SetParametersWidget::~SetParametersWidget()
{
    delete ui;
    std::for_each(parameterNameLabel.begin(),parameterNameLabel.end(),[](QLabel *parameterLabel){
        delete parameterLabel;
    });
    std::for_each(parameterValueBoxes.begin(),parameterValueBoxes.end(),[](QDoubleSpinBox *parameterValueBox){
        delete parameterValueBox;
    });
}
void SetParametersWidget::setParameters(const std::vector<double> &newParameters)
{
    if(newParameters.size() != parameterValueBoxes.size()){
        LOG(WARNING)<<"SetParametersWidget:setParameters() - wrong size of parameter vector.";
        return;
    }
    for(uint32_t i = 0; i < newParameters.size(); ++i){
        parameterValueBoxes[i]->setValue(newParameters[i]);
    }
}
void SetParametersWidget::indicateParameterChange(double)
{
    std::vector<double> parameters;
    for(uint32_t i = 0; i < parameterValueBoxes.size(); ++i) {
        parameters.push_back(parameterValueBoxes[i]->value());
    }
    emit parametersChanged(parameters);
}
