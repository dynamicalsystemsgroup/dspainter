#include "../inc/commonfunctions.h"

DynamicalSystem *commonFunctions::dynamical_system = nullptr;

commonFunctions::commonFunctions(){
}
commonFunctions::~commonFunctions(){
}
void commonFunctions::set_dynamical_system(DynamicalSystem *new_ds)
{
    dynamical_system = new_ds;
}

double commonFunctions::modPeriod(const double &value, const double &period, const double &periodMin)
{
    if((value > periodMin) && (value < periodMin + period)) return value;
    if(value < 0) return (fmod(value - periodMin,period) + periodMin + period);
    return (fmod(value - periodMin,period) + periodMin);
}
std::vector<double> commonFunctions::cubicApproximation(std::vector<double> oldPoint,
                                                        std::vector<double> newPoint,
                                                        std::vector<double> secant,
                                                        double secantSummOld,
                                                        double secantSummNew)
{
    double ra,rb,r1,r2,r3,cf1,cf2,cf3,cf4;
    uint32_t dimension = secant.size() - 1;
    std::vector<double> intersectonPoint(oldPoint.size(),0);
    ra = rb = 0;
    for(uint32_t i = 0;i<dimension;++i)
    {
        ra += oldPoint[i+dimension]*secant[i];
        rb += newPoint[i+dimension]*secant[i];
    }
    r1 = secantSummOld - secantSummNew;
    if((fabs(r1)<tolerance)||(fabs(ra)<tolerance)||(fabs(rb)<tolerance))
    {
        return std::vector<double>();// No transversal trajectories
    }
    ra = 1./ra;
    rb = 1./rb;
    r2 = r1*r1;
    r3 = r2*r1;
    cf1 = secantSummOld*secantSummOld*(secantSummOld-3.*secantSummNew)/r3;
    cf2 = secantSummNew*secantSummNew*(3.*secantSummOld-secantSummNew)/r3;
    cf3 = -secantSummNew*secantSummOld*secantSummOld/secantSummNew;
    cf4 = -secantSummNew*secantSummNew*secantSummOld/secantSummNew;
    intersectonPoint[oldPoint.size() - 1] =  cf1*newPoint[2*dimension]+cf2*oldPoint[2*dimension]+cf3*rb+cf4*ra;
    for(uint32_t i = 0;i<dimension;i++)
    {
        intersectonPoint[i] = cf1*newPoint[i]+cf2*oldPoint[i]+cf3*rb*newPoint[i+dimension]+cf4*ra*oldPoint[i+dimension];
    }
    return intersectonPoint;
}
void commonFunctions::stepmake4(double x[], double fun(double [], double [],double []),
                                double h, int n, double param[])
{
    double k1[n], k2[n], k3[n], k4[n], newpoint[n];
    int i;
    fun(x,k1,param);
    for (i=0; i<n; i++) newpoint[i]=x[i]+h*k1[i]/2;
    fun(newpoint,k2,param);
    for (i=0; i<n; i++) newpoint[i]=x[i]+h*k2[i]/2;
    fun(newpoint,k3,param);
    for (i=0; i<n; i++) newpoint[i]=x[i]+h*k3[i];
    fun(newpoint,k4,param);
    for (i=0; i<n; i++) x[i]+=h*(k1[i]+2*k2[i]+2*k3[i]+k4[i])/6;
    fun(x,k1,param);
    for(i=n;i<(2*n);i++) {x[i] = k1[i-n];}
}
bool commonFunctions::catchIntersection(std::vector<double> &intersectionCoordinates, std::vector<double> oldPhase,
                                        std::vector<double> newPhase, SecantContainer &secants)
{
    double s0a,sma,spa,s0b,smb,spb;
    s0a = sma = spa = s0b = smb = spb = 0;
    int dim = secants.dimension;
    for(int i = 0;i < dim;++i)
    {
        s0a += secants.secant[i]*oldPhase[i];
        sma += secants.secantMinus[i]*oldPhase[i];
        spa += secants.secantPlus[i]*oldPhase[i];
        s0b += secants.secant[i]*newPhase[i];
        smb += secants.secantMinus[i]*newPhase[i];
        spb += secants.secantPlus[i]*newPhase[i];
    }
    s0a+= secants.secant[dim];
    sma+= secants.secantMinus[dim];
    spa+= secants.secantPlus[dim];
    s0b+= secants.secant[dim];
    smb+= secants.secantMinus[dim];
    spb+= secants.secantPlus[dim];
    if((sma*smb)<0)
    {
        s0a = sma;
        s0b = smb;
        secants.secant[dim] = secants.secantMinus[dim];
        secants.secantMinus[dim]+= secants.period;
        secants.secantPlus[dim]+= secants.period;
        intersectionCoordinates = commonFunctions::cubicApproximation(oldPhase,newPhase,secants.secantMinus,sma,smb);
        std::vector<double> derivatives = dynamical_system->Derivatives(intersectionCoordinates);
        for(int i = 0; i < dim; ++i)
        {
            intersectionCoordinates[dim + i] = derivatives[i];
        }
        return true;
    }
    if((spa*spb)<0)
    {
        s0a = spa;
        s0b = spb;
        secants.secant[dim] = secants.secantPlus[dim];
        secants.secantMinus[dim]-= secants.period;
        secants.secantPlus[dim]-= secants.period;
        intersectionCoordinates = commonFunctions::cubicApproximation(oldPhase,newPhase,secants.secantPlus,spa,spb);
        std::vector<double> derivatives = dynamical_system->Derivatives(intersectionCoordinates);
        for(int i = 0; i < dim; ++i)
        {
            intersectionCoordinates[dim + i] = derivatives[i];
        }
        return true;
    }
    if((s0a*s0b)<0)
    {
        intersectionCoordinates = commonFunctions::cubicApproximation(oldPhase,newPhase,secants.secant,s0a,s0b);
        std::vector<double> derivatives = dynamical_system->Derivatives(intersectionCoordinates);
        for(int i = 0; i < dim; ++i)
        {
            intersectionCoordinates[dim + i] = derivatives[i];
        }
        return true;
    }
    return false;
}
mathematics::matrix commonFunctions::getJacobian(const uint32_t &dimension, const double &tolerance,
                                                 std::vector<double> point, std::vector<double> parameters,
                                                 localFunctionPrototype function)
{
    using namespace mathematics;
    matrix jacobian(dimension,dimension);
    double reference = 0;
    double referenceF = 0;
    std::vector<double> f;
    f.assign(dimension,0);
    for(uint32_t i = 0; i < dimension; ++i){
        for(uint32_t j = 0; j < dimension; ++j){
            reference = point[j];
            try {
                function(point.data(),f.data(),parameters.data());
                referenceF = f[i];
                point[j] += tolerance;
                function(point.data(),f.data(),parameters.data());
                jacobian(i,j) = (f[i] - referenceF)/tolerance;
                point[j] = reference;
            }
            catch(...){
                LOG(WARNING)<<"Jacobian calculation: exeption captured";
            }
        }
    }
    return jacobian;
}
