#include "../inc/windowsettings.h"
WindowSettings::WindowSettings(){
    horizontalProcessing = "Identical";
    verticalProcessing = "Identical";
    coordinateNumbers.assign(2,0);
    scalesX.assign(2,0);
    scalesY.assign(2,0);
    multipliers.assign(2,1);
    scaleFactors.assign(2,0);
    rangeX.assign(2,0);
    rangeX[0] = -10;
    rangeX[1] = 10;
    rangeY.assign(2,0);
    rangeY[0] = -10;
    rangeY[1] = 10;
    horizontalPeriod = M_PI*2;
    verticalPeriod = M_PI*2;
    lineColor = QColor(Qt::black);
    backgroundColor = QColor(Qt::white);
    pointSize = 1;
    lineWidth = 1;
    periodicRangeX.assign(2,0);
    periodicRangeY.assign(2,0);
    windowType = "NoType";
    joinPoints = false;
    timeLag = 2;
    renderDerivatives = false;
    cyclicCoordinateNumber = 0;
    cyclicPeriod = 2*M_PI;
//    mappingType = Twosided;
    secant.assign(2,0);
    visualizationRange.assign(2,0);
}
void WindowSettings::setGeometry(const QRectF &newGeometry){
    geometry = newGeometry;
}
void WindowSettings::setPointSize(const uint32_t &newPointSize){
    pointSize = newPointSize;
}
void WindowSettings::setLineWidth(const uint32_t &newLineWidth){
    lineWidth = newLineWidth;
}
void WindowSettings::setWindowType(const QString &newWindowType){
    windowType = newWindowType;
}
void WindowSettings::setCoordinateNumbers(const std::vector<uint32_t> &newCoordinateNumbers){
    coordinateNumbers = newCoordinateNumbers;
}
void WindowSettings::setCoordinateNumbers(const uint32_t &number,const double &value){
    coordinateNumbers[number] = value;
}
void WindowSettings::setScalesX(const std::vector<double> &newScalesX){
    scalesX = newScalesX;
}
void WindowSettings::setScaleX(const uint32_t &number,const double &value){
    if(number > 1) return;
    scalesX[number] = value;
}
void WindowSettings::setScalesY(const std::vector<double> &newScalesY){
    scalesY = newScalesY;
}
void WindowSettings::setScaleY(const uint32_t &number,const double &value){
    if(number > 1) return;
    scalesY[number] = value;
}
void WindowSettings::setMultipliers(const std::vector<double> &newMultipliers){
    multipliers = newMultipliers;
}
void WindowSettings::setMultiplier(const uint32_t &number, const double &value){
    if(number > 1) return;
    multipliers[number] = value;
}
void WindowSettings::setScaleFactors(const std::vector<double> &newScaleFactors){
    scaleFactors = newScaleFactors;
}
void WindowSettings::setScaleFactor(const uint32_t &number, const double &value){
    scaleFactors[number] = value;
}
void WindowSettings::setRangeX(const std::vector<double> &newRangeX){
    rangeX = newRangeX;
}
void WindowSettings::setRangeX(const uint32_t &number, const double &value){
    rangeX[number] = value;
}
void WindowSettings::setRangeY(const std::vector<double> &newRangeY){
    rangeY = newRangeY;
}
void WindowSettings::setRangeY(const uint32_t &number, const double &value){
    rangeY[number] = value;
}
void WindowSettings::setHorizontalProcessing(const QString &newHorizontalProcessing){
    horizontalProcessing = newHorizontalProcessing;
}
void WindowSettings::setVerticalProcessing(const QString &newVerticalProcessing){
    verticalProcessing = newVerticalProcessing;
}
void WindowSettings::setHorizontalPeriod(const double &newHorizontalPeriod){
    horizontalPeriod = newHorizontalPeriod;
}
void WindowSettings::setVerticalPeriod(const double &newVerticalPeriod){
    verticalPeriod = newVerticalPeriod;
}
void WindowSettings::setLineColor(const QColor &newLineColor){
    lineColor = newLineColor;
}
void WindowSettings::setBackgroundColor(const QColor &newBackgroundColor){
    backgroundColor = newBackgroundColor;
}
void WindowSettings::setPeriodicRangeX(const std::vector<double> &newPeriodicRangeX){
    periodicRangeX = newPeriodicRangeX;
}
void WindowSettings::setPeriodicRangeX(const uint32_t &number, const double &value){
    periodicRangeX[number] = value;
}
void WindowSettings::setPeriodicRangeY(const std::vector<double> &newPeriodicRangeY){
    periodicRangeY = newPeriodicRangeY;
}
void WindowSettings::setPeriodicRangeY(const uint32_t &number, const double &value){
    periodicRangeY[number] = value;
}
//Spatial drawer
void WindowSettings::setJoinPoints(const bool &newJoinPoints){
    joinPoints = newJoinPoints;
}
void WindowSettings::setTimeLag(const double &newTimeLag){
    timeLag = newTimeLag;
}
void WindowSettings::setRenderDerivatives(const bool &newRenderDerivatives){
    renderDerivatives = newRenderDerivatives;
}
void WindowSettings::setVisualizationRange(const std::vector<uint32_t> &newVisualizationRange){
    visualizationRange = newVisualizationRange;
}
void WindowSettings::setVisualizationRange(const uint32_t &number, const double &value){
    visualizationRange[number] = value;
}
//Poincare data
void WindowSettings::setCyclicCoordinateNumber(const uint32_t &newCyclicCoordinateNumber){
    cyclicCoordinateNumber = newCyclicCoordinateNumber;
}
void WindowSettings::setCyclicPeriod(const double &newCyclicPeriod){
    cyclicPeriod = newCyclicPeriod;
}
//    typeOfMapping mappingType;
void WindowSettings::setSecant(const std::vector<double> &newSecant){
    secant = newSecant;
}
// GET
QRectF WindowSettings::getGeometry(void) const{
    return geometry;
}
uint32_t WindowSettings::getPointSize(void) const{
    return pointSize;
}
uint32_t WindowSettings::getLineWidth(void) const{
    return lineWidth;
}
QString WindowSettings::getWindowType(void) const{
    return windowType;
}
std::vector<uint32_t> WindowSettings::getCoordinateNumbers(void) const{
    return coordinateNumbers;
}
std::vector<double> WindowSettings::getScalesX(void) const{
    return scalesX;
}
std::vector<double> WindowSettings::getScalesY(void) const{
    return scalesY;
}
std::vector<double> WindowSettings::getMultipliers(void) const{
    return multipliers;
}
std::vector<double> WindowSettings::getScaleFactors(void) const{
    return scaleFactors;
}
std::vector<double> WindowSettings::getRangeX(void) const{
    return rangeX;
}
std::vector<double> WindowSettings::getRangeY(void) const{
    return rangeY;
}
QString WindowSettings::getHorizontalProcessing(void) const{
    return horizontalProcessing;
}
QString WindowSettings::getVerticalProcessing(void) const{
    return verticalProcessing;
}
double WindowSettings::getHorizontalPeriod(void) const{
    return horizontalPeriod;
}
double WindowSettings::getVerticalPeriod(void) const{
    return verticalPeriod;
}
QColor WindowSettings::getLineColor(void) const{
    return lineColor;
}
QColor WindowSettings::getBackgroundColor(void) const{
    return backgroundColor;
}
std::vector<double> WindowSettings::getPeriodicRangeX(void) const{
    return periodicRangeX;
}
std::vector<double> WindowSettings::getPeriodicRangeY(void) const{
    return periodicRangeY;
}
//Spatial drawer
bool WindowSettings::getJoinPoints(void) const{
    return joinPoints;
}
double WindowSettings::getTimeLag(void) const{
    return timeLag;
}
bool WindowSettings::getRenderDerivatives(void) const{
    return renderDerivatives;
}
std::vector<uint32_t> WindowSettings::getVisualizationRange(void) const{
    return visualizationRange;
}
//Poincare data
uint32_t WindowSettings::getCyclicCoordinateNumber(void) const{
    return cyclicCoordinateNumber;
}
double WindowSettings::getCyclicPeriod(void) const{
    return cyclicPeriod;
}
//    typeOfMapping mappingType;
std::vector<double> WindowSettings::getSecant(void) const{
    return secant;
}