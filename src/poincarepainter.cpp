﻿#include "../inc/poincarepainter.h"

PoincarePainter::PoincarePainter(
        uint32_t newDimension,
        std::vector<QString> newCoordinateIDs,
        QWidget *parent) : Painter(newDimension, newCoordinateIDs, parent)
{
    LOG(INFO) << "CascadeDrawer upon FlowDrawer window created successfully.";
    set_secant(std::vector<double>(newDimension + 1, 0));
    setPointsAttached(false);
    periodic_coordinate_number = 0;
}
void PoincarePainter::updateSecants()
{
    secant_plus = secant;
    secant_minus = secant;
    if(secant.size() == 0) {
        LOG(WARNING) << "PoincarePainter::updateSecants() secant has zero size!";
        return;
    }
    secant_plus[secant_plus.size() - 1] -= M_PI * 2;
    secant_minus[secant_minus.size() - 1] += M_PI * 2;
    secant_container.secant = secant;
    secant_container.secantPlus = secant_plus;
    secant_container.secantMinus = secant_minus;
    secant_container.dimension = secant.size() - 1;
    secant_container.period = 2 * M_PI;
}
void PoincarePainter::set_periodic_index(int number)
{
    if(number < 0) LOG(INFO) << "PoincarePainter::set_periodic_index() periodic coordinate index problems";
    this->periodic_coordinate_number = number - 1;
}
void PoincarePainter::set_secant(const std::vector<double> &new_secant)
{
    LOG(INFO) << "PoincarePanter: secant is set.";
    secant = new_secant;
    if(secant.size() < 2) LOG(WARNING) << "PoincarePainter: secant vector has size < 2!";
    updateSecants();
}
void PoincarePainter::init_before_drawing(std::vector<double> coordinates)
{
    // last element of the secant - shift frome the origin
    if(this->periodic_coordinate_number >= 0) // when the index is different from "None"
    {
        secant[secant.size() - 1] = -coordinates[this->periodic_coordinate_number];
        updateSecants();
    }
}
void PoincarePainter::draw(const std::vector<std::vector<double> > &buffer)
{
    static std::vector<double> old_phase = buffer[0];
    if(buffer.empty())
    {
        emit paintFinished();
        return;
    }
    for(uint32_t i = 0; i < buffer.size(); ++i)
    {
        std::vector<double> intersection_coordinates;
        if(commonFunctions::catchIntersection(intersection_coordinates,old_phase, buffer[i], secant_container))
        {
            // intersection caught
            current.rx() = settings.getMultipliers()[0]*(processPhaseH(intersection_coordinates[settings.getCoordinateNumbers()[0]]) + settings.getScaleFactors()[0]) + this->getImage()->width()/2;
            current.ry() = -settings.getMultipliers()[1]*(processPhaseV(intersection_coordinates[settings.getCoordinateNumbers()[1]]) + settings.getScaleFactors()[1]) + this->getImage()->height()/2;
            drawPoint(current);
            std::vector<double> temp_scale(2, 0);
            temp_scale[0] = processPhaseH(intersection_coordinates[settings.getCoordinateNumbers()[0]]);
            temp_scale[1] = processPhaseV(intersection_coordinates[settings.getCoordinateNumbers()[1]]);
            updateScales(temp_scale);
        }
        old_phase = buffer[i];
    }
    emit paintFinished();
}
