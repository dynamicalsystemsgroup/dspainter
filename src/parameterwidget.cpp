#include "../inc/parameterwidget.h"
#include "ui_parameterwidget.h"

ParameterWidget::ParameterWidget(std::vector<QString> newParameterNames,
                                 QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ParameterWidget)
{
    ui->setupUi(this);
    parameterNames = newParameterNames;
    // Static cast is used to specify overloaded function to compiller !
    connect(ui->firstParComboBox, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),[this](int index) {
        if(index <= 0) {
            LOG(WARNING)<<"ParameterWidget: first active number LE 0.";
            return;
        } else {
            // Minus 1 due to 0 index is @None@ value
            emit numberChanged(ParameterWidget::First, index - 1);
            updateControls();
        }
    });
    connect(ui->secondParComboBox, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),[this](int index) {
        if(index <= 0) {
            LOG(WARNING)<<"ParameterWidget: second active number LE 0.";
            return;
        } else {
            // Minus 1 due to 0 index is @None@ value
            emit numberChanged(ParameterWidget::Second, index - 1);
            updateControls();
        }
    });
    connect(ui->firstParBox, static_cast<void (QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged), [this](double value) {
        parameters[ui->firstParComboBox->currentIndex() - 1] = value;
        emit valueChanged(ui->firstParComboBox->currentIndex() - 1, value);
    });
    connect(ui->secondParBox, static_cast<void (QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged), [this](double value) {
        parameters[ui->secondParComboBox->currentIndex() - 1] = value;
        emit valueChanged(ui->secondParComboBox->currentIndex() - 1, value);
    });
    connect(ui->firstParStepBox, static_cast<void (QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged), [this](double step){
        emit stepChanged(ParameterWidget::First, step);
    });
    connect(ui->secondParStepBox, static_cast<void (QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged), [this](double step){
        emit stepChanged(ParameterWidget::Second, step);
    });
    config();
    updateControls();
}
void ParameterWidget::setParameterNames(const std::vector<QString> &newNames)
{
    parameterNames = newNames;
    config();
}
ParameterWidget::~ParameterWidget()
{
    delete ui;
}
void ParameterWidget::config()
{
    ui->firstParComboBox->clear();
    ui->secondParComboBox->clear();
    ui->firstParComboBox->addItem("None");
    ui->secondParComboBox->addItem("None");
    std::for_each(parameterNames.begin(),parameterNames.end(),[&](QString parameterName){
        ui->firstParComboBox->addItem(parameterName);
        ui->secondParComboBox->addItem(parameterName);
    });
    parameters.assign(parameterNames.size(), 0);
    ui->firstParBox->setValue(0);
    ui->firstParStepBox->setValue(0.1);
    ui->secondParBox->setValue(0);
    ui->secondParStepBox->setValue(0.1);
}
void ParameterWidget::setParameters(const std::vector<double> &newParameters)
{
    parameters = newParameters;
    if(ui->firstParComboBox->currentIndex() > 0) {
        ui->firstParBox->setValue(newParameters[ui->firstParComboBox->currentIndex() - 1]);
    }
    if(ui->secondParComboBox->currentIndex() > 0) {
        ui->secondParBox->setValue(newParameters[ui->secondParComboBox->currentIndex() - 1]);
    }
    updateControls();
}
void ParameterWidget::updateControls(void)
{
    if(ui->firstParComboBox->currentText() == "None"){
        ui->firstParBox->setEnabled(false);
        ui->firstParStepBox->setEnabled(false);
    } else{
        ui->firstParBox->setEnabled(true);
        ui->firstParStepBox->setEnabled(true);
    }
    if(ui->secondParComboBox->currentText() == "None"){
        ui->secondParBox->setEnabled(false);
        ui->secondParStepBox->setEnabled(false);
    } else{
        ui->secondParBox->setEnabled(true);
        ui->secondParStepBox->setEnabled(true);
    }
}
void ParameterWidget::multiplyStep(const ActiveNumber &actNumber, const Direction &direction)
{
    switch (actNumber) {
    case ParameterWidget::First :
        if(direction == ParameterWidget::Positive){
            ui->firstParStepBox->setValue(ui->firstParStepBox->value() * 10.);
        } else {
            ui->firstParStepBox->setValue(ui->firstParStepBox->value() / 10.);
        }
        break;
    case ParameterWidget::Second :
        if(direction == ParameterWidget::Positive){
            ui->secondParStepBox->setValue(ui->secondParStepBox->value() * 10.);
        } else {
            ui->secondParStepBox->setValue(ui->secondParStepBox->value() / 10.);
        }
        break;
    default:
        LOG(WARNING)<<"ParameterWidget: multiplyStep() wrong active number.";
        break;
    }
    updateControls();
}
void ParameterWidget::shiftParameter(const ActiveNumber &actNumber, const Direction &direction)
{
    switch (actNumber) {
    case ParameterWidget::First :
        if(direction == ParameterWidget::Positive){
            ui->firstParBox->setValue(ui->firstParBox->value() + ui->firstParStepBox->value());
        } else {
            ui->firstParBox->setValue(ui->firstParBox->value() - ui->firstParStepBox->value());
        }
        break;
    case ParameterWidget::Second :
        if(direction == ParameterWidget::Positive){
            ui->secondParBox->setValue(ui->secondParBox->value() + ui->secondParStepBox->value());
        } else {
            ui->secondParBox->setValue(ui->secondParBox->value() - ui->secondParStepBox->value());
        }
        break;
    default:
        LOG(WARNING)<<"ParameterWidget: shiftParameter() wrong active number.";
        break;
    }
    updateControls();
}
void ParameterWidget::incrementActiveParameter(const ActiveNumber &actNumber)
{
    switch (actNumber) {
    case ParameterWidget::First:
        if(ui->firstParComboBox->currentIndex() >= (int) parameters.size()) { // Type cast to avoid compiller warnings
            ui->firstParComboBox->setCurrentIndex(0);
        } else {
            ui->firstParComboBox->setCurrentIndex(ui->firstParComboBox->currentIndex() + 1);
        }
        break;
    case ParameterWidget::Second:
        if(ui->secondParComboBox->currentIndex() >= (int) parameters.size()) {
            ui->secondParComboBox->setCurrentIndex(0);
        } else {
            ui->secondParComboBox->setCurrentIndex(ui->secondParComboBox->currentIndex() + 1);
        }
        break;
    default:
        LOG(WARNING)<<"ParameterWidget: incrementActiveParameter() wrong active number.";
        break;
    }
    updateControls();
}
int32_t ParameterWidget::getParameterNumber(const ActiveNumber &actNumber) const
{
    if(actNumber == ParameterWidget::First) {
        return (int32_t)(ui->firstParComboBox->currentIndex() - 1);
    } else {
        return (int32_t)(ui->secondParComboBox->currentIndex() - 1);
    }
}
ParameterSettings ParameterWidget::getParameterSettings(void) const
{
    ParameterSettings settings;
    settings.firstNumber = ui->firstParComboBox->currentIndex() - 1;
    settings.firstStep = ui->firstParStepBox->value();
    settings.secondNumber = ui->secondParComboBox->currentIndex() - 1;
    settings.secondStep = ui->secondParStepBox->value();
    settings.parameters = parameters;
    return settings;
}
void ParameterWidget::setSettings(const ParameterSettings &settings)
{
    if(settings.parameters.size() != this->parameters.size()) {
        LOG(WARNING) << "ParameterWidget::setSettings - size mismatch.";
    }
    if(settings.firstNumber > this->parameters.size()) {
        LOG(WARNING) << "ParameterWidget::setSettings - first number too large.";
    }
    if(settings.secondNumber > this->parameters.size()) {
        LOG(WARNING) << "ParameterWidget::setSettings - second number too large.";
    }
    ui->firstParComboBox->setCurrentIndex(settings.firstNumber + 1);
    ui->secondParComboBox->setCurrentIndex(settings.secondNumber + 1);
    ui->firstParStepBox->setValue(settings.firstStep);
    ui->secondParStepBox->setValue(settings.secondStep);
    this->setParameters(settings.parameters);
}
