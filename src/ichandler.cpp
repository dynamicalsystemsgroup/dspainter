#include "../inc/ichandler.h"
#include "ui_ichandler.h"
#define CONTROL_HEIGHT 20
#define CONTROL_DISTANCE 10
ICHandler::ICHandler(const uint32_t &newDimension,
                     const std::vector<double> &newCoordinates,
                     const std::vector<QString> &newCoordinateIDs,
                     QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ICHandler)
{
    ui->setupUi(this);
    this->setAttribute(Qt::WA_DeleteOnClose);
    this->setWindowTitle(tr("Set initial conditions"));
    if(newCoordinates.size() != 2*newDimension + 1){
        LOG(FATAL)<<"ICHandler: dimension mismatch.";
    }
    for(uint32_t i = 0; i < newDimension; ++i){
        phaseLabels.push_back(new QLabel(newCoordinateIDs[i]));
        forValues.push_back(new piLineEdit(newCoordinates[i]));
        mainLT.addWidget(phaseLabels[i],i,0);
        mainLT.addWidget(forValues[i],i,1);
    }
    expandingWidget.setFixedHeight(newDimension*(CONTROL_HEIGHT + CONTROL_DISTANCE) );
    expandingWidget.setLayout(&mainLT);
    expandingWidget.setFixedWidth(ui->scrollArea->width());
    ui->scrollArea->setWidget(&expandingWidget);
    readSettings();
    connect(ui->cancelBtn,SIGNAL(clicked()),this,SLOT(close()));
}
ICHandler::~ICHandler()
{
    writeSettings();
    std::for_each(forValues.begin(),forValues.end(),[](piLineEdit *edit){
        delete edit;
    });
    std::for_each(phaseLabels.begin(),phaseLabels.end(),[](QLabel *label){
        delete label;
    });
    delete ui;
}
void ICHandler::on_zeroBtn_clicked()
{
    std::for_each(forValues.begin(),forValues.end(),[](piLineEdit *edit){
        edit->setValue(0);
    });
}

void ICHandler::on_setBtn_clicked()
{
    std::vector<double> coords;
    coords.assign(2*forValues.size() + 1,0);
    for(uint32_t i = 0; i < forValues.size(); ++i){
        coords[i] = forValues[i]->getValue();
    }
    emit icChanged(coords);
    emit icFixed(ui->fixICBox->isChecked());
    this->close();
}
void ICHandler::on_randBtn_clicked()
{
    if(ui->fromDSB->value() > ui->toDSB->value()){
        QPalette pal;
        pal.setColor(QPalette::Base,Qt::red);
        ui->fromDSB->setPalette(pal);
        return;
    }
    else{
        QPalette pal;
        pal.setColor(QPalette::Base,Qt::white);
        ui->fromDSB->setPalette(pal);
    }
    srand(QTime::currentTime().msecsSinceStartOfDay());
    for(uint32_t i = 0; i < forValues.size();++i){
        auto newRand = ((double)rand())/RAND_MAX;
        newRand *= (ui->toDSB->value() - ui->fromDSB->value());
        newRand += ui->fromDSB->value();
        forValues[i]->setValue(newRand);
    }
}
void ICHandler::readSettings(void)
{
    QSettings settings(QApplication::applicationDirPath() +
                       "\\settings\\ICSettings.ini",QSettings::IniFormat,this);
    ui->toDSB->setValue(settings.value("Rand_max",QString()).toDouble());
    ui->fromDSB->setValue(settings.value("Rand_min",QString()).toDouble());
    ui->fixICBox->setChecked(settings.value("ICFixed",false).toBool());
}
void ICHandler::writeSettings(void)
{
    QSettings settings(QApplication::applicationDirPath() +
                       "\\settings\\ICSettings.ini",QSettings::IniFormat,this);
    settings.setValue("Rand_max",ui->toDSB->value());
    settings.setValue("Rand_min",ui->fromDSB->value());
    settings.setValue("ICFixed",ui->fixICBox->isChecked());
}

void ICHandler::on_toFileBtn_clicked()
{
    QString file_name = QFileDialog::getSaveFileName(nullptr, tr("IC - file name"), QApplication::applicationDirPath(),
                                                     tr("Data files (*.dat)"));
    if(file_name.isEmpty())
    {
        return;
    }
    QFile ic_file;
    ic_file.setFileName(file_name);
    if(ic_file.open(QIODevice::ReadWrite))
    {
        LOG(INFO) << "File for initial conditions created and opened successfully.";
        ic_file.close();
    }
    else
    {
        LOG(INFO) << "File for initial conditions could not be created/opened.";
        return;
    }
    QSettings file_to_save(file_name, QSettings::IniFormat, this);
    file_to_save.beginGroup("System_Name");
    file_to_save.setValue("System_name","Arbitrary system");
    file_to_save.endGroup();
    file_to_save.beginGroup("Initial_conditions");
    for(uint32_t i = 0; i < forValues.size(); ++i)
    {
        file_to_save.setValue("Coordinate_" + QString::number(i), forValues[i]->getValue());
    }
    file_to_save.endGroup();
    LOG(INFO) << "File for initial conditions is successfully written.";
}

void ICHandler::on_fromFileBtn_clicked()
{
    QString file_name = QFileDialog::getOpenFileName(nullptr, tr("Load initial conditions"),
                                                     QApplication::applicationDirPath(),
                                                     tr("Data files (*.dat)"));
    QFile ic_file;
    ic_file.setFileName(file_name);
    if(ic_file.open(QIODevice::ReadWrite))
    {
        LOG(INFO) << "File for initial conditions created and opened successfully.";
        ic_file.close();
    }
    else
    {
        LOG(INFO) << "File for initial conditions could not be created/opened.";
        return;
    }
    QSettings file_to_save(file_name, QSettings::IniFormat, this);
    file_to_save.beginGroup("System_Name");
    qDebug() << "System name: " << file_to_save.value("System_name", "Arbitrary system").toString();
    file_to_save.endGroup();
    file_to_save.beginGroup("Initial_conditions");
    for(uint32_t i = 0; i < forValues.size(); ++i)
    {
        forValues[i]->setValue(file_to_save.value("Coordinate_" + QString::number(i), 0).toDouble());
    }
    file_to_save.endGroup();
    LOG(INFO) << "File for initial conditions is successfully loaded.";
}
