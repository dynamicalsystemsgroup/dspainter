#include "../inc/generalsettings.h"
GeneralSettings::GeneralSettings(QObject *parent):QObject(parent){
    this->integratingStep = 0.01;
    this->maximumTime = 1e11;
    this->maximumCoordinateValue = 1e10;
    this->useFixedCoordinates = false;
    this->integratorBufferSize = 400;
}
GeneralSettings::GeneralSettings(double iS, double mT, double mCV, bool fixed,
                                 uint32_t intBufferSize, QObject *parent) : QObject(parent){
    this->integratingStep = iS;
    this->maximumTime = mT;
    this->maximumCoordinateValue = mCV;
    this->useFixedCoordinates = fixed;
    this->integratorBufferSize = intBufferSize;
}
GeneralSettings::GeneralSettings(const GeneralSettings &settings) : QObject(){
    this->integratingStep = settings.integratingStep;
    this->maximumTime = settings.maximumTime;
    this->maximumCoordinateValue = settings.maximumCoordinateValue;
    this->useFixedCoordinates = settings.useFixedCoordinates;
    this->integratorBufferSize = settings.integratorBufferSize;
}
void GeneralSettings::setIntegrationStep(const double &integStep){
    this->integratingStep = integStep;
}
void GeneralSettings::setMaximumTime(const double &maxTime){
    this->maximumTime = maxTime;
}
void GeneralSettings::setMaximumCoordinateValue(const double &maxCoordinate){
    this->maximumCoordinateValue = maxCoordinate;
}
void GeneralSettings::setUseFixedCoordinates(const bool &value){
    this->useFixedCoordinates = value;
}
void GeneralSettings::setIntegratorBufferSize(const uint32_t &size){
    this->integratorBufferSize = size;
}
double GeneralSettings::getIntegrationStep(void) const{
    return this->integratingStep;
}
double GeneralSettings::getMaximumTime(void) const{
    return this->maximumTime;
}
double GeneralSettings::getMaximumCoordinateValue(void) const{
    return this->maximumCoordinateValue;
}
bool GeneralSettings::getUseFixedCoordinates(void) const{
    return this->useFixedCoordinates;
}
uint32_t GeneralSettings::getIntegratorBufferSize(void) const{
    return this->integratorBufferSize;
}
