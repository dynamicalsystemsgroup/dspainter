#include "../inc/eqfinder.h"
#include "ui_eqfinder.h"

EqFinder::EqFinder(const uint32_t &newDimension,
                   const std::vector<double> &newCoordinates,
                   const std::vector<QString> &newCoordinateIDs, DynamicalSystem *DSPointer,
                   QWidget *parent) : QWidget(parent), ui(new Ui::EqFinder)
{
    ui->setupUi(this);
    this->setAttribute(Qt::WA_DeleteOnClose);
    dimension = newDimension;
    coordinates = newCoordinates;
    coordinateIDs = newCoordinateIDs;
    DS = DSPointer;
    connect(ui->exitBtn,SIGNAL(clicked()),this,SLOT(close()));
    this->setWindowTitle(tr("Find fixed points"));
    config();
    readSettings();
    clearListOfEntries();
    configListOfEntriesControls();
    LOG(INFO)<<"Equillibrium finder started.";
}
EqFinder::~EqFinder()
{
    writeSettings();
    delete ui;
    std::for_each(coordLabels.begin(),coordLabels.end(),[](QLabel *l){
        delete l;
    });
    std::for_each(ICCoordBoxes.begin(),ICCoordBoxes.end(),[](piLineEdit *le){
        delete le;
    });
    std::for_each(eigenIm.begin(),eigenIm.end(),[](QDoubleSpinBox *dsb){
        delete dsb;
    });
    std::for_each(eigenRe.begin(),eigenRe.end(),[](QDoubleSpinBox *dsb){
        delete dsb;
    });
    std::for_each(eigenV.begin(),eigenV.end(),[](QDoubleSpinBox *dsb){
        delete dsb;
    });
    std::for_each(coordinateList.begin(),coordinateList.end(),[](piLineEdit *p){
        delete p;
    });
    std::for_each(coordinateLabelList.begin(),coordinateLabelList.end(),[](QLabel *l){
        delete l;
    });
}
void EqFinder::config()
{
    ui->infoLabel->clear();
    eigenVectorRight = mathematics::matrix::eye(dimension,dimension);
    for(uint32_t i = 0 ; i < dimension; ++i){
        this->coordLabels.push_back(new QLabel(coordinateIDs[i]));
        ICCoordBoxes.push_back(new piLineEdit(coordinates[i]));
    }
    for(uint32_t i = 0; i < dimension; ++i){
        eigenIm.push_back(new QDoubleSpinBox(0));
        eigenRe.push_back(new QDoubleSpinBox(0));
        eigenV.push_back(new QDoubleSpinBox(0));
        eigenIm[i]->setDecimals(15);
        eigenIm[i]->setRange(-1e10,1e10);
        eigenRe[i]->setDecimals(15);
        eigenRe[i]->setRange(-1e10,1e10);
        eigenV[i]->setDecimals(15);
        eigenV[i]->setRange(-1e10,1e10);
        ICLayout.addWidget(coordLabels[i],i,0);
        ICLayout.addWidget(ICCoordBoxes[i],i,1);
        eigenLayout.addWidget(eigenRe[i],i,0);
        eigenLayout.addWidget(eigenIm[i],i,1);
        eigenVLayout.addWidget(eigenV[i],i,0);
    }
    ICWidget.setLayout(&ICLayout);
    eigenWidget.setLayout(&eigenLayout);
    eigenVWidget.setLayout(&eigenVLayout);
    ICWidget.setFixedSize(ui->ICScrollArea->width(),
                          dimension*MAX_HEIGHT + (dimension - 1)*SPACING);
    ui->ICScrollArea->setWidget(&ICWidget);
    eigenWidget.setFixedSize(ui->eigenScrollArea->width(),
                             dimension*MAX_HEIGHT + (dimension - 1)*SPACING);
    ui->eigenScrollArea->setWidget(&eigenWidget);
    eigenVWidget.setFixedSize(ui->eigenVScrollArea->width(),
                              dimension*MAX_HEIGHT + (dimension-1)*SPACING);
    ui->eigenVScrollArea->setWidget(&eigenVWidget);
    QStringList list;
    for(uint32_t i = 0; i < dimension; ++i)
        list<<("Vector " + QString::number(i+1));
    ui->eigVectorBox->addItems(list);
    ui->eigVectorBox->setCurrentIndex(0);
    LOG(INFO)<<"Equillibrium finder configuration is done.";
}
void EqFinder::paramChanged(){
    calculate();
}
void EqFinder::on_calcBtn_clicked(){
    calculate();
}
void EqFinder::calculate(void){
        using namespace mathematics;
        ui->infoLabel->setText(tr("<font color=\"DarkOrange\">In progress!"));
        for(uint32_t bb = 0; bb < dimension; ++bb){
            eigenIm[bb]->setValue(0);
            eigenRe[bb]->setValue(0);
            eigenV[bb]->setValue(0);
        }
        matrix f(dimension,1);
        matrix xB(dimension,1);// previous coordinates
        matrix xA(dimension,1);// next coordinates
        matrix jacobian(dimension,dimension);//jacobian in current point
        for(uint32_t i = 0 ; i < dimension; ++i){
            xB(i,0) = ICCoordBoxes[i]->getValue();
        }
        // Loop until Newtons iterations converge or until
        // counter reaches maximum iterations number
        for(uint32_t i = 0;i < (uint32_t)ui->maxIterBox->value(); ++i){
            DS->Function()(xB.getDoubleData(),f.getDoubleData(),DS->Parameters().data());
            jacobian = commonFunctions::getJacobian(dimension,ui->diffAccuracyBox->value(),
                                                    xB.getStdVectorData(),DS->Parameters(),DS->Function());
            xA = xB - mathematics::solve(jacobian,f);
            if((xA-xB).norm() < ui->newtonToleranceBox->value()) {
                matrix eigenValueRe(DS->Dimension(),1);
                matrix eigenValueIm(DS->Dimension(),1);
                matrix eigenVectorRight(DS->Dimension(),DS->Dimension());
                eigenData(jacobian,eigenValueRe,eigenValueIm,eigenVectorRight);
                for(uint32_t bb = 0; bb < DS->Dimension(); ++bb){
                    ICCoordBoxes[bb]->setValue(xA(bb,0));
                    eigenRe[bb]->setValue(eigenValueRe(bb,0));
                    eigenIm[bb]->setValue(eigenValueIm(bb,0));
                    eigenV[bb]->setValue(eigenVectorRight(bb,ui->eigVectorBox->currentIndex()));
                }
                ui->infoLabel->setText(tr("<font color=\"DarkBlue\">Equillibrium found!"));
                return;
            }
            xB = xA;
        }
        ui->infoLabel->setText(tr("<font color=\"DeepPink\">Equillibrium not found!"));
}
void EqFinder::on_eigVectorBox_currentIndexChanged(int index)
{
    for(uint32_t bb = 0; bb < DS->Dimension(); ++bb) {
        eigenV[bb]->setValue(eigenVectorRight(bb,index));
    }
}
void EqFinder::on_invertBtn_clicked()
{
    for(uint32_t i = 0; i < DS->Dimension(); ++i){
        eigenV[i]->setValue(-eigenV[i]->value());
    }
}
void EqFinder::readSettings()
{
    QSettings settings(QApplication::applicationDirPath() +
                       "\\settings\\Equillibrium.ini",QSettings::IniFormat,this);
    ui->intTimeBox->setValue(settings.value("Integrating_time",100).toDouble());
    ui->stepBox->setValue(settings.value("Step",0.05).toDouble());
    ui->maxIterBox->setValue(settings.value("Max_Iterations",40).toInt());
    ui->diffAccuracyBox->setValue(settings.value("Diff_accuracy",1e-8).toDouble());
    ui->newtonToleranceBox->setValue(settings.value("Newton_tolerance",1e-7).toDouble());
}
void EqFinder::writeSettings(){
    QSettings settings(QApplication::applicationDirPath() +
                       "\\settings\\Equillibrium.ini",QSettings::IniFormat,this);
    settings.setValue("Integrating_time",ui->intTimeBox->value());
    settings.setValue("Step",ui->stepBox->value());
    settings.setValue("Max_Iterations",ui->maxIterBox->value());
    settings.setValue("Diff_accuracy",ui->diffAccuracyBox->value());
    settings.setValue("Newton_tolerance",ui->newtonToleranceBox->value());
}
void EqFinder::setFixed(bool value)
{
    double eigVDelta = ui->stepBox->value();
    specialTrajectoryEntry singleEntry;
    singleEntry.coordinates.assign(2*DS->Dimension() + 1, 0);
    std::vector<double> derivatives;
    for(uint32_t i = 0; i < DS->Dimension(); ++i){
        singleEntry.coordinates[i] = ICCoordBoxes[i]->getValue() +
                              eigenV[ui->eigVectorBox->currentIndex()]->value() * eigVDelta;
    }
    derivatives = DS->Derivatives(singleEntry.coordinates);
    for(uint32_t i = DS->Dimension(); i < 2*DS->Dimension(); ++i){
        singleEntry.coordinates[i] = derivatives[i-DS->Dimension()];
    }
    singleEntry.integratingTime = ui->intTimeBox->value();
    singleEntry.step = ui->stepBox->value();
    singleEntry.fixed = value;
    if(eigenRe[ui->eigVectorBox->currentIndex()]->value() > 0) {
        singleEntry.typeOfTime = typeTime::INVERSE;
    } else {
        singleEntry.typeOfTime = typeTime::DIRECT;
    }
    emit newSingleCoordinates(singleEntry);
}
void EqFinder::on_setBtn_clicked()
{
    setFixed(false);
}
void EqFinder::on_setFixedBtn_clicked()
{
    setFixed(true);
}
void EqFinder::on_addToListBtn_clicked()
{
    double eigVDelta = 0.001;
    specialTrajectoryEntry singleEntry;
    singleEntry.coordinates.assign(2*DS->Dimension() + 1,0);
    std::vector<double> derivatives;
    derivatives.assign(DS->Dimension(),0);
    for(uint32_t i = 0; i < DS->Dimension(); ++i){
        singleEntry.coordinates[i] = ICCoordBoxes[i]->getValue() +
                              eigenV[ui->eigVectorBox->currentIndex()]->value() * eigVDelta;
    }
    DS->Function()(singleEntry.coordinates.data(),derivatives.data(),DS->Parameters().data());
    for(uint32_t i = DS->Dimension(); i < 2*DS->Dimension(); ++i){
        singleEntry.coordinates[i] = derivatives[i-DS->Dimension()];//if coordinates.size() == 2*dim
    }
    singleEntry.integratingTime = ui->intTimeBox->value();
    singleEntry.step = ui->stepBox->value();
    if(eigenRe[ui->eigVectorBox->currentIndex()]->value() > 0) singleEntry.typeOfTime = DIRECT;
    else singleEntry.typeOfTime = INVERSE;
    addNewEntry(singleEntry);
}
// Set of IC related
void EqFinder::configListOfEntriesControls(void)
{
    ui->setDirectionBox->addItem(tr("Direct"));
    ui->setDirectionBox->addItem(tr("Inverse"));
    QPixmap pMap(ui->setColorLabel->size());
    pMap.fill(Qt::black);
    ui->setColorLabel->setPixmap(pMap);
    for(uint32_t i = 0; i < DS->Dimension(); ++i){
        coordinateList.push_back(new piLineEdit);
        connect(coordinateList[i],SIGNAL(newValue(double)),this,SLOT(coordinatesChanged(double)));
        coordinateLabelList.push_back(new QLabel(DS->CoordinateIDs()[i]));
        coordinatesLayout.addWidget(coordinateLabelList.at(i),i,0);
        coordinatesLayout.addWidget(coordinateList.at(i),i,1);
    }
    coordinatesScrollAreaWidget.setLayout(&coordinatesLayout);
    coordinatesScrollAreaWidget.setFixedHeight(DS->Dimension()*MAX_HEIGHT + (DS->Dimension()-1)*SPACING);
    ui->setCoordinateScrollArea->setWidget(&coordinatesScrollAreaWidget);
}
void EqFinder::coordinatesChanged(double)
{
    for(uint32_t i = 0; i < coordinateList.size(); ++i){
        if(ui->entryNumberBox->currentIndex() > setOfInitialConditions.size() + 1) {
            LOG(WARNING) << "EqFinder::coordinatesChanged() coordinates dimension mismatch.";
        }
        setOfInitialConditions[ui->entryNumberBox->currentIndex() - 1].coordinates[i] =
                coordinateList[i]->getValue();
    }
}
void EqFinder::clearListOfEntries(void)
{
    ui->entryNumberBox->clear();
    ui->entryNumberBox->addItem(tr("None"));
    setOfInitialConditions.clear();
    reconfigEntries();
}
void EqFinder::reconfigEntries(void)
{
    if(setOfInitialConditions.size() != 0) {
        ui->deleteAllEntriesBtn->setEnabled(true);
        ui->setSaveBtn->setEnabled(true);
        ui->setEntriesBtn->setEnabled(true);
    } else {
        ui->deleteAllEntriesBtn->setEnabled(false);
        ui->setSaveBtn->setEnabled(false);
        ui->setEntriesBtn->setEnabled(false);
    }
    if(ui->entryNumberBox->currentIndex() == 0) {
        ui->entryContentBox->setEnabled(false);
    } else {
        ui->entryContentBox->setEnabled(true);
        if(ui->entryNumberBox->currentIndex() - 1 > setOfInitialConditions.size()) {
            LOG(WARNING) << "EqFinder::reconfigEntries() coordinates dimension mismatch.";
        }
        if(setOfInitialConditions[ui->entryNumberBox->currentIndex() - 1].typeOfTime == DIRECT){
            ui->setDirectionBox->setCurrentText(tr("Direct"));
        } else {
            ui->setDirectionBox->setCurrentText(tr("Inverse"));
        }
        ui->setMaxTimeBox->setValue(setOfInitialConditions[ui->entryNumberBox->currentIndex() - 1].integratingTime);
        ui->setStepBox->setValue(setOfInitialConditions[ui->entryNumberBox->currentIndex() - 1].step);
        QPixmap pMap(ui->setColorLabel->size());
        pMap.fill(setOfInitialConditions[ui->entryNumberBox->currentIndex() - 1].lineColor);
        ui->setColorLabel->setPixmap(pMap);
        ui->commentEdit->setPlainText(setOfInitialConditions[ui->entryNumberBox->currentIndex() - 1].comment);
    }
}
void EqFinder::on_deleteAllEntriesBtn_clicked()
{
    clearListOfEntries();
    reconfigEntries();
}
void EqFinder::addNewEntry(specialTrajectoryEntry singleEntry)
{
    setOfInitialConditions.push_back(singleEntry);
    ui->entryNumberBox->addItem(QString::number(ui->entryNumberBox->count()));
    reconfigEntries();
}
void EqFinder::on_entryNumberBox_currentIndexChanged(int value){
    if(ui->entryNumberBox->count() < 2) return;
    if(value == 0) return;
    if(value - 1 > setOfInitialConditions.size()) {
        LOG(WARNING) << "EqFinder::on_entryNumberBox_currentIndexChanged() coordinates dimension mismatch.";
    }
    for(uint32_t i = 0; i < DS->Dimension(); ++i){
        coordinateList[i]->setValue(setOfInitialConditions[value-1].coordinates[i]);
    }
    reconfigEntries();
}
void EqFinder::on_setDirectionBox_currentIndexChanged(const QString &arg1){
    if(ui->entryNumberBox->currentIndex() == 0) return;
    if(ui->entryNumberBox->currentIndex() - 1 > setOfInitialConditions.size()) {
        LOG(WARNING) << "EqFinder::on_setDirectionBox_currentIndexChanged() coordinates dimension mismatch.";
    }
    if(arg1 == "Direct"){
        setOfInitialConditions[ui->entryNumberBox->currentIndex() - 1].typeOfTime = DIRECT;
    }
    else{
        setOfInitialConditions[ui->entryNumberBox->currentIndex() - 1].typeOfTime = INVERSE;
    }
}
void EqFinder::on_setMaxTimeBox_valueChanged(double)
{
    if(ui->entryNumberBox->currentIndex() - 1 > setOfInitialConditions.size()) {
        LOG(WARNING) << "EqFinder::on_setMaxTimeBox_valueChanged() coordinates dimension mismatch.";
    }
    setOfInitialConditions[ui->entryNumberBox->currentIndex() - 1].integratingTime =
            ui->setMaxTimeBox->value();
}
void EqFinder::on_setStepBox_valueChanged(double)
{
    if(ui->entryNumberBox->currentIndex() - 1 > setOfInitialConditions.size()) {
        LOG(WARNING) << "EqFinder::on_setStepBox_valueChanged() coordinates dimension mismatch.";
    }
    setOfInitialConditions[ui->entryNumberBox->currentIndex() - 1].step =
            ui->setStepBox->value();
}
void EqFinder::on_commentEdit_textChanged()
{
    if(ui->entryNumberBox->currentIndex() - 1 > setOfInitialConditions.size()) {
        LOG(WARNING) << "EqFinder::on_commentEdit_textChanged() coordinates dimension mismatch.";
    }
    setOfInitialConditions[ui->entryNumberBox->currentIndex() - 1].comment =
            ui->commentEdit->toPlainText();
}
void EqFinder::on_deleteEntryBtn_clicked()
{
    try {
        setOfInitialConditions.erase(setOfInitialConditions.begin() + ui->entryNumberBox->currentIndex() - 1);
    } catch(...) {
        LOG(WARNING) << "EqFinder::on_deleteEntryBtn_clicked() catch erase exception!";
    }
    ui->entryNumberBox->removeItem(ui->entryNumberBox->currentIndex());
    for(uint32_t i = 1; i < (uint32_t)ui->entryNumberBox->count(); ++i){
        ui->entryNumberBox->setItemText(i,QString::number(i));
    }
    reconfigEntries();
}
void EqFinder::on_setEntriesBtn_clicked()
{
    emit newSetOfCoordinates(setOfInitialConditions);
}
void EqFinder::on_setSaveBtn_clicked(){
    QDir home;
    home.setPath(QApplication::applicationDirPath() + "\\settings\\Sets of initial conditions\\");
    if(!home.exists()) home.mkdir(QApplication::applicationDirPath() + "\\settings\\Sets of initial conditions\\");
    QString path = QFileDialog::getSaveFileName(0,tr("Save set to file"),home.path(),tr("(*.ini)"));
    QSettings setFile(path,QSettings::IniFormat);
    setFile.clear();
    setFile.setValue("System_name",DS->SystemName());
    setFile.setValue("Entries",static_cast<int>(setOfInitialConditions.size())); // Size_type cannot be converted into const QVariant automatically
    for(uint32_t i = 0; i < setOfInitialConditions.size(); ++i) {
        setFile.beginGroup("Set_" + QString::number(i));
        setFile.beginGroup("Coordinates");
        for(uint32_t j = 0; j < setOfInitialConditions[i].coordinates.size(); ++j){
            setFile.setValue("C_"+QString::number(j),setOfInitialConditions[i].coordinates[j]);
        }
        setFile.endGroup();
        setFile.beginGroup("Direction");
        if(setOfInitialConditions[i].typeOfTime == DIRECT){
            setFile.setValue("Dir","Direct");
        } else {
            setFile.setValue("Dir","Inverse");
        }
        setFile.endGroup();
        setFile.beginGroup("Numerical");
        setFile.setValue("Int_time",setOfInitialConditions[i].integratingTime);
        setFile.setValue("Step",setOfInitialConditions[i].step);
        setFile.endGroup();

        setFile.endGroup();
    }
}
void EqFinder::on_setLoadBtn_clicked(){
    QDir home;
    home.setPath(QApplication::applicationDirPath() + "\\settings\\Sets of initial conditions\\");
    if(!home.exists()) home.mkdir(QApplication::applicationDirPath() + "\\settings\\Sets of initial conditions\\");
    QString path = QFileDialog::getOpenFileName(0,tr("Load set from file"),home.path(),tr("(*.ini)"));
    QSettings getFile(path,QSettings::IniFormat);
    if(DS->SystemName() != getFile.value("System_name","General").toString()){
        LOG(WARNING)<<"Incorrect IC file for loaded system";
    }
    clearListOfEntries();
    setOfInitialConditions.assign(getFile.value("Entries",0).toUInt(),specialTrajectoryEntry());
    for(uint32_t i = 0; i < setOfInitialConditions.size(); ++i){
        setOfInitialConditions[i].coordinates.assign(2*DS->Dimension() + 1, 0);
    }
    for(uint32_t i = 0; i < setOfInitialConditions.size(); ++i){
        getFile.beginGroup("Set_" + QString::number(i));
        getFile.beginGroup("Coordinates");
        for(uint32_t j = 0; j < setOfInitialConditions[i].coordinates.size(); ++j){
            setOfInitialConditions[i].coordinates[j] = getFile.value("C_"+QString::number(j),0).toDouble();
        }
        getFile.endGroup();
        getFile.beginGroup("Direction");
        if(getFile.value("Dir","Direct").toString() == "Direct"){
            setOfInitialConditions[i].typeOfTime = DIRECT;
        }
        else{
            setOfInitialConditions[i].typeOfTime = INVERSE;
        }
        getFile.endGroup();
        getFile.beginGroup("Numerical");
        setOfInitialConditions[i].integratingTime = getFile.value("Int_time",100).toDouble();
        setOfInitialConditions[i].step = getFile.value("Step",0.02).toDouble();
        getFile.endGroup();

        getFile.endGroup();
    }
    for(uint32_t i = 0; i < setOfInitialConditions.size(); ++i) {
        ui->entryNumberBox->addItem(QString::number(i+1));
    }
    reconfigEntries();
}
void EqFinder::on_setCurrentEntry_clicked(){
    std::vector<specialTrajectoryEntry> set;
    set.push_back(setOfInitialConditions[ui->entryNumberBox->currentIndex() - 1]);
    emit newSetOfCoordinates(set);
}
void EqFinder::on_fixedBox_toggled(bool checked){
    setOfInitialConditions[ui->entryNumberBox->currentIndex()].fixed = checked;
}
void EqFinder::on_p1UpButton_clicked()
{
    QKeyEvent *event = new QKeyEvent(QEvent::KeyPress,Qt::Key_Right,Qt::NoModifier);
    emit keyPressed(event);
    delete event;
}
void EqFinder::on_p1DownButton_clicked()
{
    QKeyEvent *event = new QKeyEvent(QEvent::KeyPress,Qt::Key_Left,Qt::NoModifier);
    emit keyPressed(event);
    delete event;
}
void EqFinder::on_p2UpButton_clicked()
{
    QKeyEvent *event = new QKeyEvent(QEvent::KeyPress,Qt::Key_Up,Qt::NoModifier);
    emit keyPressed(event);
    delete event;
}
void EqFinder::on_p2DownButton_clicked()
{
    QKeyEvent *event = new QKeyEvent(QEvent::KeyPress,Qt::Key_Down,Qt::NoModifier);
    emit keyPressed(event);
    delete event;
}
void EqFinder::parametersChanged(const std::vector<double> &newParameters)
{
    calculate();
}
