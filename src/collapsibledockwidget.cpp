#include "../inc/collapsibledockwidget.h"
CollapsibleDockWidget::CollapsibleDockWidget(const QString & title,
                      QWidget * parent,
                      Qt::WindowFlags flags) : QDockWidget(title,parent,flags)
{
    sidelabel = new QLabel("W\ni\nn\nd\no\nw\n \nS\ne\nt\nt\ni\ng\ns", this);
    sidelabel->setStyleSheet("color: black; font-size: 12pt;");// font-weight: bold;");
    sidelabel->setAlignment(Qt::AlignTop);
    this->setTitleBarWidget(sidelabel);
    this->setFeatures(QDockWidget::DockWidgetVerticalTitleBar);
    connect(this,SIGNAL(featuresChanged(QDockWidget::DockWidgetFeatures)),
            this,SLOT(featuresChanged(QDockWidget::DockWidgetFeatures)));
}
CollapsibleDockWidget::CollapsibleDockWidget(QWidget * parent, Qt::WindowFlags flags) : QDockWidget(parent,flags)
{
    this->setFeatures(QDockWidget::DockWidgetVerticalTitleBar);
    connect(this,SIGNAL(featuresChanged(QDockWidget::DockWidgetFeatures)),
            this,SLOT(featuresChanged(QDockWidget::DockWidgetFeatures)));
}
void CollapsibleDockWidget::setNewWidget(QWidget *widget)
{
    this->setWidget(widget);
    hideWidget();
}
void CollapsibleDockWidget::enterEvent(QEvent *)
{
    showWidget();
}
void CollapsibleDockWidget::leaveEvent(QEvent *)
{
    hideWidget();
}
void CollapsibleDockWidget::hideWidget()
{
    if(this->widget() == nullptr) return;
    QRect globalRect(this->mapToGlobal(QPoint(0,0)),this->size());
    if(!globalRect.contains(QCursor::pos())){
        this->widget()->setFixedWidth(MIN_WIDTH);
    }
}
void CollapsibleDockWidget::showWidget()
{
    if(this->widget() == nullptr) return;// Check widget presence
    if(this->widget()->width() == MAX_WIDTH) return;// Check if the widget is already shown
    QGraphicsOpacityEffect *opacity = new QGraphicsOpacityEffect(this->widget());
    this->widget()->setGraphicsEffect(opacity);
    QPropertyAnimation *animation = new QPropertyAnimation(opacity,"opacity");
    animation->setDuration(500);
    animation->setStartValue(0.1);
    animation->setEndValue(1);
    animation->setEasingCurve(QEasingCurve::InQuad);
    animation->start(QAbstractAnimation::DeleteWhenStopped);
    this->widget()->setFixedWidth(MAX_WIDTH);
}
void CollapsibleDockWidget::keyPressEvent(QKeyEvent *keyEvent)
{
    emit keyPressed(keyEvent);
}
