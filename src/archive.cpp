#include "../inc/archive.h"
#include "ui_archive.h"

Archive::Archive(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Archive)
{
    ui->setupUi(this);
    this->setAttribute(Qt::WA_DeleteOnClose);
    this->setWindowModality(Qt::ApplicationModal);
    this->setAttribute(Qt::WA_AlwaysStackOnTop);
}

Archive::~Archive()
{
    delete ui;
}

void Archive::on_cancelButton_clicked()
{
    this->close();
}

void Archive::on_browseButton_clicked()
{
    //QString fileName = QFileDialog::getOpenFileName();
}
