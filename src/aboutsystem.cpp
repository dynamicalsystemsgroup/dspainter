#include "aboutsystem.h"
#include "ui_aboutsystem.h"

aboutSystem::aboutSystem(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::aboutSystem){
    ui->setupUi(this);
    this->setAttribute(Qt::WA_DeleteOnClose);
    this->setWindowFlags(Qt::WindowStaysOnTopHint);
    config();
}
void aboutSystem::config(void){
    this->setWindowTitle(tr("About dynamical system"));
    if(DS->isLoaded()){
        ui->nameEdit->setText(DS->SystemName());
        ui->typeEdit->setText(DS->SystemType());
        ui->dimEdit->setText(QString::number(DS->Dimension()));
        ui->parDimEdit->setText(QString::number(DS->ParamDimension()));
        ui->patPerNumEdit->setText(QString::number(DS->PatternPerNumber()));
        if(DS->SystemType() == "direct"){
            ui->lineNumEdit->setText(QString::number(0));
            ui->gridNumEdit->setText(QString::number(0));
            ui->cubeNumEdit->setText(QString::number(0));
        }
        if(DS->SystemType() == "patternLine"){
            ui->lineNumEdit->setText(QString::number(DS->LineNumber()));
            ui->gridNumEdit->setText(QString::number(0));
            ui->cubeNumEdit->setText(QString::number(0));
        }
        if(DS->SystemType() == "patternGrid"){
            ui->lineNumEdit->setText(QString::number(DS->LineNumber()));
            ui->gridNumEdit->setText(QString::number(DS->GridNumber()));
            ui->cubeNumEdit->setText(QString::number(0));
        }
        if(DS->SystemType() == "patternCube"){
            ui->lineNumEdit->setText(QString::number(DS->LineNumber()));
            ui->gridNumEdit->setText(QString::number(DS->GridNumber()));
            ui->cubeNumEdit->setText(QString::number(DS->CubeNumber()));
        }
    }
    else{
        ui->nameEdit->setText(tr("System is not loaded"));
        ui->typeEdit->setText(tr("System is not loaded"));
        ui->dimEdit->setText(QString::number(0));
        ui->parDimEdit->setText(QString::number(0));
        ui->patPerNumEdit->setText(QString::number(0));
        ui->lineNumEdit->setText(QString::number(0));
        ui->gridNumEdit->setText(QString::number(0));
        ui->cubeNumEdit->setText(QString::number(0));
    }
    connect(ui->okBtn,SIGNAL(clicked()),this,SLOT(close()));
}
aboutSystem::~aboutSystem(){
    delete ui;
}
