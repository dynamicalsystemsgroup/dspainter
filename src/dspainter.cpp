﻿#include "../inc/dspainter.h"
DSPainter::DSPainter(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Form)
{
    ui->setupUi(this);
//    this->setWindowIcon(QIcon(":/Icon/Icon.ico"));
    setUpToolBar();
    setUpStatusBar();
    setUpDockWidget();
    setUpMenu();
    createActions();
    this->setWindowTitle(tr("Dynamical system drawer"));
}
void DSPainter::createActions()
{
    // Set @New Window Mode@ active
    newWindowAction = new QAction(QIcon("Icons/new.png"),tr("New"),this);
    connect(newWindowAction,&QAction::triggered,[this](){
        emit newWindowModeTriggered();
        qDebug()<<"New window mode triggered";
    });
    newWindowAction->setCheckable(true);
    // Set @Edit Mode@ active
    editAction = new QAction(QIcon("Icons/edit.png"),tr("Edit"),this);
    connect(editAction,&QAction::triggered,[this](){
        emit editModeTriggered();
        qDebug()<<"Edit mode triggered";
    });
    editAction->setCheckable(true);
    // Set @Lock Mode@ active
    lockAction = new QAction(QIcon("Icons/lock.png"),tr("Lock"),this);
    lockAction->setCheckable(true);
    connect(lockAction,&QAction::triggered,[this](){
        emit lockModeTriggered();
        qDebug()<<"Lock mode triggered";
    });
    // Exit action
    exitAction = new QAction(QIcon("Icons/quit.png"),tr("Exit"),this);
    connect(exitAction,SIGNAL(triggered()),this,SLOT(close()));
    //Window operations
    alignTopAction = new QAction(QIcon("Icons/align_top.png"),tr("Align top"),this);
    connect(alignTopAction,&QAction::triggered,[this](){
        emit alignTopTriggered();
        qDebug()<<"Align top triggered";
    });
    alignLeftAction = new QAction(QIcon("Icons/align_left.png"),tr("Align left"),this);
    connect(alignLeftAction,&QAction::triggered,[this](){
        emit alignLeftTriggered();
        qDebug()<<"Align left triggered";
    });
    deleteSelectedAction = new QAction(QIcon("Icons/close_selected.png"),tr("Close selected"),this);
    connect(deleteSelectedAction,&QAction::triggered,[this](){
        emit deleteSelectedTriggered();
        qDebug()<<"Delete selection triggered";
    });
    closeAllWindowsAction = new QAction(QIcon("Icons/close_all.png"),tr("Close all windows"),this);
    connect(closeAllWindowsAction,&QAction::triggered,[this](){
        emit closeAllWindowsTriggered();
        qDebug()<<"Close all triggered";
    });
    saveImageAction = new QAction(QIcon("Icons/saveImage.png"),tr("Save figure"),this);
    connect(saveImageAction,&QAction::triggered,[this](){
        emit saveImageTriggered();
        qDebug()<<"Save Image triggered";
    });
    // Integrator manipulations
    startIntegratingAction = new QAction(QIcon("Icons/start.png"),tr("Start"),this);
    startIntegratingAction->setCheckable(true);
    connect(startIntegratingAction,&QAction::triggered,[this](){
        emit startIntegrationTriggered();
        qDebug()<<"Start integration triggered";
    });
    stopIntegratingAction = new QAction(QIcon("Icons/stop.png"),tr("Stop"),this);
    connect(stopIntegratingAction,&QAction::triggered,[this](){
        emit stopIntegrationTriggered();
        qDebug()<<"Stop integration triggered";
    });
    stopIntegratingAction->setCheckable(true);
    // Initial conditions manipulations
    setPhaseCoordinatesAction = new QAction(QIcon("Icons/coordinates.png"),tr("Set initial conditions"),this);
    connect(setPhaseCoordinatesAction,&QAction::triggered,[this](){
        emit setPhaseCoordinatesTriggered();
        qDebug()<<"Set initial conditions triggered";
    });
    equillibriumFinderAction = new QAction(QIcon("Icons/equillibrium.png"),tr("Find fixed points"),this);

    connect(equillibriumFinderAction,&QAction::triggered,[this](){
        emit equillibriumFinderTriggered();
        qDebug()<<"Equillibrium finder triggered";
    });
    loadNewLibraryAction = new QAction(QIcon("Icons/ds_load.png"),tr("Load new system"),this);
    connect(loadNewLibraryAction,&QAction::triggered,[this](){
        emit loadNewLibraryTriggered();
        qDebug()<<"Load new library triggered";
    });
    loadLibBuilderAction = new QAction(QIcon("Icons/ds_b_load.png"),tr("Load system builder"),this);
    connect(loadLibBuilderAction,&QAction::triggered,[this](){
        emit loadLibraryBuilderTriggered();
        qDebug()<<"Load libBuilder triggered";
    });
    setParametersAction = new QAction(QIcon("Icons/setparam.png"),tr("Set control parameters"),this);
    connect(setParametersAction,&QAction::triggered,[this](){
        emit setParametersTriggered();
        qDebug()<<"Set parameters triggered";
    });
    createColormap = new QAction(QIcon("Icons/colormap.png"), tr("Create 2D colormap"), this);
    connect(createColormap, &QAction::triggered, [this](){
        emit createColormapTriggered();
        qDebug()<<"Create colormap triggered";
    });
    createWaveDrawer = new QAction(QIcon("Icons/wave.png"), tr("Create 2D wave oscillogramm"), this);
    connect(createWaveDrawer, &QAction::triggered, [this](){
        emit createWaveDrawerTriggered();
        qDebug()<<"Create wavedrawer triggered";
    });
    toolBar->addAction(newWindowAction);
    toolBar->addAction(editAction);
    toolBar->addAction(lockAction);
    toolBar->addSeparator();
    toolBar->addAction(setPhaseCoordinatesAction);
    toolBar->addAction(setParametersAction);
    toolBar->addAction(equillibriumFinderAction);
    toolBar->addAction(createColormap);
    toolBar->addAction(createWaveDrawer);
    toolBar->addSeparator();
    toolBar->addAction(alignTopAction);
    toolBar->addAction(alignLeftAction);
    toolBar->addAction(deleteSelectedAction);
    toolBar->addAction(closeAllWindowsAction);
    toolBar->addAction(saveImageAction);
    toolBar->addSeparator();
    toolBar->addAction(startIntegratingAction);
    toolBar->addAction(stopIntegratingAction);
    toolBar->addSeparator();
    toolBar->addAction(exitAction);

    scaleAction = new QAction(QIcon("Icons/scale.png"),tr("Scale"),this);
    connect(scaleAction,&QAction::triggered,[this](){
        emit scaleTriggered();
        qDebug()<<"Scale triggered";
    });
    resetAction = new QAction(QIcon("Icons/reset.png"),tr("Reset"),this);
    connect(resetAction,&QAction::triggered,[this](){
        emit resetTriggered();
        qDebug()<<"Reset triggered";
    });

//    dublicateAct = new QAction(QIcon("Icons/dublicate.png"),tr("Dublicate"),this);
//    connect(dublicateAct,SIGNAL(triggered()),this,SLOT(dublicateHandler()));
    mainMenu->addAction(newWindowAction);
    mainMenu->addAction(editAction);
    mainMenu->addAction(lockAction);
    mainMenu->addSeparator();
    mainMenu->addAction(loadNewLibraryAction);
    mainMenu->addAction(loadLibBuilderAction);
    mainMenu->addSeparator();
    mainMenu->addAction(exitAction);

    integrationOptionsMenu->addAction(setPhaseCoordinatesAction);
    integrationOptionsMenu->addAction(setParametersAction);
    integrationOptionsMenu->addAction(equillibriumFinderAction);
    integrationOptionsMenu->addAction(createColormap);
    integrationOptionsMenu->addAction(createWaveDrawer);
    integrationOptionsMenu->addSeparator();
    integrationOptionsMenu->addAction(startIntegratingAction);
    integrationOptionsMenu->addAction(stopIntegratingAction);

    windowMenu->addAction(alignTopAction);
    windowMenu->addAction(alignLeftAction);
    windowMenu->addSeparator();
    windowMenu->addAction(deleteSelectedAction);
    windowMenu->addAction(closeAllWindowsAction);
    windowMenu->addSeparator();
    windowMenu->addAction(saveImageAction);

    optionsAction = new QAction(QIcon("Icons/options.png"),tr("Options"),optionsMenu);
    connect(optionsAction,&QAction::triggered,[this](){
        emit showOptionsTriggered();
        qDebug()<<"Show options triggered";
    });
    aboutSystemAction = new QAction(QIcon("Icons/about.png"),tr("Dynamical system info"),optionsMenu);
    connect(aboutSystemAction,&QAction::triggered,[this](){
        emit aboutSystemTriggered();
        qDebug()<<"About system triggered";
    });
    optionsMenu->addAction(optionsAction);
    optionsMenu->addAction(aboutSystemAction);
    aboutApplicationAction = new QAction(QIcon("Icons/about.png"),tr("Show application info"),aboutMenu);
    connect(aboutApplicationAction,&QAction::triggered,[this](){
        emit aboutApplicationTriggered();
        qDebug()<<"About application triggered";
    });
    aboutMenu->addAction(aboutApplicationAction);
}
void DSPainter::keyPressEvent(QKeyEvent *event)
{
    emit keyPressed(event);
}
void DSPainter::changeGraphicalMode(GraphicalAreaNS::graphicalMode newMode)
{
    switch (newMode) {
    case GraphicalAreaNS::draw:
        newWindowAction->setChecked(true);
        editAction->setChecked(false);
        lockAction->setChecked(false);
        break;
    case GraphicalAreaNS::select:
        newWindowAction->setChecked(false);
        editAction->setChecked(true);
        lockAction->setChecked(false);
        break;
    case GraphicalAreaNS::lock:
        newWindowAction->setChecked(false);
        editAction->setChecked(false);
        lockAction->setChecked(true);
        break;
    default:
        break;
    }
}
void DSPainter::setWindowSettingsDockWidget(CollapsibleDockWidget *windowSettingsDock)
{
    windowSettingsDock->setAllowedAreas(Qt::RightDockWidgetArea);
    windowSettingsDock->setFeatures(QDockWidget::NoDockWidgetFeatures);
    this->addDockWidget(Qt::RightDockWidgetArea,windowSettingsDock);
}
void DSPainter::setUpDockWidget(void)
{
    parameterDockWidget = new QDockWidget(tr("Parameters"));
    parameterDockWidget->setAllowedAreas(Qt::LeftDockWidgetArea);
    parameterDockWidget->setFeatures(QDockWidget::NoDockWidgetFeatures);
    this->addDockWidget(Qt::LeftDockWidgetArea, parameterDockWidget);
}
void DSPainter::setUpStatusBar(void)
{
    statusBar = new QStatusBar(this);
    this->setStatusBar(statusBar);
    statusBar->showMessage(tr("Ready"));
}
void DSPainter::setUpToolBar(void)
{
    toolBar = this->addToolBar(tr("Main toolbar"));
    toolBar->setMovable(false);
}
void DSPainter::mousePressEvent(QMouseEvent *event){
    if(event->button() == Qt::RightButton){
        emit lockModeTriggered();
    }
}
void DSPainter::setGraphicalArea(QWidget *graphicalArea)
{
    QScrollArea *mainScrollArea = new QScrollArea(this);
    mainScrollArea->installEventFilter(this);
    mainScrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    mainScrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    mainScrollArea->setWidgetResizable(true);
    this->setCentralWidget(mainScrollArea);
    mainScrollArea->setWidget(graphicalArea);
    graphicalArea->show();
    mainScrollArea->show();
}
void DSPainter::setParameterDockWidget(QWidget *parameterDock)
{
    parameterDockWidget->setWidget(parameterDock);
}
DSPainter::~DSPainter()
{
    emit mainWindowClosed();
    delete ui;
}
void DSPainter::setUpMenu(void)
{
    mainMenu = this->menuBar()->addMenu(tr("&File"));
    integrationOptionsMenu = this->menuBar()->addMenu(tr("&Integration options"));
    windowMenu = this->menuBar()->addMenu(tr("&Window"));
    optionsMenu = this->menuBar()->addMenu(tr("Options"));
    aboutMenu = this->menuBar()->addMenu(tr("&Help"));
}
void DSPainter::reconfigureActions(bool value)
{
    newWindowAction->setEnabled(value);
    editAction->setEnabled(value);
    lockAction->setEnabled(value);
    alignLeftAction->setEnabled(value);
    alignTopAction->setEnabled(value);
    deleteSelectedAction->setEnabled(value);
    closeAllWindowsAction->setEnabled(value);
    setPhaseCoordinatesAction->setEnabled(value);
    startIntegratingAction->setEnabled(value);
    stopIntegratingAction->setEnabled(value);
    equillibriumFinderAction->setEnabled(value);
    saveImageAction->setEnabled(value);
    createColormap->setEnabled(false); // excluded from this version
    createWaveDrawer->setEnabled(false); // excluded from this version
    setParametersAction->setEnabled(value);
}
void DSPainter::reconfigStartStopIcons(bool isRunning)
{
    startIntegratingAction->setChecked(isRunning);
    stopIntegratingAction->setChecked(!isRunning);
}
bool DSPainter::eventFilter(QObject *object, QEvent *event)
{
    // This filter implemented to handle keypress events from DSPainter children
    if(event->type() == QEvent::KeyPress) {
        emit keyPressed(static_cast<QKeyEvent*>(event));
        return true;
    } else {
        return QMainWindow::eventFilter(object, event);
    }
}
