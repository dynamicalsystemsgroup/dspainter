#include "../inc/settingsloader.h"
SettingsLoader::SettingsLoader(){
    applicationPath = QApplication::applicationDirPath();
}
void SettingsLoader::loadPathSettings(void){
    QSettings settings(applicationPath + "\\settings\\paths.ini", QSettings::IniFormat);
    pathSet.libraryPath = settings.value("Library_path",QString()).toString();
    pathSet.libBuilderPath = settings.value("LibBuilder_path",QString()).toString();
}
void SettingsLoader::savePathSettings(void){
    QSettings settings(applicationPath + "\\settings\\paths.ini", QSettings::IniFormat);
    settings.setValue("Library_path",pathSet.libraryPath);
    settings.setValue("LibBuilder_path",pathSet.libBuilderPath);
}
void SettingsLoader::setLibraryPath(const QString &libPath){
    pathSet.libraryPath = libPath;
}
void SettingsLoader::setLibBuilderPath(const QString &libBuilderPath){
    pathSet.libBuilderPath = libBuilderPath;
}
QString SettingsLoader::getLibraryPath(void) const{
    return pathSet.libraryPath;
}
QString SettingsLoader::getLibBuilderPath(void) const{
    return pathSet.libBuilderPath;
}
ParameterSettings SettingsLoader::getParameterSettings(void) const{
    return parameterSet;
}
void SettingsLoader::set_path_settings(const pathSettings& settings)
{
    pathSet = settings;
}
pathSettings SettingsLoader::get_path_settings(void)
{
    return pathSet;
}
void SettingsLoader::load(QString systemName)
{
    dynamicalSystemName = systemName;
    loadCoordinates();
    loadParameters();
    loadWindows();
    loadGeneral();
}
void SettingsLoader::configureLoader(const uint32_t &newDimension, const uint32_t &newParameterDimension){
    dimension = newDimension;
    parameterDimension = newParameterDimension;
    coordinates.clear();
    parameters.clear();
    coordinates.assign(dimension * 2 + 1,0);
    parameters.assign(parameterDimension,0);
}
void SettingsLoader::setDSName(const QString &name){
    dynamicalSystemName = name;
}
void SettingsLoader::save(const std::vector<double> &coord, const ParameterSettings &set,
                          const std::vector<WindowSettings> &wind){
    saveCoordinates(coord);
    saveParameters(set);
    saveWindows(wind);
}
std::vector<double> SettingsLoader::getCoordinates(void) const{
    return coordinates;
}
std::vector<double> SettingsLoader::getParameters(void) const{
    return parameters;
}
std::vector<WindowSettings> SettingsLoader::getWindows(void) const{
    return windows;
}
GeneralSettings SettingsLoader::getGeneralSettings(void) const{
    return general;
}
void SettingsLoader::setCoordinates(const std::vector<double> &newCoordinates){
    coordinates = newCoordinates;
}
void SettingsLoader::setParameters(const std::vector<double> &newParameters/*, parameterSettings set*/){
    parameters = newParameters;
}
void SettingsLoader::setWindows(const std::vector<WindowSettings> &newWindows){
    windows = newWindows;
}
void SettingsLoader::setGeneralSettings(const GeneralSettings &newGeneral){
    general = newGeneral;
}
void SettingsLoader::loadCoordinates(void){
    QSettings settings(applicationPath + "\\settings\\system_coordinates\\" +
                       dynamicalSystemName + ".ini", QSettings::IniFormat);
    settings.beginGroup("Coordinates");
    for(uint32_t i = 0; i < coordinates.size(); ++i){
        coordinates[i] = settings.value("c_" + QString::number(i),0).toDouble();
    }
    settings.endGroup();
    LOG(INFO)<<"SettingsLoader::loadCoordinates() - coordinates loaded.";
}
void SettingsLoader::loadParameters(void){
    QSettings settings(applicationPath + "\\settings\\system_parameters\\" +
                       dynamicalSystemName + ".ini", QSettings::IniFormat);
    settings.beginGroup("Parameters");
    for(uint32_t i = 0; i < parameters.size(); ++i){
        parameters[i] = settings.value("p_" + QString::number(i),0).toDouble();
    }
    settings.endGroup();
    settings.beginGroup("Parameter_settings");
    parameterSet.firstNumber = settings.value("First_parameter",0).toInt();
    parameterSet.firstStep = settings.value("First_parameter_step",0.1).toDouble();
    parameterSet.secondNumber = settings.value("Second_parameter",0).toInt();
    parameterSet.secondStep = settings.value("Second_parameter_step",0.1).toDouble();
    parameterSet.parameters = parameters;
    settings.endGroup();
    LOG(INFO)<<"SettingsLoader::loadParameters() - parameters loaded.";
}
void SettingsLoader::loadWindows(void){
    QSettings settings(applicationPath + "\\settings\\system_windows\\" +
                       dynamicalSystemName + ".ini", QSettings::IniFormat);
    windows.assign(settings.value("Window_number",0).toUInt(),WindowSettings());
    for(uint32_t i = 0; i < windows.size(); ++i){
        settings.beginGroup("Window_" + QString::number(i));
        windows[i].setWindowType(settings.value("Window_type",QString("NoType")).toString());
        windows[i].setGeometry(QRectF(settings.value("x",0).toFloat(),
                                      settings.value("y",0).toFloat(),
                                      settings.value("width",400).toFloat(),
                                      settings.value("height",400).toFloat()
                                      ));
        windows[i].setCoordinateNumbers(0,settings.value("x_number",0).toUInt());
        windows[i].setCoordinateNumbers(1,settings.value("y_number",0).toUInt());
        windows[i].setScaleX(0, settings.value("scale_x_min",-10).toDouble());
        windows[i].setScaleX(1, settings.value("scale_x_max",10).toDouble());
        windows[i].setScaleY(0, settings.value("scale_y_min",-10).toDouble());
        windows[i].setScaleY(1, settings.value("scale_y_max",10).toDouble());
        windows[i].setMultiplier(0, settings.value("x_multiplier",1).toDouble());
        windows[i].setMultiplier(1, settings.value("y_multiplier",1).toDouble());
        windows[i].setScaleFactor(0, settings.value("x_scalar_factor",0).toDouble());
        windows[i].setScaleFactor(1, settings.value("y_scalar_factor",0).toDouble());
        windows[i].setRangeX(0, settings.value("x_range_min",-10).toDouble());
        windows[i].setRangeX(1, settings.value("x_range_max",10).toDouble());
        windows[i].setRangeY(0, settings.value("y_range_min",-10).toDouble());
        windows[i].setRangeY(1, settings.value("y_range_max",10).toDouble());
        windows[i].setHorizontalProcessing(settings.value("horizontal_processing","Identical").toString());
        windows[i].setVerticalProcessing(settings.value("vertical_processing","Identical").toString());
        windows[i].setHorizontalPeriod(settings.value("horizontal_period",M_PI*2).toDouble());
        windows[i].setVerticalPeriod(settings.value("vertical_period",M_PI*2).toDouble());
        windows[i].setPointSize(settings.value("point_size",1).toFloat());
        windows[i].setLineWidth(settings.value("line_width",1).toFloat());
        windows[i].setPeriodicRangeX(0,settings.value("x_periodic_range_min",-M_PI).toDouble());
        windows[i].setPeriodicRangeX(1,settings.value("x_periodic_range_max",M_PI).toDouble());
        windows[i].setPeriodicRangeY(0, settings.value("y_periodic_range_min",-M_PI).toDouble());
        windows[i].setPeriodicRangeY(1, settings.value("y_periodic_range_max",M_PI).toDouble());
        windows[i].setJoinPoints(settings.value("JoinPoints",false).toBool());
        windows[i].setVisualizationRange(0, settings.value("VisualisationRange_min",1).toUInt());
        windows[i].setVisualizationRange(1, settings.value("VisualisationRange_max",dimension).toUInt());
        settings.endGroup();
    }
}
void SettingsLoader::loadGeneral(void){
    QSettings settings(applicationPath + "\\settings\\system_general\\" +
                       dynamicalSystemName + ".ini", QSettings::IniFormat);
    settings.beginGroup("General_settings");
    general.setIntegrationStep(settings.value("Integrating_step",0.01).toDouble());
    general.setMaximumCoordinateValue(settings.value("Maximum_coordinate",1e10).toDouble());
    general.setMaximumTime(settings.value("Maximum_time",1e11).toDouble());
    general.setUseFixedCoordinates(settings.value("Use_fixed_coordinates",false).toBool());
    settings.endGroup();
    LOG(INFO)<<"SettingsLoader::loadGeneral() - general settings loaded.";
}
void SettingsLoader::saveCoordinates(const std::vector<double> &coord){
    QSettings settings(applicationPath + "\\settings\\system_coordinates\\" +
                       dynamicalSystemName + ".ini", QSettings::IniFormat);
    settings.clear();
    coordinates = coord;
    settings.beginGroup("Coordinates");
    for(uint32_t i = 0; i < coordinates.size(); ++i){
        settings.setValue("c_" + QString::number(i),coordinates[i]);
    }
    settings.endGroup();
    LOG(INFO)<<"SettingsLoader::saveCoordinates() - coordinates saved.";
}
void SettingsLoader::saveParameters(const ParameterSettings &set){
    QSettings settings(applicationPath + "\\settings\\system_parameters\\" +
                       dynamicalSystemName + ".ini", QSettings::IniFormat);
    settings.clear();
    parameters = set.parameters;
    settings.beginGroup("Parameters");
    for(uint32_t i = 0; i < parameters.size(); ++i){
        settings.setValue("p_" + QString::number(i),parameters[i]);
    }
    settings.endGroup();
    settings.beginGroup("Parameter_settings");
    settings.setValue("First_parameter",set.firstNumber);
    settings.setValue("First_parameter_step",set.firstStep);
    settings.setValue("Second_parameter",set.secondNumber);
    settings.setValue("Second_parameter_step",set.secondStep);
    settings.endGroup();
    LOG(INFO)<<"SettingsLoader::saveParameters() - Parameters saved.";
}
void SettingsLoader::saveWindows(const std::vector<WindowSettings> &wind){
    QSettings settings(applicationPath + "\\settings\\system_windows\\" +
                       dynamicalSystemName + ".ini", QSettings::IniFormat);
    settings.clear();
    windows = wind;
    settings.setValue("Window_number",(uint32_t)windows.size());
    for(uint32_t i = 0; i < windows.size(); ++i){
        if(windows[i].getWindowType() == "NoType") continue;
        settings.beginGroup("Window_" + QString::number(i));
        settings.setValue("Window_type",windows[i].getWindowType());
        settings.setValue("x",windows[i].getGeometry().x());
        settings.setValue("y",windows[i].getGeometry().y());
        settings.setValue("width",windows[i].getGeometry().width());
        settings.setValue("height",windows[i].getGeometry().height());
        settings.setValue("x_number",windows[i].getCoordinateNumbers()[0]);
        settings.setValue("y_number",windows[i].getCoordinateNumbers()[1]);
        settings.setValue("scale_x_min",windows[i].getScalesX()[0]);
        settings.setValue("scale_x_max",windows[i].getScalesX()[1]);
        settings.setValue("scale_y_min",windows[i].getScalesY()[0]);
        settings.setValue("scale_y_max",windows[i].getScalesY()[1]);
        settings.setValue("x_multiplier",windows[i].getMultipliers()[0]);
        settings.setValue("y_multiplier",windows[i].getMultipliers()[1]);
        settings.setValue("x_scalar_factor",windows[i].getScaleFactors()[0]);
        settings.setValue("y_scalar_factor",windows[i].getScaleFactors()[1]);
        settings.setValue("x_range_min",windows[i].getRangeX()[0]);
        settings.setValue("x_range_max",windows[i].getRangeX()[1]);
        settings.setValue("y_range_min",windows[i].getRangeY()[0]);
        settings.setValue("y_range_max",windows[i].getRangeY()[1]);
        settings.setValue("horizontal_processing",windows[i].getHorizontalProcessing());
        settings.setValue("vertical_processing",windows[i].getVerticalProcessing());
        settings.setValue("horizontal_period",windows[i].getHorizontalPeriod());
        settings.setValue("vertical_period",windows[i].getVerticalPeriod());
        settings.setValue("point_size",windows[i].getPointSize());
        settings.setValue("line_width",windows[i].getLineWidth());
        settings.setValue("x_periodic_range_min",windows[i].getPeriodicRangeX()[0]);
        settings.setValue("x_periodic_range_max",windows[i].getPeriodicRangeX()[1]);
        settings.setValue("y_periodic_range_min",windows[i].getPeriodicRangeY()[0]);
        settings.setValue("y_periodic_range_max",windows[i].getPeriodicRangeY()[1]);
        settings.setValue("JoinPoints",windows[i].getJoinPoints());
        settings.setValue("VisualisationRange_min",windows[i].getVisualizationRange()[0]);
        settings.setValue("VisualisationRange_max",windows[i].getVisualizationRange()[1]);
        settings.endGroup();
    }
    LOG(INFO)<<"SettingsLoader::saveWindows() - windows saved.";
}
void SettingsLoader::saveGeneral(const GeneralSettings &generalSet){
    QSettings settings(applicationPath + "\\settings\\system_general\\" +
                       dynamicalSystemName + ".ini", QSettings::IniFormat);
    settings.clear();
    general = generalSet;
    settings.beginGroup("General_settings");
    settings.setValue("Integrating_step",general.getIntegrationStep());
    settings.setValue("Maximum_coordinate",general.getMaximumCoordinateValue());
    settings.setValue("Maximum_time",general.getMaximumTime());
    settings.setValue("Use_fixed_coordinates",general.getUseFixedCoordinates());
    settings.endGroup();
    LOG(INFO)<<"SettingsLoader::saveGeneral() - general settings saved.";
}
