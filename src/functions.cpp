#include "functions.h"
void stepmake4(double x[], double fun(double [], double [],double []), double h, int n, double param[])
{
    double k1[n], k2[n], k3[n], k4[n], newpoint[n];
    int i;

    fun(x,k1,param);
    for (i=0; i<n; i++) newpoint[i]=x[i]+h*k1[i]/2;

    fun(newpoint,k2,param);
    for (i=0; i<n; i++) newpoint[i]=x[i]+h*k2[i]/2;


    fun(newpoint,k3,param);
    for (i=0; i<n; i++) newpoint[i]=x[i]+h*k3[i];

    fun(newpoint,k4,param);

    for (i=0; i<n; i++) x[i]+=h*(k1[i]+2*k2[i]+2*k3[i]+k4[i])/6;
    fun(x,k1,param);
    for(i=n;i<(2*n);i++) {x[i] = k1[i-n];}
}
