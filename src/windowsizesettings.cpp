#include "windowsizesettings.h"
#include "ui_windowsizesettings.h"

windowSizeSettings::windowSizeSettings(templateWindow *wnd, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::windowSizeSettings)
{
    this->setAttribute(Qt::WA_DeleteOnClose);
    this->setWindowFlags(Qt::WindowStaysOnTopHint);
    ui->setupUi(this);
    window = wnd;
    ui->heightBox->setValue(window->height());
    ui->widthBox->setValue(window->width());
}

windowSizeSettings::~windowSizeSettings()
{
    delete ui;
}

void windowSizeSettings::on_cancelBtn_clicked()
{
    close();
}

void windowSizeSettings::on_setBtn_clicked()
{
    window->setFixedSize(ui->widthBox->value(),ui->heightBox->value());
    this->close();
}
