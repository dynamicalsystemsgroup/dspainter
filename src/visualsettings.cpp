#include "../inc/visualsettings.h"
visualSettings::visualSettings(QWidget *parent) :
    QWidget(parent),ui(new Ui::visualSettingsForm){
    ui->setupUi(this);
    configPrimaryLayout();
    configSecondaryLayout();
    updateWidgets();
}
void visualSettings::keyPressEvent(QKeyEvent *keyEvent){
    emit keyPressed(keyEvent);
}
void visualSettings::setDimensionAndIDs(const uint32_t &newDimension, const std::vector<QString> &IDs){
    coordList.clear();
    for(uint32_t i = 0; i < IDs.size(); ++i){
        coordList.push_back(IDs[i]);
    }
    ui->horizCombo->clear();
    ui->vertCombo->clear();
    ui->horizCombo->addItems(coordList);
    ui->vertCombo->addItems(coordList);
    ui->horizCombo->setCurrentIndex(0);
    ui->vertCombo->setCurrentIndex(0);
}
void visualSettings::configPrimaryLayout(void){   
    methodsList<<"Identical";methodsList<<"Periodic";methodsList<<"User defined";
    ui->functionBoxH->addItems(methodsList);
    ui->functionBoxV->addItems(methodsList);
    ui->functionBoxH->setCurrentIndex(0);
    ui->functionBoxV->setCurrentIndex(0);
    connect(ui->horizCombo,SIGNAL(currentIndexChanged(int)),this,SLOT(changePhaseH(int)));
    connect(ui->vertCombo,SIGNAL(currentIndexChanged(int)),this,SLOT(changePhaseV(int)));
    connect(ui->functionBoxH,SIGNAL(currentIndexChanged(QString)),this,SLOT(changeProcH(QString)));
    connect(ui->functionBoxV,SIGNAL(currentIndexChanged(QString)),this,SLOT(changeProcV(QString)));
    connect(ui->pointSizeBox,SIGNAL(valueChanged(int)),this,SLOT(changePointSize(int)));
    connect(ui->lineWidthBox,SIGNAL(valueChanged(int)),this,SLOT(changeLineWidth(int)));
    connect(ui->joinChBox,SIGNAL(toggled(bool)),this,SLOT(changeJoinPoints(bool)));
    pMap = new QPixmap(ui->colorPresLabel->width(),ui->colorPresLabel->height());
    pMap->fill(Qt::black);
    ui->colorPresLabel->setPixmap(*pMap);
    pMap->fill(Qt::white);
    ui->bgColorPresLabel->setPixmap(*pMap);
}
void visualSettings::changeJoinPoints(bool value){
    settings.setJoinPoints(value);
    updateControlValues();
    updateWidgets();
}
void visualSettings::changePointSize(int value){
    settings.setPointSize((uint32_t) value);
    updateControlValues();
    updateWidgets();
}
void visualSettings::changeLineWidth(int value){
    settings.setLineWidth((uint32_t) value);
    updateControlValues();
    updateWidgets();
}
visualSettings::~visualSettings(){
    delete ui;
    delete pMap;
}
void visualSettings::setSettings(const WindowSettings &set){
    settings = set;
    configForSpecialWindow(set);
    updateControlValuesPrivate();
    updateWidgets();
}
void visualSettings::configForSpecialWindow(WindowSettings set){
    if(set.getWindowType() == "FlowDrawer"){
        ui->horizCombo->setEnabled(true);
        ui->vertCombo->setEnabled(true);
        periodH->setEnabled(true);
        hMax->setEnabled(true);
        hMin->setEnabled(true);
        ui->joinChBox->setEnabled(false);
        timeLag->setEnabled(false);
        ui->renderDerivativesBox->setEnabled(false);
        ui->functionBoxH->setEnabled(true);
    }
    if(set.getWindowType() == "SpatialDrawer"){
        ui->horizCombo->setEnabled(false);
        ui->vertCombo->setEnabled(false);
        periodH->setEnabled(false);
        hMax->setEnabled(false);
        hMin->setEnabled(false);
        ui->joinChBox->setEnabled(true);
        timeLag->setEnabled(true);
        ui->renderDerivativesBox->setEnabled(true);
        ui->functionBoxH->setEnabled(false);
    }
}
void visualSettings::configSecondaryLayout(void){
    hMax = new piLineEdit(10,this);
    hMin = new piLineEdit(-10,this);
    vMax = new piLineEdit(10,this);
    vMin = new piLineEdit(-10,this);
    periodH = new piLineEdit(2*M_PI,this);
    periodV = new piLineEdit(2*M_PI,this);
    timeLag = new piLineEdit(2,this);
    connect(hMax,SIGNAL(newValue(double)),this,SLOT(changeHMax(double)));
    connect(hMin,SIGNAL(newValue(double)),this,SLOT(changeHMin(double)));
    connect(vMax,SIGNAL(newValue(double)),this,SLOT(changeVMax(double)));
    connect(vMin,SIGNAL(newValue(double)),this,SLOT(changeVMin(double)));
    connect(periodH,SIGNAL(newValue(double)),this,SLOT(changePeriodH(double)));
    connect(periodV,SIGNAL(newValue(double)),this,SLOT(changePeriodV(double)));
    connect(timeLag,SIGNAL(newValue(double)),SLOT(changeTimeLag(double)));
    ui->mainGridLT->addWidget(periodH,6,1);
    ui->mainGridLT->addWidget(hMin,7,1);
    ui->mainGridLT->addWidget(hMax,8,1);
    ui->mainGridLT->addWidget(periodV,9,1);
    ui->mainGridLT->addWidget(vMin,10,1);
    ui->mainGridLT->addWidget(vMax,11,1);
    ui->mainGridLT->addWidget(timeLag,15,1);
}
void visualSettings::changeTimeLag(double value){
    settings.setTimeLag(value);
    updateControlValues();
    updateWidgets();
}
void visualSettings::changePeriodH(double value){
    hMax->setValue(hMin->getValue() + periodH->getValue());
    settings.setHorizontalPeriod(value);
    settings.setRangeX(1,hMin->getValue() + value);
    updateControlValues();
    updateWidgets();
}
void visualSettings::changePeriodV(double value){
    vMax->setValue(vMin->getValue() + periodV->getValue());
    settings.setVerticalPeriod(value);
    settings.setRangeY(1,vMin->getValue() + value);
    updateControlValues();
    updateWidgets();
}
void visualSettings::changeHMax(double value){
    settings.setRangeX(1,value);
    updateControlValues();
    updateWidgets();
}
void visualSettings::changeHMin(double value){
    settings.setRangeX(0,value);
    if(ui->functionBoxH->currentText() == "Periodic"){
        hMax->setValue(hMin->getValue() + periodH->getValue());
        settings.setRangeX(1,hMax->getValue());
    }
    updateControlValues();
    updateWidgets();
}
void visualSettings::changeVMax(double value){
    settings.setRangeY(1,value);
    updateControlValues();
    updateWidgets();
}
void visualSettings::changeVMin(double value){
    settings.setRangeY(0,value);
    if(ui->functionBoxV->currentText() == "Periodic")
    {
        vMax->setValue(vMin->getValue() + periodV->getValue());
        settings.setRangeY(1,vMax->getValue());
    }
    updateControlValues();
    updateWidgets();
}
void visualSettings::updateControlValuesPrivate(void){
    if(settings.getCoordinateNumbers()[0] < ui->horizCombo->count())
        ui->horizCombo->setCurrentIndex(settings.getCoordinateNumbers()[0]);
    if(settings.getCoordinateNumbers()[0] < ui->horizCombo->count())
        ui->vertCombo->setCurrentIndex(settings.getCoordinateNumbers()[1]);
    ui->functionBoxH->setCurrentText(settings.getHorizontalProcessing());
    ui->functionBoxV->setCurrentText(settings.getVerticalProcessing());
    periodH->setValue(settings.getHorizontalPeriod());
    periodV->setValue(settings.getVerticalPeriod());
    hMax->setValue(settings.getRangeX()[1]);
    hMin->setValue(settings.getRangeX()[0]);
    vMax->setValue(settings.getRangeY()[1]);
    vMin->setValue(settings.getRangeY()[0]);
    ui->fromBox->setValue(settings.getVisualizationRange()[0]);
    ui->toBox->setValue(settings.getVisualizationRange()[1]);
}
void visualSettings::updateControlValues(void){
    updateControlValuesPrivate();
    emit propertiesChanged(settings);
}
void visualSettings::updateWidgets(){
    if(ui->functionBoxH->currentText() == "Periodic"){
        periodH->setEnabled(true);
        hMax->setValue(hMin->getValue() + periodH->getValue());
        if(settings.getWindowType() == "SpatialDrawer"){
        }
        if(settings.getWindowType() == "FlowDrawer") hMax->setEnabled(false);
    }
    else{
        if(settings.getWindowType() == "SpatialDrawer"){
        }
        if(settings.getWindowType() == "FlowDrawer"){
            hMax->setEnabled(true);
            periodH->setEnabled(false);
        }
    }
    if(ui->functionBoxV->currentText() == "Periodic"){
        periodV->setEnabled(true);
        vMax->setValue(vMin->getValue() + periodV->getValue());
        vMax->setEnabled(false);
    }
    else{
        vMax->setEnabled(true);
        periodV->setEnabled(false);
    }
}
void visualSettings::changePhaseH(int currentH){
    if(ui->horizCombo->count() == 0) return;
    settings.setCoordinateNumbers(0,(uint32_t) currentH);
    updateControlValues();
    updateWidgets();
}
void visualSettings::changePhaseV(int currentV){
    if(ui->vertCombo->count() == 0) return;
    settings.setCoordinateNumbers(1,(uint32_t) currentV);
    updateControlValues();
    updateWidgets();
}
void visualSettings::changeProcH(QString currentH){
    updateWidgets();
    settings.setHorizontalProcessing(currentH);
    updateControlValues();
    updateWidgets();
}
void visualSettings::changeProcV(QString currentV){
    updateWidgets();
    settings.setVerticalProcessing(currentV);
    updateControlValues();
    updateWidgets();
}
void visualSettings::on_selectBgColorBtn_clicked(){
    QColorDialog colorDialog;
    colorDialog.setOption(QColorDialog::NoButtons);
    connect(&colorDialog,SIGNAL(currentColorChanged(QColor)),this,SLOT(bgColorChanged(QColor)));
    colorDialog.exec();
}
void visualSettings::lineColorChanged(QColor color){
    QPixmap pMap(ui->colorPresLabel->width(),ui->colorPresLabel->height());
    pMap.fill(color);
    ui->colorPresLabel->setPixmap(pMap);
    settings.setLineColor(color);
    updateControlValues();
    updateWidgets();
}
void visualSettings::bgColorChanged(QColor color){
    QPixmap pMap(ui->bgColorPresLabel->width(),ui->bgColorPresLabel->height());
    pMap.fill(color);
    ui->bgColorPresLabel->setPixmap(pMap);
    settings.setBackgroundColor(color);
    updateControlValues();
    updateWidgets();
}
void visualSettings::on_selectColorBtn_clicked(){
    QColorDialog colorDialog;
    colorDialog.setOption(QColorDialog::NoButtons);
    connect(&colorDialog,SIGNAL(currentColorChanged(QColor)),this,SLOT(lineColorChanged(QColor)));
    colorDialog.exec();
}
void visualSettings::on_renderDerivativesBox_toggled(bool checked){
    settings.setRenderDerivatives(checked);
    updateControlValues();
    updateWidgets();
}
void visualSettings::on_fromBox_valueChanged(int arg1){
    settings.setVisualizationRange(0,(uint32_t)arg1);
    updateControlValues();
    updateWidgets();
}
void visualSettings::on_toBox_valueChanged(int arg1){
    settings.setVisualizationRange(1,(uint32_t)arg1);
    updateControlValues();
    updateWidgets();
}
