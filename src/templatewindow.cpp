﻿#include "../inc/templatewindow.h"

TemplateWindow::TemplateWindow(uint32_t newDimension,
                               std::vector<QString> newCoordinateIDs,
                               QWidget *parent) :
    QWidget(parent),
    ui(new Ui::templateWindowForm)
{
    this->setAttribute(Qt::WA_DeleteOnClose);
    this->setMinimumSize(MIN_WIDTH,MIN_HEIGHT);
    ui->setupUi(this);
    resetFlag = false;
    maximumReachedFlag = false;
    pointsAttached = true;
    dimension = newDimension;
    coordinateIDs = newCoordinateIDs;
    oldCoordinates.assign(2*dimension + 1,0);
    setUpImageLabel();
    initialize();
    connect(&timer,SIGNAL(timeout()),this,SLOT(updateImage()));
    reset();
    timer.start(50);
}
void TemplateWindow::initialize(void)
{
}
void TemplateWindow::setWindowType(QString newType)
{
    settings.setWindowType(newType);
}
WindowSettings TemplateWindow::getSettings(void) const
{
    return settings;
}
void TemplateWindow::resizeEvent(QResizeEvent *event)
{
    delete pntr;
    ui->gridLayoutWidget->setFixedSize(event->size().width() - 50,
                                       event->size().height() - 20);
    ui->imageLabel->setFixedSize(ui->gridLayoutWidget->size().width() - ui->vMin->width() - 6,
                                 ui->gridLayoutWidget->size().height() - ui->hMin->height() - 6);
    pntr = new QImage(ui->imageLabel->size(), QImage::Format_RGB32);
    updateImage();
    doscalePrivate();
    updateLabels();
}
void TemplateWindow::updateGeometryFromHolder(const QRectF &newGeometry)
{
    settings.setGeometry(newGeometry);
}
QImage* TemplateWindow::getImage(void) const
{
    return pntr;
}
void TemplateWindow::setUpImageLabel(void)
{
    ui->vMin->setFixedSize(LABEL_FIXED_SIZE);
    ui->vMax->setFixedSize(LABEL_FIXED_SIZE);
    ui->hMin->setFixedSize(LABEL_FIXED_SIZE);
    ui->hMax->setFixedSize(LABEL_FIXED_SIZE);
    ui->imageLabel->setFixedSize(ui->gridLayoutWidget->width() - ui->vMin->width() - 1,
                                 ui->gridLayoutWidget->height() - ui->hMin->height() - 1);
    pntr = new QImage(ui->imageLabel->size(),QImage::Format_RGB32);
}
void TemplateWindow::drawPoint(QPointF point)
{
    if(
            (point.x() < ui->imageLabel->width()) &&
            (point.x() > 0) &&
            (point.y() < ui->imageLabel->height()) &&
            (point.y() > 0)
       )
    {
        pointBuffer.push_back(point);
    }
}
void TemplateWindow::drawPointNow(QPointF point)
{
    pointBufferFast.push_back(point);
}
void TemplateWindow::drawLineNow(QPointF start, QPointF end)
{
    lineBufferFast.push_back(QLineF(start,end));
}
void TemplateWindow::drawLine(QPointF start, QPointF end)
{
    QLineF line(start,end);
    lineBuffer.push_back(line);
}
double TemplateWindow::processPhaseH(double value) const
{
    if(settings.getHorizontalProcessing() == "Periodic")
    {
        return commonFunctions::modPeriod(value,
                settings.getHorizontalPeriod(),settings.getRangeX()[0]);
    }
    else if(settings.getHorizontalProcessing() == "Identical")
    {
        return value;
    }
    return 0;
//    else return userProcessingFunctionH(DS->Coordinate(settings.coordinateNumbers[0]));
}
double TemplateWindow::processPhaseV(double value) const
{
    if(settings.getVerticalProcessing() == "Periodic")
    {
        return commonFunctions::modPeriod(value,
                settings.getVerticalPeriod(),settings.getRangeY()[0]);
    }
    else if(settings.getVerticalProcessing() == "Identical")
    {
        return value;
    }
    return 0;
//    else return userProcessingFunctionV(DS->Coordinate(settings.coordinateNumbers[1]));
}
void TemplateWindow::setSettings(const WindowSettings &set)
{
    settings = set;
    settings.setPeriodicRangeX(settings.getRangeX());
    settings.setPeriodicRangeY(settings.getRangeY());
    doscalePrivate();
}
void TemplateWindow::reset(void)
{
    resetFlag = true;
}
void TemplateWindow::updateLabels(void)
{
    ;
}
void TemplateWindow::updateRange()
{
    ;
}
void TemplateWindow::setRunningX(bool running)
{
    isRunningX = running;
}
void TemplateWindow::setRunningY(bool running)
{
    isRunningY = running;
}
void TemplateWindow::doscale(void)
{
    updateRange();
    doscalePrivate();
}
void TemplateWindow::doscalePrivate(void)
{
}
void TemplateWindow::updateMultipliers(void)
{
    ;
}
void TemplateWindow::drawMarkMaximum(void)
{
    maximumReachedFlag = true;
}
void TemplateWindow::breakTrajectory()
{
    resetAttach = false;
}
void TemplateWindow::clearImage(void)
{
    lineBuffer.clear();
    pointBuffer.clear();
    pntr->fill(settings.getBackgroundColor());
    lineBufferFast.push_back(QLineF(QPointF(0,0),QPointF(pntr->width(),0)));
    lineBufferFast.push_back(QLineF(QPoint(0,0),QPoint(0,pntr->height())));
    lineBufferFast.push_back(QLineF(QPoint(0,pntr->height()-1),QPoint(pntr->width()-1,pntr->height()-1)));
    lineBufferFast.push_back(QLineF(QPoint(pntr->width()-1,0),QPoint(pntr->width()-1,pntr->height()-1)));
    breakTrajectory();
}
void TemplateWindow::updateImage()
{
    ui->imageLabel->setPixmap(QPixmap::fromImage(*pntr));
}
void TemplateWindow::updateScales(std::vector<double>)
{
    ;
}
TemplateWindow::~TemplateWindow()
{
    delete ui;
    delete pntr;
    LOG(INFO) << "Window deleted.";
}
void TemplateWindow::setLineWidth(uint32_t width)
{
    settings.setLineWidth(width);
}
void TemplateWindow::setPointSize(uint32_t size)
{
    settings.setPointSize(size);
}
void TemplateWindow::setPointsAttached(bool value)
{
    pointsAttached = value;
}
void TemplateWindow::draw(const std::vector<std::vector<double> >&)
{
    ;
}
void TemplateWindow::init_before_drawing(std::vector<double> coordinates)
{
    ;
}
void TemplateWindow::paintEvent(QPaintEvent *)
{
    QPainter painter(pntr);
    QPen pen(settings.getLineColor());
    painter.setBrush(Qt::SolidPattern);
    painter.setPen(pen);
    if(resetFlag){
        clearImage();
        resetFlag = false;
        emit paintFinished();
    }
    painter.setRenderHint(QPainter::Antialiasing);
    if(maximumReachedFlag)
    {
        painter.drawRect(QRectF(ui->imageLabel->width() - 15,0,15,15));
        maximumReachedFlag = false;
    }
    if(pointsAttached)
    {
        if(!lineBuffer.empty())
        {
            pen.setWidth(settings.getLineWidth());
            painter.setPen(pen);
            std::for_each(lineBuffer.begin(),lineBuffer.end(),[&painter](QLineF line){
                painter.drawLine(line);
            });
            lineBuffer.clear();
            emit paintFinished();
        }
    }
    else
    {
        if(!pointBuffer.empty())
        {
            pen.setWidth(settings.getPointSize());
            painter.setPen(pen);
            std::for_each(pointBuffer.begin(),pointBuffer.end(),[&painter,this](QPointF point){
                painter.drawEllipse(point,settings.getPointSize(),settings.getPointSize());//.drawPoint(point);
            });
            pointBuffer.clear();
            emit paintFinished();
        }
    }
    if(pointBufferFast.size() > 0)
    {
        pen.setWidth(settings.getPointSize());
        painter.setPen(pen);
        std::for_each(pointBufferFast.begin(),pointBufferFast.end(),[&painter](QPointF point){
            painter.drawPoint(point);
        });
        pointBufferFast.clear();
    }
    if(lineBufferFast.size() > 0)
    {
        pen.setWidth(settings.getLineWidth());
        painter.setPen(pen);
        std::for_each(lineBufferFast.begin(),lineBufferFast.end(),[&painter](QLineF line){
            painter.drawLine(line);
        });
        lineBufferFast.clear();
    }
}
