﻿#include "colormapdrawer.h"
#include "ui_colormapdrawer.h"

ColormapDrawer::ColormapDrawer(DynamicalSystem *DS, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ColormapDrawer),
    ds(DS)
{
    ui->setupUi(this);
    read_settings();
    this->setAttribute(Qt::WA_DeleteOnClose);
    setup_plot();
    ui->typeComboBox->addItems(QStringList() << tr("Временная диаграмма") << tr("Решётка"));
    ui->progressBar->setVisible(false);
    this->setWindowTitle(tr("2D colormap"));
}
ColormapDrawer::~ColormapDrawer()
{
    write_settings();
    delete ui;
}
void ColormapDrawer::read_settings()
{
    QSettings settings(QApplication::applicationDirPath() +
                       "/settings/colormap_settings/" + ds->SystemName() + ".ini",
                       QSettings::IniFormat);
    ui->timeLineEdit->setValue(settings.value("Integration_time", 100).toDouble());
    ui->stepLineEdit->setValue(settings.value("Integration_step", 0.01).toDouble());
}
void ColormapDrawer::write_settings()
{
    QSettings settings(QApplication::applicationDirPath() +
                       "/settings/colormap_settings/" + ds->SystemName() + ".ini",
                       QSettings::IniFormat);
    settings.setValue("Integration_time", ui->timeLineEdit->getValue());
    settings.setValue("Integration_step", ui->stepLineEdit->getValue());
}
void ColormapDrawer::on_closeButton_clicked()
{
    this->close();
}
void ColormapDrawer::setup_plot()
{
    ui->colormap_plot->setInteractions(QCP::iRangeDrag|QCP::iRangeZoom);
    ui->colormap_plot->axisRect()->setupFullAxesBox(true);
    ui->colormap_plot->xAxis->setLabel("Time");
    ui->colormap_plot->yAxis->setLabel("i");

    QCPColorMap *colorMap = new QCPColorMap(ui->colormap_plot->xAxis, ui->colormap_plot->yAxis);
    ui->colormap_plot->addPlottable(colorMap);
    int nx = qRound(ui->timeLineEdit->getValue() / ui->stepLineEdit->getValue()); // Number of steps for x coordinate
    int ny = ds->Dimension(); // number of elements for y coordinate
    colorMap->data()->setSize(nx, ny); // we want the color map to have nx * ny data points
    colorMap->data()->setRange(QCPRange(0, ui->timeLineEdit->getValue()),
                               QCPRange(0, ds->Dimension())); // and span the coordinate range -4..4 in both key (x) and value (y) dimensions

    double x, y, z;
    for (int xIndex = 0; xIndex < nx; ++xIndex)
    {
      for (int yIndex = 0; yIndex < ny; ++yIndex)
      {
        colorMap->data()->cellToCoord(xIndex, yIndex, &x, &y);
        double r = 3*qSqrt(x*x+y*y)+1e-2;
        z = 2*x*(qCos(r+2)/r-qSin(r+2)/r); // the B field strength of dipole radiation (modulo physical constants)
        colorMap->data()->setCell(xIndex, yIndex, z);
      }
    }

    QCPColorScale *colorScale = new QCPColorScale(ui->colormap_plot);
    ui->colormap_plot->plotLayout()->addElement(0, 1, colorScale); // add it to the right of the main axis rect
    colorScale->setType(QCPAxis::atRight); // scale shall be vertical bar with tick/axis labels right (actually atRight is already the default)
    colorMap->setColorScale(colorScale); // associate the color map with the color scale
    colorScale->axis()->setLabel(tr("Phase velocity"));

    colorMap->setGradient(QCPColorGradient::gpJet);
    colorMap->rescaleDataRange();

    QCPMarginGroup *marginGroup = new QCPMarginGroup(ui->colormap_plot);
    ui->colormap_plot->axisRect()->setMarginGroup(QCP::msBottom|QCP::msTop, marginGroup);
    colorScale->setMarginGroup(QCP::msBottom|QCP::msTop, marginGroup);

    ui->colormap_plot->rescaleAxes();
}
void ColormapDrawer::on_resetScalePushButton_clicked()
{
    rescale_plot();
}
void ColormapDrawer::rescale_plot()
{
    LOG(INFO) << "ColormapDrawer: plot is being rescaled.";
    ui->colormap_plot->yAxis->setRange(QCPRange(0, ds->Dimension()));
    ui->colormap_plot->xAxis->setRange(QCPRange(0, ui->timeLineEdit->getValue()));
    ui->colormap_plot->replot();
}

void ColormapDrawer::calculate_spatiotemporal_figure()
{
    LOG(INFO) << "ColormapDrawer: Calculations started.";
    int nx = qRound(ui->timeLineEdit->getValue() / ui->stepLineEdit->getValue()); // Number of steps for x coordinate
    int ny = ds->Dimension(); // number of elements for y coordinate
    ui->progressBar->setVisible(true);
    ui->progressBar->setRange(0, nx);
    auto colorMap = static_cast<QCPColorMap*>(ui->colormap_plot->plottable());
    colorMap->clearData();
    colorMap->data()->setSize(nx, ny);
    colorMap->data()->setRange(QCPRange(0, ui->timeLineEdit->getValue()),
                               QCPRange(0, ds->Dimension()));
    // At first we get multidimensional curve
    std::vector<std::vector<double>> integralCurve = ds->getIntegralCurve(ds->Coordinates(),
                                                                          ui->timeLineEdit->getValue(),
                                                                          ui->stepLineEdit->getValue());
    for (int xIndex = 0; xIndex < nx; ++xIndex)
    {
      for (int yIndex = 0; yIndex < ny; ++yIndex)
      {
         colorMap->data()->setCell(xIndex, yIndex, log10(fabs(integralCurve[xIndex][yIndex + ds->Dimension()])));
      }
      ui->progressBar->setValue(xIndex);
      QApplication::processEvents();
    }
    rescale_plot();
    ui->progressBar->setVisible(false);
    LOG(INFO) << "ColormapDrawer: Calculations finished.";
}

void ColormapDrawer::on_calculatePushButton_clicked()
{
    calculate_spatiotemporal_figure();
}

void ColormapDrawer::on_saveImagePushButton_clicked()
{
    QString file_name = QFileDialog::getSaveFileName(nullptr, QString("Save image"), QApplication::applicationDirPath(), "images (*.pdf)");
    if(file_name.isEmpty())
    {
        return;
    }
    ui->colormap_plot->savePdf(file_name);
    LOG(INFO) << "ColormapDrawer: image saved.";
}
