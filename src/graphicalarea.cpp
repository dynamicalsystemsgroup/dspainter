﻿#include "../inc/graphicalarea.h"
using namespace GraphicalAreaNS;
GraphicalArea::GraphicalArea(QWidget *parent) : QWidget(parent)
{
    QPalette pal;
    pal.setBrush(QPalette::Window,QBrush(Qt::white));
    this->setPalette(pal);// Setting background color to white
    this->setMouseTracking(true);
    this->setAutoFillBackground(true);// force paint engine to refill background every paintEvent
//    this->setStyleSheet("background: white");
    // All states are finished, starting mode = draw mode
    mode = draw;
    drawingState = finished;
    relocatingState = finished;
    resizeState = resizeFinished;
    LOG(INFO)<<"Graphical area created.";
}
GraphicalArea::~GraphicalArea()
{
    ;// TODO: check if all the memory is deallocated
}
void GraphicalArea::init_before_drawing(std::vector<double> coordinates)
{
    foreach (WindowHolder *holder, this->listOfWindows) {
        if(holder->WindowType() == localWindowType::cascadeDrawer)
        {
            static_cast<PoincarePainter*>(holder->widget())->init_before_drawing(coordinates);
        }
    }
    LOG(INFO)<<"GraphicalArea: Windows are initialized for drawing.";
}
void GraphicalArea::setDSProperties(uint32_t newDimension, std::vector<QString> newCoordinateIDs)
{
    dimensionForWindows = newDimension;
    coordinateIDForWindows = newCoordinateIDs;
    LOG(INFO)<<"GraphicalArea: Dimensions and IDs are changed.";
}
void GraphicalArea::rescale(void)
{
    // rescale all windows, each type individually
    foreach (WindowHolder *w, listOfWindows)
    {
        static_cast<TemplateWindow*>(w->widget())->doscale();
//        if(w->WindowType() == flowDrawer)
//        {
//            static_cast<Painter*>(w->widget())->doscale();
//        }
//        else if(w->WindowType() == flowDrawer)
//        {
//            static_cast<Painter*>(w->widget())->doscale();
//        }
//        else if(w->WindowType() == spatialDrawer)
//        {
//            static_cast<SpatialPainter*>(w->widget())->doscale();
//        }
    }
}
void GraphicalArea::reset(void)
{
    //reset images for every window
    foreach (WindowHolder *w, listOfWindows)
    {
        static_cast<TemplateWindow*>(w->widget())->reset();
    }
}
void GraphicalArea::breakTrajectories(void)
{
    // Break trajectory made to prevent drawing of redundant lines
    foreach (WindowHolder *wind, listOfWindows)
    {
        static_cast<TemplateWindow*>(wind->widget())->breakTrajectory();
    }
}
void GraphicalArea::createWindow(const QPoint &pos, const QSize &size, localWindowType type)
{
    if( (size.width() > MIN_WIDTH_LOCAL) &&
            (size.height() > MIN_HEIGHT_LOCAL))
    {
        listOfWindows << new WindowHolder(dimensionForWindows,coordinateIDForWindows,
                                        type,this);
        listOfWindows.last()->setGeometry(QRect(pos,size));
        listOfWindows.last()->show();
        connect(listOfWindows.last(),SIGNAL(paintFinished()),this,SLOT(collectFinishedSignals()));
        LOG(WARNING)<<"GraphicalArea: window created.";
    }
    else
    {
        LOG(WARNING)<<"GraphicalArea: window is too small.";
    }
}
void GraphicalArea::createWindow(const WindowSettings &settings)
{
    if(settings.getWindowType() == "FlowDrawer")
    {
        createWindow(QPoint(settings.getGeometry().x(),settings.getGeometry().y()),
                     QSize((int)settings.getGeometry().width(),(int)settings.getGeometry().height()),
                     flowDrawer);
        if(listOfWindows.length() > 0)
        {
            static_cast<Painter*>(listOfWindows.last()->widget())->setSettings(settings);
        }
    }
    else if(settings.getWindowType() == "CascadeDrawer")
    {
        createWindow(QPoint(settings.getGeometry().x(),settings.getGeometry().y()),
                     QSize((int)settings.getGeometry().width(),(int)settings.getGeometry().height()),
                     cascadeDrawer);
        if(listOfWindows.length() > 0)
        {
            static_cast<PoincarePainter*>(listOfWindows.last()->widget())->setSettings(settings);
        }
    }
//    else if(settings.getWindowType() == "SpatialDrawer")
//    {
//        createWindow(QPoint(settings.getGeometry().x(),settings.getGeometry().y()),
//                     QSize((int)settings.getGeometry().width(),(int)settings.getGeometry().height()),
//                     flowDrawer);
//        if(listOfWindows.length() > 0)
//        {
//            static_cast<SpatialPainter*>(listOfWindows.last()->widget())->setSettings(settings);
//        }
//    }
    else
    {
        LOG(WARNING)<<"GraphicalArea: unknown type of window.";
    }
}
void GraphicalArea::collectFinishedSignals(void)
{
    static uint32_t signalsRecieved = 0;
    signalsRecieved++;
    if(signalsRecieved == (uint32_t)this->listOfWindows.size())
    {
        emit paintFinished();
        signalsRecieved = 0;
    }
}
void GraphicalArea::drawBuffer(std::vector<std::vector<double> > &buffer)
{
    if(listOfWindows.isEmpty()){
        emit paintFinished();
        return;
    }
    foreach (WindowHolder *w, listOfWindows)
    {
        static_cast<TemplateWindow *>(w->widget())->draw(buffer);
//        if(w->WindowType() == flowDrawer){
//            static_cast<Painter *>(w->widget())->draw(buffer);
//        }
//        else if(w->WindowType() == spatialDrawer){
//            static_cast<SpatialPainter *>(w->widget())->draw(buffer);
//        }
//        else if(w->WindowType() == cascadeDrawer){
//            static_cast<PoincarePainter *>(w->widget())->draw(buffer);
//        }
    }
}
QList<WindowHolder *> GraphicalArea::getSelectionList(void) const
{
    return selectionList;
}
QList<WindowHolder*> GraphicalArea::getWindowsList(void) const
{
    foreach (WindowHolder* w, listOfWindows) {
        w->configureGeometry();
    }
    return listOfWindows;
}
void GraphicalArea::closeAllWindows(void)
{
    foreach (WindowHolder *w, listOfWindows)
    {
        w->close();
    }
    listOfWindows.clear();
    selectionList.clear();
    emit selectionChanged();
    this->repaint();
    LOG(INFO)<<"GraphicalArea: All windows closed.";
}
void GraphicalArea::setNewWindowMode(void)
{
    mode = draw;
    selectionList.clear();
    repaint();
    emit selectionChanged();
    emit graphicalModeChanged(mode);
    LOG(INFO)<<"GraphicalArea: New window mode is set.";
}
void GraphicalArea::setEditMode(void)
{
    mode = select;
    drawingState = finished;
    repaint();
    emit graphicalModeChanged(mode);
    LOG(INFO)<<"GraphicalArea: Edit mode is set.";
}
void GraphicalArea::setLockMode(void)
{
    mode = lock;
    relocatingState = finished;
    selectionList.clear();
    repaint();
    emit selectionChanged();
    emit graphicalModeChanged(mode);
    LOG(INFO)<<"GraphicalArea: Lock mode is set.";
}
void GraphicalArea::mousePressEvent(QMouseEvent *event)
{
    if(event->button() == Qt::MiddleButton)
    {
        this->setEditMode();
        LOG(INFO)<<"GraphicalArea: Edit mode is set.";
    }
    if(event->button() == Qt::LeftButton)
    {
        switch (mode)
        {
        case draw:
            if(drawingState == finished)
            {
                LOG(INFO)<<"GraphicalArea: Drawing started.";
                drawingState = started;
                currentRectangle.setCoords(event->pos().x(),event->pos().y(),
                                           event->pos().x(),event->pos().y());
            }
            break;
        case select:
            relocateStartPosition = event->pos();
            if(selectionList.size() == 1)
            {
                // Check if user pulling edges of the window
                if( (abs(selectionList.first()->pos().y() - event->pos().y()) < 5 ) &&
                        (event->pos().x() > selectionList.first()->pos().x()) &&
                        (event->pos().x() < selectionList.first()->pos().x() + selectionList.first()->width())
                        )
                {
                    resizeState = startedTop;
                    resizeStartPosition = event->pos();
                }
                if( (abs(selectionList.first()->pos().y() + selectionList.first()->height() - event->pos().y()) < 5 ) &&
                        (event->pos().x() > selectionList.first()->pos().x()) &&
                        (event->pos().x() < selectionList.first()->pos().x() + selectionList.first()->width())
                        )
                {
                    resizeState = startedBottom;
                    resizeStartPosition = event->pos();
                }
                if( (abs(selectionList.first()->pos().x() - event->pos().x()) < 5) &&
                        (event->pos().y() > selectionList.first()->pos().y()) &&
                        (event->pos().y() < selectionList.first()->pos().y() + selectionList.first()->height())
                        )
                {
                    resizeState = startedLeft;
                    resizeStartPosition = event->pos();
                }
                if( (abs(selectionList.first()->pos().x()+ selectionList.first()->width() - event->pos().x()) < 5) &&
                        (event->pos().y() > selectionList.first()->pos().y()) &&
                        (event->pos().y() < selectionList.first()->pos().y() + selectionList.first()->height()))
                {
                    resizeState = startedRight;
                    resizeStartPosition = event->pos();
                }
                if( ( abs(selectionList.first()->pos().x() + selectionList.first()->width() - event->pos().x()) < 5 ) &&
                        ( abs(selectionList.first()->pos().y() + selectionList.first()->height() - event->pos().y()) < 5 ))
                {
                    resizeState = startedBottomRight;
                    resizeStartPosition = event->pos();
                }
                if( ( abs(selectionList.first()->pos().x() - event->pos().x()) < 5 ) &&
                        ( abs(selectionList.first()->pos().y() + selectionList.first()->height() - event->pos().y()) < 5 ))
                {
                    resizeState = startedBottomLeft;
                    resizeStartPosition = event->pos();
                }
                if( ( abs(selectionList.first()->pos().x() - event->pos().x()) < 5 ) &&
                        ( abs(selectionList.first()->pos().y() - event->pos().y()) < 5 ))
                {
                    resizeState = startedTopLeft;
                    resizeStartPosition = event->pos();
                }
                if( ( abs(selectionList.first()->pos().x() + selectionList.first()->width() - event->pos().x()) < 5 ) &&
                        ( abs(selectionList.first()->pos().y() - event->pos().y()) < 5 ))
                {
                    resizeState = startedTopRight;
                    resizeStartPosition = event->pos();
                }
            }
            break;
        case lock:
            break;
        default:
            break;
        }
    }
    event->ignore();
}
void GraphicalArea::keyPressEvent(QKeyEvent *event)
{
    // Pass key events up
    emit keyPressed(event);
}
void GraphicalArea::alignTop(void)
{
    if(selectionList.size() < 2) return;
    foreach (WindowHolder *w, selectionList)
    {
        w->setGeometry(w->x(),selectionList.first()->y(),
                       w->width(),w->height());
        this->repaint();
    }
}
void GraphicalArea::alignLeft(void)
{
        if(selectionList.size() < 2) return;
        foreach (WindowHolder *w, selectionList)
        {
            w->setGeometry(selectionList.first()->x(),w->y(),
                           w->width(),w->height());
        }
        this->repaint();
}
void GraphicalArea::deleteSelected(void)
{
    foreach (WindowHolder *w, selectionList) {
        w->close();
        selectionList.removeOne(w);
        listOfWindows.removeOne(w);
    }
    emit selectionChanged();
    this->repaint();
}
void GraphicalArea::dublicateSelectedWindow(void)
{
    ;
}
void GraphicalArea::mouseReleaseEvent(QMouseEvent *event)
{
    switch (mode) {
    case draw:
        if(event->button() == Qt::LeftButton)
        {
            if(mode == draw)
            {
                drawingState = finished;
                this->repaint();
                if( (currentRectangle.width() >= MIN_WIDTH_LOCAL) &&
                        (currentRectangle.height() >= MIN_HEIGHT_LOCAL))
                {
                    createWindow(currentRectangle.normalized().topLeft(),
                                 currentRectangle.normalized().size(),
                                 noType);
                }
            }
        }
        break;
    case select:
        if(event->button() == Qt::LeftButton)
        {
            if(resizeState != resizeFinished)
            {
                resizeState = resizeFinished;
                if( (selectionList.last()->width() < MIN_WIDTH_LOCAL) &&
                       (selectionList.last()->height() < MIN_HEIGHT_LOCAL) )
                {
                    selectionList.last()->resize(QSize(MIN_WIDTH_LOCAL,MIN_HEIGHT_LOCAL));
                }
                else if(selectionList.last()->width() < MIN_WIDTH_LOCAL)
                {
                    selectionList.last()->resize(QSize(MIN_WIDTH_LOCAL,selectionList.last()->height()));
                }
                else if(selectionList.last()->height() < MIN_HEIGHT_LOCAL)
                {
                    selectionList.last()->resize(QSize(selectionList.last()->width(),MIN_HEIGHT_LOCAL));
                }
            }
            else if(relocatingState != started)
            {
                bool anySelected = false;
                foreach (WindowHolder *w, listOfWindows)
                {
                    if(w->underMouse())
                    {
                        if(event->modifiers() & Qt::ShiftModifier)
                        {
                            selectionList << w;
                        }
                        else
                        {
                            selectionList.clear();
                            selectionList << w;
                        }
                        if(!anySelected)
                        {
                            anySelected = true;
                        }
                    }
                }
                if(!anySelected) selectionList.clear();
                emit selectionChanged();
            }
            else
            {
                relocatingState = finished;
                QApplication::setOverrideCursor(QCursor(Qt::ArrowCursor));
            }
            repaint();
        }
        break;
    default:
        break;
    }
}
void GraphicalArea::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    switch (mode) {
    case draw:
        if( (drawingState == started) ){
            painter.setRenderHint(QPainter::Antialiasing,true);
            painter.setPen(QPen(QColor(50, 153, 204)/*Qt::darkBlue*/,3,Qt::DashLine));
            painter.setBrush(QBrush(Qt::darkBlue,Qt::DiagCrossPattern));
            painter.drawRect(currentRectangle);
        }
        break;
    case select:
        break;
    case lock:
        ;
        break;
    default:
        break;
    }
    if(!selectionList.isEmpty())
    {
        painter.setRenderHint(QPainter::Antialiasing,true);
        painter.setPen(QPen(QColor(50, 153, 204)/*Qt::darkBlue*/,5,Qt::DashLine));
        foreach (QWidget *w, selectionList)
        {
            painter.drawRect(w->x() - 2, w->y() - 2,
                                  w->width() + 4, w->height() + 4);
        }
    }
    int maxX = 0, maxY = 0;
    foreach (WindowHolder *w, listOfWindows)
    {
        if( (w->x() + w->width()) > maxX )
        {
            maxX = w->x() + w->width();
        }
        if( (w->y() + w->height()) > maxY )
        {
            maxY = w->y() + w->height();
        }
    }
    // Magic numbers: reconsider
    if(maxX > QApplication::desktop()->screenGeometry().width() - 100) this->setFixedWidth(maxX + 100);
    else this->setFixedWidth(QApplication::desktop()->screenGeometry().width());
    if(maxY > QApplication::desktop()->screenGeometry().height() - 100) this->setFixedHeight(maxY + 100);
    else this->setFixedHeight(QApplication::desktop()->screenGeometry().height());
}
void GraphicalArea::mouseMoveEvent(QMouseEvent *event)
{
    switch (mode)
    {
    case draw:
        if( drawingState == started)
        {
            currentRectangle.setBottomRight(event->pos());
            this->repaint();
        }
        break;
    case select:
        if(resizeState == resizeFinished){
            bool anySelectedUnderCursor = false;
            foreach (WindowHolder *w, selectionList)
            {
                if(w->underMouse())
                {
                    if(!anySelectedUnderCursor) anySelectedUnderCursor = true;
                    break;
                }
            }
            if( (event->buttons() & Qt::LeftButton) & anySelectedUnderCursor)
            {
                relocatingState = started;
                QApplication::setOverrideCursor(QCursor(Qt::SizeAllCursor));
                foreach (WindowHolder *w, selectionList)
                {
                    w->move(w->pos() + event->pos() - relocateStartPosition);
                    this->repaint();
                }
                relocateStartPosition = event->pos();
            }
        }
        else
        {
            if(resizeState == startedBottom)
            {
                // pulling bottom edge => just increasing height of the window
                selectionList.last()->resize(QSize(selectionList.last()->width(),
                                                   selectionList.last()->height() + event->pos().y() - resizeStartPosition.y()));
                resizeStartPosition = event->pos();
                this->repaint();
            }
            if(resizeState == startedTop)
            {
                // pulling top edge => Resize and move top edge of the window
                selectionList.last()->move(QPoint(selectionList.last()->x(),
                                                  selectionList.last()->y() + event->pos().y() - resizeStartPosition.y()));
                selectionList.last()->resize(QSize(selectionList.last()->width(),
                                                   selectionList.last()->height() - event->pos().y()+ resizeStartPosition.y()));
                //qDebug()<<"T resize";
                resizeStartPosition = event->pos();
                this->repaint();
            }
            if(resizeState == startedLeft)
            {
                // pulling left edge => Resize and move left edge of the window
                selectionList.last()->move(QPoint(selectionList.last()->x()+ event->pos().x() - resizeStartPosition.x(),
                                                  selectionList.last()->y()));
                selectionList.last()->resize(QSize(selectionList.last()->width() - event->pos().x() + resizeStartPosition.x(),
                                                   selectionList.last()->height()));
                //qDebug()<<"L resize";
                resizeStartPosition = event->pos();
                this->repaint();
            }
            if(resizeState == startedRight)
            {
                // pulling right edge => Just increase width of the window
                selectionList.last()->resize(QSize(selectionList.last()->width() + event->pos().x() - resizeStartPosition.x(),
                                                   selectionList.last()->height()));
                resizeStartPosition = event->pos();
                this->repaint();
            }
            if(resizeState == startedBottomRight)
            {
                // pulling bottom right point of the window => Increase both width and height
                selectionList.last()->resize(QSize(selectionList.last()->width() + event->pos().x() - resizeStartPosition.x(),
                                                   selectionList.last()->height() + event->pos().y() - resizeStartPosition.y()));
                resizeStartPosition = event->pos();
                this->repaint();
            }
            if(resizeState == startedBottomLeft)
            {
                // pulling bottom left point of the window => Resize and move left edge of the window
                selectionList.last()->move(QPoint(selectionList.last()->x()+ event->pos().x() - resizeStartPosition.x(),
                                                  selectionList.last()->y()));
                selectionList.last()->resize(QSize(selectionList.last()->width() - event->pos().x() + resizeStartPosition.x(),
                                                   selectionList.last()->height() + event->pos().y() - resizeStartPosition.y()));
                resizeStartPosition = event->pos();
                this->repaint();
            }
            if(resizeState == startedTopLeft)
            {
                // pulling top left point of the window => Resize and move left and top edges of the window
                selectionList.last()->move(QPoint(selectionList.last()->x() + event->pos().x() - resizeStartPosition.x(),
                                                  selectionList.last()->y() + event->pos().y() - resizeStartPosition.y()));
                selectionList.last()->resize(QSize(selectionList.last()->width() - event->pos().x() + resizeStartPosition.x(),
                                                   selectionList.last()->height() - event->pos().y() + resizeStartPosition.y()));
                //qDebug()<<"TL resize";
                resizeStartPosition = event->pos();
                this->repaint();
            }
            if(resizeState == startedTopRight)
            {
                // pulling top right point of the window => Resize and  move top edge of the widow
                selectionList.last()->move(QPoint(selectionList.last()->x(),
                                                  selectionList.last()->y() + event->pos().y() - resizeStartPosition.y()));
                selectionList.last()->resize(QSize(selectionList.last()->width() + event->pos().x() - resizeStartPosition.x(),
                                                   selectionList.last()->height() - event->pos().y() + resizeStartPosition.y()));
                //qDebug()<<"TR resize";
                resizeStartPosition = event->pos();
                this->repaint();
            }
        }
    break;
    default:
        break;
    }
}
