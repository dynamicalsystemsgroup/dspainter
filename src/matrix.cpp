#include "../inc/matrix.h"

namespace mathematics{

matrix::matrix(){
    this->rows = 0;
    this->columns = 0;
}
matrix::matrix(uint32_t rowNumber, uint32_t columnNumber){
    this->rows = rowNumber;
    this->columns = columnNumber;
    for(uint32_t i = 0; i<this->rows*this->columns; ++i){
        data.push_back(0);
    }
}
matrix::matrix(const matrix &m){
    this->rows = m.rows;
    this->columns = m.columns;
    this->data = m.data;
}
matrix matrix::random(uint32_t row, uint32_t column, double rangeMin, double rangeMax)
{
    matrix rM(row,column);
    for(uint32_t i = 0; i<rM.Rows(); ++i){
        for(uint32_t j = 0; j<rM.Columns(); ++j){
            rM(i,j) = (rangeMax - rangeMin)*((double)rand()/RAND_MAX) + rangeMin;
        }
    }
    return rM;
}
matrix &matrix::operator=(matrix right){
    this->rows = right.rows;
    this->columns = right.columns;
    this->data = right.data;
    return *this;
}
double &matrix::operator()(uint32_t row, uint32_t column){
    return this->data[column*this->rows + row];
}
matrix matrix::eye(uint32_t row, uint32_t column){
    matrix retM(row,column);
    for(uint32_t i = 0; i<retM.rows; ++i){
        retM(i,i) = 1;
    }
    return retM;
}
matrix matrix::transpose(){
    matrix temp(this->columns,this->rows);
    for(uint32_t i = 0; i < this->rows; ++i)
        for(uint32_t j = 0; j < this->columns; ++j)
            temp(j,i) = this->operator ()(i,j);
    return temp;
}
matrix matrix::inverse(){
//    const uint32_t COLUMN_ORDER = 1;
    // Нужно ли это вообще?
    matrix inverseM(*this);
//    int pivots[9];//[inverseM.Rows()];
//    for(uint32_t i = 0; i<inverseM.Rows(); ++i) pivots[i] = 0;
//    LAPACKE_dgetri(LAPACK_COL_MAJOR,COLUMN_ORDER,inverseM.getDoubleData(),
//                   inverseM.Rows(),pivots);
    return inverseM;
}
uint32_t matrix::Rows(){
    return this->rows;
}
uint32_t matrix::Columns(){
    return this->columns;
}
double matrix::norm(){
    double retNorm = 0;
    for(uint32_t i = 0; i<this->data.size();++i) retNorm += this->data[i] * this->data[i];
    retNorm = sqrt(retNorm);
    return retNorm;
}
double *matrix::getDoubleData(){
    return this->data.data();
}
std::vector<double> matrix::getStdVectorData(void){
    std::vector<double> retVector = this->data;
    return retVector;
}
void eigenData(matrix input, matrix &eigenValuesRe,
               matrix &eigenValuesIm, matrix &eigenVectors)
{
    if(input.Rows() != input.Columns()) {
        LOG(INFO)<<"matrix::eigenData() nonsquare matrix!";
    }
    Eigen::MatrixXd eigenStorage(input.Rows(),input.Columns());
    eigenValuesRe = matrix(input.Rows(),1);
    eigenValuesIm = matrix(input.Rows(),1);
    eigenVectors = matrix(input);
    for(uint32_t i = 0; i < input.Rows(); ++i) {
        for(uint32_t j = 0; j < input.Columns(); ++j) {
            eigenStorage(i,j) = input(i,j);
        }
    }
    Eigen::EigenSolver<Eigen::MatrixXd> solver(eigenStorage);
    Eigen::MatrixXcd eigenValuesLocal = solver.eigenvalues();
    Eigen::MatrixXcd eigenVectorsLocal = solver.eigenvectors();
    for(uint32_t i = 0; i < input.Rows(); ++i) {
        for(uint32_t j = 0; j < input.Columns(); ++j) {
            eigenVectors(i,j) = eigenVectorsLocal(i,j).real() + eigenVectorsLocal(i,j).imag();// CHECK
        }
        eigenValuesRe(i,0) = eigenValuesLocal(i,0).real();
        eigenValuesIm(i,0) = eigenValuesLocal(i,0).imag();
    }
    sortEigenDescending(eigenValuesRe,eigenValuesIm,eigenVectors);
}
void sortEigenDescending(matrix &valuesRe, matrix &valuesIm, matrix &vectors)
{
    std::vector<eigenUtilary> arrayForSorting;
    matrix valuesReRef = valuesRe;
    matrix valuesImRef = valuesIm;
    matrix vectorsRef = vectors;
    for(uint32_t i = 0; i < valuesRe.Rows(); ++i){
        arrayForSorting.push_back(eigenUtilary(valuesRe(i,0),i));
    }
    std::sort(arrayForSorting.begin(),arrayForSorting.end(),[](eigenUtilary left, eigenUtilary right){
        return (left.data > right.data);
    });
    for(uint32_t i = 0; i < valuesRe.Rows(); ++i){
        valuesRe(i,0) = valuesReRef(arrayForSorting[i].number,0);
        valuesIm(i,0) = valuesImRef(arrayForSorting[i].number,0);
        vectors(i,0) = vectorsRef(arrayForSorting[i].number,0);
    }
}
matrix solve(matrix A, matrix B)
{
    if(A.Columns() != A.Rows()) {
        LOG(WARNING)<<"solve: nonsquare matrix!";
    }
    if(A.Rows() != B.Rows()) {
        LOG(WARNING)<<"solve: dimensions mismatch!";
    }
    Eigen::MatrixXd linearSystem(A.Rows(),A.Columns());
    Eigen::VectorXd rightHandSide(A.Rows());
    Eigen::VectorXd result(A.Rows());
    for(uint32_t i = 0; i < A.Rows(); ++i) {
        for(uint32_t j = 0; j < A.Columns(); ++j) {
            linearSystem(i,j) = A(i,j);
        }
        rightHandSide(i) = B(i,0);
    }
    result = linearSystem.colPivHouseholderQr().solve(rightHandSide);
    for(uint32_t i = 0; i < B.Rows(); ++i) {
        B(i,0) = result(i);
    }
    return B;
}

}
