#include "../inc/spatialpainter.h"

SpatialPainter::SpatialPainter(uint32_t newDimension,
                               std::vector<QString> newCoordinateIDs,
                               QWidget *parent) : TemplateWindow(newDimension,newCoordinateIDs,parent){
    this->setPointsAttached(false);
    this->initialize();
    timeLag = 2;
    step = 0.01;
    settings.setVisualizationRange(0,1);
    settings.setVisualizationRange(1,dimension);
}
void SpatialPainter::setTimeLag(const double &newTimeLag){
    timeLag = newTimeLag;
}
void SpatialPainter::setStep(const double &newStep){
    if(step == 0){
        LOG(FATAL)<<"SpatialPainter: new step is equal to zero.";
    }
    step = newStep;
}
double SpatialPainter::getTimeLag(void) const{
    return timeLag;
}
void SpatialPainter::initialize(void){
    resetAttach = true;
    isRunningY = false;
    settings.setScaleY(0,-10);
    settings.setScaleY(1,10);
    settings.setRangeY(settings.getScalesY());
    settings.setMultiplier(1,(this->getImage()->height())/fabs(settings.getScalesY()[0]-settings.getScalesY()[1]));
}
void SpatialPainter::doscalePrivate(void){
    reset();
    this->updateMultipliers();
    this->updateLabels();
}
void SpatialPainter::doscale(void){
    this->updateRange();
    this->doscalePrivate();
}
void SpatialPainter::setSettings(const WindowSettings &set){
    settings = set;
    settings.setPeriodicRangeX(settings.getRangeX());
    settings.setPeriodicRangeY(settings.getRangeY());
    this->setTimeLag(settings.getTimeLag());
    if(settings.getVisualizationRange()[0] > settings.getVisualizationRange()[1]){
        LOG(WARNING)<<"SpatialPainter: visualisation range is invalid";
    }
    this->doscalePrivate();
}
void SpatialPainter::draw(const std::vector<std::vector<double> > &buffer){
    if(buffer.empty()){
        emit paintFinished();
        return;
    }
    std::vector<double> tempScale;
    for(uint32_t i = 0; i < buffer.size(); i += (uint32_t)(timeLag/step)){
        tempScale.assign(2,processPhaseV(buffer[i][settings.getCoordinateNumbers()[1]]));
        current.rx() = 1 + this->getImage()->width()/dimension/2;
        settings.setCoordinateNumbers(1,settings.getVisualizationRange()[0]);
        if(!settings.getRenderDerivatives()){
            current.ry() = -settings.getMultipliers()[1]*(
                        processPhaseV(buffer[i][settings.getCoordinateNumbers()[1]])
                    + settings.getScaleFactors()[1]) + this->getImage()->height()/2;
            if(tempScale[0] > processPhaseV(buffer[i][settings.getCoordinateNumbers()[1]]))
                tempScale[0] = processPhaseV(buffer[i][settings.getCoordinateNumbers()[1]]);
            if(tempScale[1] < processPhaseV(buffer[i][settings.getCoordinateNumbers()[1]]))
                tempScale[1] = processPhaseV(buffer[i][settings.getCoordinateNumbers()[1]]);
        }
        else{
            current.ry() = -settings.getMultipliers()[1]*(
                        processPhaseV(buffer[i][settings.getCoordinateNumbers()[1] + dimension])
                    + settings.getScaleFactors()[1]) + this->getImage()->height()/2;
            if(tempScale[0] > processPhaseV(buffer[i][settings.getCoordinateNumbers()[1] + dimension]))
                tempScale[0] = processPhaseV(buffer[i][settings.getCoordinateNumbers()[1] + dimension]);
            if(tempScale[1] < processPhaseV(buffer[i][settings.getCoordinateNumbers()[1] + dimension]))
                tempScale[1] = processPhaseV(buffer[i][settings.getCoordinateNumbers()[1] + dimension]);
        }
        pointBuffer.push_back(current);
        for(uint32_t j = settings.getVisualizationRange()[0] - 1; j < settings.getVisualizationRange()[1]; ++j){
            current.rx() = (double)j * this->getImage()->width()/(dimension) +
                    this->getImage()->width()/dimension/2;
            settings.setCoordinateNumbers(1,j);
            if(!settings.getRenderDerivatives()){
                current.ry() = -settings.getMultipliers()[1]*(
                            processPhaseV(buffer[i][settings.getCoordinateNumbers()[1]])
                        + settings.getScaleFactors()[1]) + this->getImage()->height()/2;
            }
            else{
                current.ry() = -settings.getMultipliers()[1]*(
                            processPhaseV(buffer[i][settings.getCoordinateNumbers()[1] + dimension])
                        + settings.getScaleFactors()[1]) + this->getImage()->height()/2;
            }
            pointBuffer.push_back(current);
//            if(settings.getJoinPoints()){
//                lineBuffer.push_back(QLineF(pointBuffer[i-1],pointBuffer[i]));
//            }
            if(!settings.getRenderDerivatives()){
                if(tempScale[0] > processPhaseV(buffer[i][settings.getCoordinateNumbers()[1]]))
                    tempScale[0] = processPhaseV(buffer[i][settings.getCoordinateNumbers()[1]]);
                if(tempScale[1] < processPhaseV(buffer[i][settings.getCoordinateNumbers()[1]]))
                    tempScale[1] = processPhaseV(buffer[i][settings.getCoordinateNumbers()[1]]);
            }
            else{
                if(tempScale[0] > processPhaseV(buffer[i][settings.getCoordinateNumbers()[1] + dimension]))
                    tempScale[0] = processPhaseV(buffer[i][settings.getCoordinateNumbers()[1] + dimension]);
                if(tempScale[1] < processPhaseV(buffer[i][settings.getCoordinateNumbers()[1] + dimension]))
                    tempScale[1] = processPhaseV(buffer[i][settings.getCoordinateNumbers()[1] + dimension]);
            }
        }
    }
    oldCoordinates = buffer.back();
    this->updateScales(tempScale);
}
void SpatialPainter::updateScales(std::vector<double> scales){
    if(settings.getScalesY()[0] > scales[0]) settings.setScaleY(0,scales[0]);
    if(settings.getScalesY()[1] < scales[1]) settings.setScaleY(1,scales[1]);
}
void SpatialPainter::updateLabels(void){
    ui->xLabel->setText(tr("Coordinate number"));
    ui->yLabel->setText(tr("Amplitude"));
    if(settings.getVerticalProcessing() != "Periodic"){
        ui->vMin->setText(QString::number(settings.getRangeY()[0],'f',3));
        ui->vMax->setText(QString::number(settings.getRangeY()[1],'f',3));
    }
    else{
        ui->vMin->setText(QString::number(settings.getPeriodicRangeY()[0],'f',3));
        ui->vMax->setText(QString::number(settings.getPeriodicRangeY()[1],'f',3));
    }
    ui->hMin->setText(QString::number(1));
    ui->hMax->setText(QString::number(dimension));
    this->update();
}
void SpatialPainter::updateMultipliers(void){
    if(settings.getCoordinateNumbers()[1] == 2*dimension){//If Time - simply update multipliers
        settings.setMultiplier(1,(this->getImage()->height())/fabs(settings.getRangeY()[1] - settings.getRangeY()[0]));
        settings.setScaleFactor(1,-(settings.getRangeY()[1] + settings.getRangeY()[0])/2);
    }
    else{
        if(settings.getVerticalProcessing() == "Periodic"){
            if((fabs(settings.getScalesY()[1]-settings.getScalesY()[0])>1e-8)){
                if(fabs(settings.getPeriodicRangeY()[1] - settings.getPeriodicRangeY()[0]) > 0){
                    settings.setMultiplier(1,(this->getImage()->height())/fabs(settings.getPeriodicRangeY()[1] - settings.getPeriodicRangeY()[0]));
                }
                else{
                    settings.setMultiplier(1,1);
                    LOG(WARNING)<<"Vertical multiplicator tends to infinity";
                }
                settings.setScaleFactor(1,-(settings.getPeriodicRangeY()[1] + settings.getPeriodicRangeY()[0])/2);
                if(fabs(settings.getPeriodicRangeY()[1] - settings.getPeriodicRangeY()[0]) < 0.95*settings.getVerticalPeriod())
                    settings.setMultiplier(1,settings.getMultipliers()[1]*0.9);
            }
            else{
                settings.setMultiplier(1,(this->getImage()->height())* 1e8);
                settings.setScaleFactor(1,-processPhaseV(oldCoordinates[settings.getCoordinateNumbers()[1]]));
            }
        }
        else{
            if((fabs(settings.getScalesY()[1]-settings.getScalesY()[0])>1e-8)){
                if(fabs(settings.getRangeY()[1] - settings.getRangeY()[0]) > 0){
                    settings.setMultiplier(1,(this->getImage()->height())/fabs(settings.getRangeY()[1] - settings.getRangeY()[0]));
                }
                else{
                    settings.setMultiplier(1,1);
                    LOG(WARNING)<<"Vertical multiplicator tends to infinity";
                }
                settings.setScaleFactor(1,-(settings.getRangeY()[1] + settings.getRangeY()[0])/2);
                settings.setMultiplier(1,settings.getMultipliers()[1]*0.9);
            }
            else{
                settings.setMultiplier(1,(this->getImage()->height())* 1e8);
                settings.setScaleFactor(1,-processPhaseV(oldCoordinates[settings.getCoordinateNumbers()[1]]));
            }
        }
    }
}
void SpatialPainter::resizeEvent(QResizeEvent *event){
    if((event->size().width() <= MIN_WIDTH) && (event->size().height() <= MIN_HEIGHT)){
        return;
    }
    delete pntr;
    ui->gridLayoutWidget->setFixedSize(event->size().width() - SIZE_SHIFT_X,
                                       event->size().height() - SIZE_SHIFT_Y);
    ui->imageLabel->setFixedSize(ui->gridLayoutWidget->size().width() - ui->vMin->width() - 6,
                                 ui->gridLayoutWidget->size().height() - ui->hMin->height() - 6);
    pntr = new QImage(ui->imageLabel->size(), QImage::Format_RGB32);
    pntr->fill(settings.getBackgroundColor());
    lineBuffer.clear();
    lineBufferFast.clear();
    pointBuffer.clear();
    pointBufferFast.clear();
    updateImage();
    this->doscalePrivate();
    this->updateLabels();
}
void SpatialPainter::updateRange(void){
    if(settings.getCoordinateNumbers()[1] == 2*dimension){
        ;//We will not update range for oscillograms
    }
    else{
        if(settings.getVerticalProcessing() != "Periodic"){
            settings.setRangeY(0,settings.getScalesY()[0]);//If identical processing - set maxmin scales as range
            settings.setRangeY(1,settings.getScalesY()[1]);
        }
        else{
            if(isRunningY){
                settings.setPeriodicRangeY(0,settings.getRangeY()[0]);
                settings.setPeriodicRangeY(1,settings.getRangeY()[1]);
            }
            else{
                settings.setPeriodicRangeY(0,settings.getScalesY()[0]);
                settings.setPeriodicRangeY(1,settings.getScalesY()[1]);
            }
        }
    }
}
