﻿#include "wavedrawer.h"
#include "ui_wavedrawer.h"

WaveDrawer::WaveDrawer(DynamicalSystem *DS, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::WaveDrawer),
    ds(DS)
{
    ui->setupUi(this);
    this->setAttribute(Qt::WA_DeleteOnClose);
    setup_plot();
    this->setWindowTitle(tr("Пространственная диаграмма"));
}

WaveDrawer::~WaveDrawer()
{
    delete ui;
}
void WaveDrawer::setup_plot()
{
    ui->wavePlot->plotLayout()->insertRow(0);
    ui->wavePlot->plotLayout()->addElement(0, 0, new QCPPlotTitle(ui->wavePlot, tr("Временная диаграмма")));
    QPen pen;
    pen.setColor(QColor(50, 153, 204));
    ui->wavePlot->addGraph();
    QVector<double> x(ds->Dimension()), y(ds->Dimension());
    for (uint32_t k = 0; k < ds->Dimension(); ++k)
    {
        x[k] = k;
        y[k] = commonFunctions::modPeriod(ds->Coordinate(k), 2*M_PI, -M_PI);
    }
    ui->wavePlot->graph()->setData(x, y);
    ui->wavePlot->graph()->rescaleAxes(true);
    ui->wavePlot->graph()->setPen(pen);
    ui->wavePlot->graph()->setName(tr("Пространственная диаграмма"));
    ui->wavePlot->graph()->setLineStyle(QCPGraph::lsLine);
    ui->wavePlot->graph()->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, 6));
    ui->wavePlot->yAxis->setRange(-M_PI, M_PI);
}
void WaveDrawer::draw(std::vector<std::vector<double>> trajectory)
{
    int step = static_cast<int>(static_cast<double>(ui->sizeSpinBox->value()) / ds->Step());
    for (int i = 0; i < trajectory.size(); i += step)
    {
        if(i >= trajectory.size())
        {
            break;
        }
        QVector<double> x(ds->Dimension()), y(ds->Dimension());
        for (uint32_t k = 0; k < ds->Dimension(); ++k)
        {
            x[k] = k;
            y[k] = commonFunctions::modPeriod(trajectory[i][k], 2*M_PI, -M_PI);
        }
        ui->wavePlot->graph()->setData(x, y);
        ui->wavePlot->replot();
        QApplication::processEvents();
    }
}
void WaveDrawer::on_sizeSpinBox_valueChanged(int arg1)
{
    ui->wavePlot->graph()->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, arg1));
    ui->wavePlot->replot();
}
void WaveDrawer::on_closePushButton_clicked()
{
    this->close();
}
void WaveDrawer::on_joinCheckBox_clicked(bool checked)
{
    if(checked)
    {
        ui->wavePlot->graph()->setLineStyle(QCPGraph::lsLine);
    }
    else
    {
        ui->wavePlot->graph()->setLineStyle(QCPGraph::lsNone);
    }
    ui->wavePlot->replot();
}
