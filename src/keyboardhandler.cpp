#include "../inc/keyboardhandler.h"

KeyboardHandler::KeyboardHandler(QObject *parent) : QObject(parent){
}
KeyboardHandler::~KeyboardHandler(){
}
void KeyboardHandler::processPressedKey(QKeyEvent *event){
    switch (event->key()) {
    case Qt::Key_Right:
        qDebug() << "Key Right pressed";
        emit parameter1ShiftUp();
        break;
    case Qt::Key_Left:
        qDebug() << "Key Left pressed";
        emit parameter1ShiftDown();
        break;
    case Qt::Key_Up:
        qDebug() << "Key Up pressed";
        emit parameter2ShiftUp();
        break;
    case Qt::Key_Down:
        qDebug() << "Key Down pressed";
        emit parameter2ShiftDown();
        break;
    case Qt::Key_A:
        qDebug() << "Key A pressed";
        emit parameter1StepShiftDown();
        break;
    case Qt::Key_D:
        qDebug() << "Key D pressed";
        emit parameter1StepShiftUp();
        break;
    case Qt::Key_W:
        if(event->modifiers() & Qt::ControlModifier) {
            qDebug() << "Alt + W pressed";
            emit makeNewArchiveEntry();
        } else {
            qDebug() << "Key W pressed";
            emit parameter2StepShiftUp();
        }
        break;
    case Qt::Key_S:
        qDebug() << "Key S pressed";
        emit parameter2StepShiftDown();
        break;
    case Qt::Key_T:
        qDebug() << "Key T pressed";
        emit alignTop();
        break;
    case Qt::Key_L:
        qDebug() << "Key L pressed";
        emit alignLeft();
        break;
    case Qt::Key_F1:
        qDebug() << "Key F1 pressed";
        emit changeParameter1();
        break;
    case Qt::Key_F2:
        qDebug() << "Key F2 pressed";
        emit changeParameter2();
        break;
    case Qt::Key_R:
        qDebug() << "Key R pressed";
        emit reset();
        break;
    case Qt::Key_M:
        qDebug() << "Key M pressed";
        emit scale();
        break;
    case Qt::Key_B:
        qDebug() << "Key B pressed";
        emit dublicate();
        break;
    case Qt::Key_Delete:
        qDebug() << "Key Delete pressed";
        emit deleteSelectedWindows();
        break;
    case Qt::Key_F:
        qDebug() << "Key F pressed";
        emit showMaximized();
        break;
    case Qt::Key_G:
        qDebug() << "Key G pressed";
        emit debugTrigger();
        break;
    case Qt::Key_Escape:
        qDebug() << "Key Escape pressed";
        emit cancel();
        break;
    default:
        break;
    };
}
