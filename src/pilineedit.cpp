﻿#include "../inc/pilineedit.h"
//#include <QDebug>
piLineEdit::piLineEdit(QWidget *parent) :
    QLineEdit(parent){
    connect(this,SIGNAL(editingFinished()),this,SLOT(editingFinishedSlot()));
    this->setValue(0);
    this->setMinimumWidth(70);
    this->setAlignment(Qt::AlignLeft);
}
piLineEdit::piLineEdit(double initValue,QWidget *parent){
    this->setParent(parent);
    connect(this,SIGNAL(editingFinished()),this,SLOT(editingFinishedSlot()));
    this->setMinimumWidth(70);
    setValue(initValue);
}
void piLineEdit::editingFinishedSlot(){
    procString();
}
void piLineEdit::setValue(double val){
    this->setText(QString("%1").arg(val,15,'f',16,'0'));
}
double piLineEdit::getValue(){
    return this->text().toDouble();
}
void piLineEdit::keyPressEvent(QKeyEvent *event){
    if(event->key() == Qt::Key_0) insert("0");
    else if(event->key() == Qt::Key_1) insert("1");
    else if(event->key() == Qt::Key_2) insert("2");
    else if(event->key() == Qt::Key_3) insert("3");
    else if(event->key() == Qt::Key_4) insert("4");
    else if(event->key() == Qt::Key_5) insert("5");
    else if(event->key() == Qt::Key_6) insert("6");
    else if(event->key() == Qt::Key_7) insert("7");
    else if(event->key() == Qt::Key_8) insert("8");
    else if(event->key() == Qt::Key_9) insert("9");
    else if(event->key() == Qt::Key_Comma) insert(".");
    else if(event->key() == Qt::Key_P) insert("p");
    else if(event->key() == Qt::Key_I) insert("i");
    else if(event->key() == Qt::Key_Minus) insert("-");
    else if(event->key() == Qt::Key_Asterisk) insert("*");
    else if(event->key() == Qt::Key_Space) {this->setText(this->text().remove(this->cursorPosition(),this->text().length()-this->cursorPosition()));procString();}
    else if(event->key() == Qt::Key_Enter) procString();
    else if(event->key() == Qt::Key_Backspace) this->backspace();
    else if(event->key() == Qt::Key_Delete) this->del();
    else if(event->key() == Qt::Key_Right)this->cursorForward(false);
    else if((event->modifiers() & Qt::ShiftModifier) && (event->key() == Qt::Key_Right))this->cursorForward(true);
    else if(event->key() == Qt::Key_Left)this->cursorBackward(false);
    else if((event->modifiers() & Qt::ShiftModifier) && (event->key() == Qt::Key_Left))this->cursorBackward(true);
}
void piLineEdit::procString(){
    if(this->text().length() == 0) {this->setValue(0); return;}
    int index = 0;
    bool minusSign = false;
    if(this->text().at(0) == '-') minusSign = true;
    QString refString;
    index = this->text().indexOf("pi*",Qt::CaseInsensitive);
    if(index == -1)
    {
        index = this->text().indexOf("pi",Qt::CaseInsensitive);
        if(index == -1)
        {
            if(this->text().indexOf(",") != -1)
            {
             this->setText(this->text().replace(QString(","),QString(".")));
            }
            expression = this->text().toDouble();
            this->setValue(expression);
        }
        else
        {
            if(minusSign) expression = -M_PI;
            else expression = M_PI;
            this->setValue(expression);
        }
    }
    else
    {
        refString = this->text().remove(0,index+3);
        if(minusSign) expression = -M_PI*refString.toDouble();
        else expression = M_PI*refString.toDouble();
        this->setValue(expression);
    }
    emit newValue(expression);
}
