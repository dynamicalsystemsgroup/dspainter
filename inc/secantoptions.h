﻿#ifndef SECANTOPTIONS_H
#define SECANTOPTIONS_H

#include <QWidget>
#include <QLabel>
#include <QGridLayout>
#include "pilineedit.h"
#include "types.h"
#include "dsglobals.h"
namespace Ui {
class SecantOptions;
}

class SecantOptions : public QWidget
{
    Q_OBJECT

public:
    explicit SecantOptions(QWidget *parent = 0);
    ~SecantOptions();
    void configure(const uint32_t &dim, const std::vector<QString> &IDs);
private slots:
    void on_typeComboBox_currentIndexChanged(const QString &arg1);
    void on_cyclicNumberComboBox_currentIndexChanged(int index);
//    void on_secant_changed(double);
//    void onPeriodChanged(double);
private:
    Ui::SecantOptions *ui;
    std::vector<piLineEdit*> secantEdits;
    std::vector<QLabel*> secantLabels;
    QWidget scrollWidget;
    QGridLayout mainLayout;
    piLineEdit cyclicPeriodEdit;
    WindowSettings settings;
signals:
    void settingsChanged(WindowSettings set);
    void secant_changed(std::vector<double> secant);
    void periodic_coordinate_changed(int number);
};

#endif // SECANTOPTIONS_H
