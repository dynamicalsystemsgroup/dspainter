﻿#ifndef TABFORDOCK_H
#define TABFORDOCK_H

#include <QWidget>
#include "dsglobals.h"
#include "visualsettings.h"
#include "secantoptions.h"
#include "windowsettings.h"
class WindowSettingsWidget : public QTabWidget
{
    Q_OBJECT
public:
    WindowSettingsWidget(QWidget *parent = 0);
    void config(const uint32_t &newDimension, const std::vector<QString> &IDs);
    void config_secant_tab(const uint32_t &newDimension, const std::vector<QString> &IDs);
    void setSettings(const WindowSettings &settings);
protected:
    void keyPressEvent(QKeyEvent *keyEvent);
private:
    visualSettings windowSettingsTab;
    SecantOptions secantTab;
signals:
    void keyPressed(QKeyEvent *keyEvent);
    void settingsChanged(WindowSettings settings);
    void secant_changed(std::vector<double> secant);
    void periodic_index_changed(int number);
};

#endif // TABFORDOCK_H
