#ifndef ABOUTAPPLICATION_H
#define ABOUTAPPLICATION_H

#include <QWidget>
#include <QApplication>
namespace Ui {
class aboutApplication;
}

class aboutApplication : public QWidget
{
    Q_OBJECT

public:
    explicit aboutApplication(QWidget *parent = 0);
    ~aboutApplication();
protected:
    void mousePressEvent(QMouseEvent *);
    void keyPressEvent(QKeyEvent *);
private:
    Ui::aboutApplication *ui;
};

#endif // ABOUTAPPLICATION_H
