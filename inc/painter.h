#ifndef PAINTER_H
#define PAINTER_H

#include "templatewindow.h"

class Painter : public TemplateWindow
{
    Q_OBJECT
public:
    explicit Painter(uint32_t newDimension, std::vector<QString> newCoordinateIDs,
                     QWidget *parent = 0);
    //Methods
//    void setInitialPoint(void);
    virtual void draw(const std::vector<std::vector<double> > &buffer);
    virtual void setSettings(const WindowSettings &set);
private:
protected:
    void resizeEvent(QResizeEvent *event);
    virtual void initialize(void);
    virtual void doscalePrivate(void);
    virtual void updateScales(std::vector<double> scales);
public slots:
    virtual void doscale();
    virtual void updateLabels(void);
    virtual void updateMultipliers(void);
private slots:
    virtual void updateRange(void);
};

#endif // PAINTER_H
