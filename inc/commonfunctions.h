﻿#ifndef COMMONFUNCTIONS_H
#define COMMONFUNCTIONS_H

#include "types.h"
#include "matrix.h"
#include "easyloggingpp.h"
#include "dynamicalsystem.h"

using localFunctionPrototype = double (*)(double*,double*,double*);

class commonFunctions
{
public:
    commonFunctions();
    ~commonFunctions();
    static constexpr double tolerance = 1e-6;
    //static methods
    static double modPeriod(const double &value, const double &period, const double &minimum);
    static std::vector<double> cubicApproximation(std::vector<double> oldPoint,
                                                  std::vector<double> newPoint, std::vector<double> secant,
                                                  double secantSummOld,
                                                  double secantSummNew);
    static void stepmake4(double x[], double fun(double [], double [],double []), double h, int n, double param[]);
    static bool catchIntersection(std::vector<double> &intersectionCoordinates, std::vector<double> oldPhase,
                                  std::vector<double> newPhase, SecantContainer &secants);
    static mathematics::matrix getJacobian(const uint32_t &dimension, const double &tolerance,
                                    std::vector<double> point, std::vector<double> parameters, localFunctionPrototype function);
    static void set_dynamical_system(DynamicalSystem *new_ds);
private:
    static DynamicalSystem *dynamical_system;
};

#endif // COMMONFUNCTIONS_H
