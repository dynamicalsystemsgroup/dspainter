#ifndef TYPES
#define TYPES
#include <vector>
#include <stdint.h>
#include <QString>
#include <QColor>
#include <QRect>
#include <math.h>
#include <stdint-gcc.h>
#include "generalsettings.h"
#include "windowsettings.h"
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

class ParameterSettings
{
public:
    uint32_t firstNumber;
    uint32_t secondNumber;
    double firstStep;
    double secondStep;
    std::vector<double> parameters;
};

enum TypeOfMapping{
    Onesided,
    Twosided
};

namespace GraphicalAreaNS {
enum graphicalMode{
    draw,
    select,
    relocate,
    resize,
    lock
};
}

class SecantContainer{
public:
    std::vector<double> secant;
    std::vector<double> secantPlus;
    std::vector<double> secantMinus;
    double period;
    int dimension; // ?? why here was double
};
struct BifDOptions{
    uint32_t dimension;
    uint32_t coordinateNumber;
    uint32_t cyclicCoordinateNumber;
    TypeOfMapping mappingType;
    uint32_t parameterNumber;
    double period;
    double paramMax;
    double paramMin;
    double paramStep;
    uint32_t transItNumber;
    double intTime;
    double intStep;
    uint32_t width;
    uint32_t height;
};
class infoClass{
public:
    infoClass(){numberOfEntries = 0;}
    QString systemName;
    uint32_t numberOfEntries;
};
enum typeTime{
    DIRECT,
    INVERSE
};
class specialTrajectoryEntry{
public:
    specialTrajectoryEntry(){
        typeOfTime = DIRECT;
        integratingTime = 100;
        step = 0.05;
        lineColor = QColor(Qt::black);
        fixed = false;
    }
    typeTime typeOfTime;// Direct or inverse
    std::vector<double> coordinates;
    double integratingTime;
    double step;
    QColor lineColor;
    bool fixed;
    QString comment;
};
enum DSType{
    Flow,
    Cascade
};
class pathSettings{
public:
    pathSettings(){;}
    pathSettings(QString libPath,
                 QString libBPath){
        libraryPath = libPath;
        libBuilderPath = libBPath;
    }
    QString libraryPath;
    QString libBuilderPath;
};

#endif // TYPES

