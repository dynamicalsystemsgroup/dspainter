#ifndef KEYBOARDHANDLER_H
#define KEYBOARDHANDLER_H

#include "dsglobals.h"

class KeyboardHandler : public QObject
{
    Q_OBJECT
public:
    explicit KeyboardHandler(QObject *parent = 0);
    ~KeyboardHandler();
public slots:
    void processPressedKey(QKeyEvent *event);
signals:
    void parameter1ShiftUp(void);
    void parameter1ShiftDown(void);
    void parameter2ShiftUp(void);
    void parameter2ShiftDown(void);
    void parameter1StepShiftUp(void);
    void parameter1StepShiftDown(void);
    void parameter2StepShiftUp(void);
    void parameter2StepShiftDown(void);
    void deleteSelectedWindows(void);
    void alignTop(void);
    void alignLeft(void);
    void reset(void);
    void scale(void);
    void changeParameter1(void);
    void changeParameter2(void);
    void dublicate(void);
    void showMaximized(void);
    void cancel(void);
    void debugTrigger(void);
    void makeNewArchiveEntry(void);
};

#endif // KEYBOARDHANDLER_H
