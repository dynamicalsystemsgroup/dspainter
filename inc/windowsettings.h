#ifndef WINDOWSETTINGS
#define WINDOWSETTINGS
#include <stdint-gcc.h>
#include <QRectF>
#include <QString>
#include <vector>
#include <QColor>
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif
class WindowSettings{
public:
    explicit WindowSettings();
    void setGeometry(const QRectF &newGeometry);
    void setPointSize(const uint32_t &newPointSize);
    void setLineWidth(const uint32_t &newLineWidth);
    void setWindowType(const QString &newWindowType);
    void setCoordinateNumbers(const std::vector<uint32_t> &newCoordinateNumbers);
    void setCoordinateNumbers(const uint32_t &number,const double &value);
    void setScalesX(const std::vector<double> &newScalesX);
    void setScaleX(const uint32_t &number,const double &value);
    void setScalesY(const std::vector<double> &newScalesY);
    void setScaleY(const uint32_t &number,const double &value);
    void setMultipliers(const std::vector<double> &newMultipliers);
    void setMultiplier(const uint32_t &number, const double &value);
    void setScaleFactors(const std::vector<double> &newScaleFactors);
    void setScaleFactor(const uint32_t &number, const double &value);
    void setRangeX(const std::vector<double> &newRangeX);
    void setRangeX(const uint32_t &number, const double &value);
    void setRangeY(const std::vector<double> &newRangeY);
    void setRangeY(const uint32_t &number, const double &value);
    void setHorizontalProcessing(const QString &newHorizontalProcessing);
    void setVerticalProcessing(const QString &newVerticalProcessing);
    void setHorizontalPeriod(const double &newHorizontalPeriod);
    void setVerticalPeriod(const double &newVerticalPeriod);
    void setLineColor(const QColor &newLineColor);
    void setBackgroundColor(const QColor &newBackgroundColor);
    void setPeriodicRangeX(const std::vector<double> &newPeriodicRangeX);
    void setPeriodicRangeX(const uint32_t &number, const double &value);
    void setPeriodicRangeY(const std::vector<double> &newPeriodicRangeY);
    void setPeriodicRangeY(const uint32_t &number, const double &value);
    //Spatial drawer
    void setJoinPoints(const bool &newJoinPoints);
    void setTimeLag(const double &newTimeLag);
    void setRenderDerivatives(const bool &newRenderDerivatives);
    void setVisualizationRange(const std::vector<uint32_t> &newVisualizationRange);
    void setVisualizationRange(const uint32_t &number, const double &value);
    //Poincare data
    void setCyclicCoordinateNumber(const uint32_t &newCyclicCoordinateNumber);
    void setCyclicPeriod(const double &newCyclicPeriod);
//    typeOfMapping mappingType;
    void setSecant(const std::vector<double> &newSecant);
    // GET
    QRectF getGeometry(void) const;
    uint32_t getPointSize(void) const;
    uint32_t getLineWidth(void) const;
    QString getWindowType(void) const;
    std::vector<uint32_t> getCoordinateNumbers(void) const;
    std::vector<double> getScalesX(void) const;
    std::vector<double> getScalesY(void) const;
    std::vector<double> getMultipliers(void) const;
    std::vector<double> getScaleFactors(void) const;
    std::vector<double> getRangeX(void) const;
    std::vector<double> getRangeY(void) const;
    QString getHorizontalProcessing(void) const;
    QString getVerticalProcessing(void) const;
    double getHorizontalPeriod(void) const;
    double getVerticalPeriod(void) const;
    QColor getLineColor(void) const;
    QColor getBackgroundColor(void) const;
    std::vector<double> getPeriodicRangeX(void) const;
    std::vector<double> getPeriodicRangeY(void) const;
    //Spatial drawer
    bool getJoinPoints(void) const;
    double getTimeLag(void) const;
    bool getRenderDerivatives(void) const;
    std::vector<uint32_t> getVisualizationRange(void) const;
    //Poincare data
    uint32_t getCyclicCoordinateNumber(void) const;
    double getCyclicPeriod(void) const;
//    typeOfMapping mappingType;
    std::vector<double> getSecant(void) const;
private:
    QRectF geometry;
    uint32_t pointSize;
    uint32_t lineWidth;
    QString windowType;
    std::vector<uint32_t> coordinateNumbers;
    std::vector<double> scalesX;
    std::vector<double> scalesY;
    std::vector<double> multipliers;
    std::vector<double> scaleFactors;
    std::vector<double> rangeX;
    std::vector<double> rangeY;
    QString horizontalProcessing;
    QString verticalProcessing;
    double horizontalPeriod;
    double verticalPeriod;
    QColor lineColor;
    QColor backgroundColor;
    std::vector<double> periodicRangeX;
    std::vector<double> periodicRangeY;
    //Spatial drawer
    bool joinPoints;
    double timeLag;
    bool renderDerivatives;
    std::vector<uint32_t> visualizationRange;
    //Poincare data
    uint32_t cyclicCoordinateNumber;
    double cyclicPeriod;
//    typeOfMapping mappingType;
    std::vector<double> secant;
};
#endif // WINDOWSETTINGS

