#ifndef OPTIONS_H
#define OPTIONS_H

#include <QWidget>
#include "types.h"
#include <QFileDialog>
#include <QSettings>

namespace Ui {
class Options;
}

class Options : public QWidget
{
    Q_OBJECT
public:
    explicit Options(QWidget *parent = 0);
    ~Options();
    void setGeneralSettings(const GeneralSettings &settings);
    void setPathSettings(pathSettings set);
private slots:
    void on_OkBtn_clicked();
    void on_browseBtn_clicked();
private:
    Ui::Options *ui;
    void read_settings(void);
    void write_settings(void);
signals:
    void generalSettingsChanged(GeneralSettings settings);
    void pathSettingsChanged(pathSettings);
};

#endif // OPTIONS_H
