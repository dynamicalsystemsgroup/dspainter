﻿#ifndef DYNAMICALSYSTEM_H
#define DYNAMICALSYSTEM_H

#include "easyloggingpp.h"
#include <stdint-gcc.h>
#include <vector>
#include <QString>
#include <QLibrary>
#include "matrix.h"
#include <QMutex>
#include <QThread>

/// This function prototipes have such arguments and return types due to our convention
/// on the dynamical system library
using functionPrototype = double (*)(double*,double*,double*);
using getNPrototype = int (*)(void);
using getParPrototype = int (*)(void);
using getPhNamePrototype = char* (*)(int);
using getParNamePrototype = char* (*)(int);
using getSysTypePrototype = char* (*)(void);
using getLineNumPrototype = int (*)(void);
using getGridNumPrototype = int (*)(void);
using getCubeNumPrototype = int (*)(void);
using getSysNamePrototype = char* (*)(void);
using getPatPerNumPrototype = int (*)(void);

/// Methods have convinient names
///
class DynamicalSystem : public QThread{
    Q_OBJECT
public:
    explicit DynamicalSystem(QObject *parent = 0);
    ~DynamicalSystem();
    //methods
    bool reload(const QString &name);// load new dynamical system from file with addr in name
    functionPrototype Function(void) const;// returns the pointer to a DS function, calculating righthand sides of equations
    std::vector<double> Parameters(void) const;
    double Parameter(uint32_t number);
    double Coordinate(uint32_t number);
    void setParameter(uint32_t number, double newValue);
    std::vector<double> Coordinates(void) const;
    void setCoordinate(uint32_t number, double newValue);
    std::vector<QString> ParameterIDs(void) const;
    std::vector<QString> CoordinateIDs(void) const;
    std::vector<double> Derivatives(std::vector<double> &coordinate);
    QString SystemName(void);
    uint32_t Time(void);
    uint32_t Dimension(void);
    uint32_t ParamDimension(void);
    uint32_t LineNumber(void);
    uint32_t GridNumber(void);
    uint32_t CubeNumber(void);
    uint32_t PatternPerNumber(void);
    QString SystemType(void);
    double Step(void);
    void setStep(const double& newStep);
    bool isLoaded(void);
    void setLibName(const QString &name);
    void setBufferSize(const uint32_t &size);
    std::vector<std::vector<double>> getBuffer(void) const;
    void setDebugMode(bool value);
    void setNullTime(void);
    std::vector<std::vector<double> > getIntegralCurve(std::vector<double> initialConditions, const double &maximumTime,
                                         const double &localStep);
public slots:
    void startIntegrating(const double &Step, const double &MaxTime);
    void stopIntegrating(void);
    void resume(void);
    void setParameters(const std::vector<double> &newParameters);
    void setCoordinates(const std::vector<double> &newCoordinates);
protected:
    virtual void run();
private:
    QLibrary library;
    QString libraryName;
    functionPrototype function;
    std::vector<double> parameters;
    std::vector<QString> parameterIDs;
    std::vector<double> coordinates;
    std::vector<QString> coordinateIDs;
    std::vector<double> derivatives;
    double time;
    uint32_t dimension;
    uint32_t paramDimension;
    uint32_t lineNumber;
    uint32_t gridNumber;
    uint32_t cubeNumber;
    uint32_t patternsPerNumber;
    QString systemType;
    QString systemName;
    //
    double maxTime;
    double step;
    QMutex mutex;
    bool stopCondition;
    std::vector<std::vector<double>> buffer;
    uint32_t maxBufferSize;
    bool debugMode;
    //methods
    void initializeDynamicalSystem();
    void makeStep(void);
    void collectBuffer(void);
signals:
    void bufferCollected(std::vector<std::vector<double>> &);
    void loaded(bool);
    void operationStatusChanged(bool status);
    void stepChanged(double);
};

#endif // DYNAMICALSYSTEM_H
