#ifndef COLLAPSIBLEDOCKWIDGET_H
#define COLLAPSIBLEDOCKWIDGET_H

#include <QObject>
#include <QDockWidget>
#include <QPropertyAnimation>
#include <QGraphicsOpacityEffect>
#include <QLabel>
const uint32_t MAX_WIDTH = 420;
const uint32_t MIN_WIDTH = 1;
class CollapsibleDockWidget : public QDockWidget
{
    Q_OBJECT
public:
    CollapsibleDockWidget(const QString & title,
                          QWidget * parent = 0,
                          Qt::WindowFlags flags = 0);
    CollapsibleDockWidget(QWidget * parent = 0, Qt::WindowFlags flags = 0);
    void setNewWidget(QWidget *widget);
    void hideWidget();
    void showWidget();
protected:
    void enterEvent(QEvent *);
    void leaveEvent(QEvent *);
    void keyPressEvent(QKeyEvent *keyEvent);
private:
    QLabel *sidelabel;
private slots:
    void featuresChanged(QDockWidget::DockWidgetFeatures features){
        this->setFeatures(features | QDockWidget::DockWidgetVerticalTitleBar);
    }
signals:
    void keyPressed(QKeyEvent *keyEvent);
};

#endif // COLLAPSIBLEDOCKWIDGET_H
