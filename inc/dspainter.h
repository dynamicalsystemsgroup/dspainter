﻿#ifndef DSPAINTER_H
#define DSPAINTER_H
#include "dsglobals.h"
#include "ui_dspainter.h"
#include <QShortcut>
#include <QProcess>
#include <QToolBar>
#include <QMainWindow>
#include <QDockWidget>
#include <QStatusBar>
#include "collapsibledockwidget.h"

class DSPainter : public QMainWindow
{
    Q_OBJECT

public:
    explicit DSPainter(QWidget *parent = 0);
    ~DSPainter();
    void setGraphicalArea(QWidget *graphicalArea);
    void setParameterDockWidget(QWidget *parameterDock);
    void setWindowSettingsDockWidget(CollapsibleDockWidget *windowSettingsDock);
    bool eventFilter(QObject *object, QEvent *event);
public slots:
    void reconfigStartStopIcons(bool isRunning);
    void changeGraphicalMode(GraphicalAreaNS::graphicalMode newMode);
    void reconfigureActions(bool value);
protected:
    void keyPressEvent(QKeyEvent *event);
    void mousePressEvent(QMouseEvent *event);
private:
    //members
    Ui::Form *ui;
    QToolBar *toolBar;
    QStatusBar *statusBar;
    QDockWidget *parameterDockWidget;
    QMenu *mainMenu;
    QMenu *integrationOptionsMenu;
    QMenu *windowMenu;
    QMenu *optionsMenu;
    QMenu *aboutMenu;

    QAction *newWindowAction;
    QAction *editAction;
    QAction *lockAction;
    QAction *alignLeftAction;
    QAction *alignTopAction;
    QAction *deleteSelectedAction;
    QAction *closeAllWindowsAction;
    QAction *saveImageAction;
    QAction *setPhaseCoordinatesAction;
    QAction *scaleAction;
    QAction *resetAction;
    QAction *aboutApplicationAction;
    QAction *exitAction;
    QAction *startIntegratingAction;
    QAction *stopIntegratingAction;
    QAction *loadNewLibraryAction;
    QAction *loadLibBuilderAction;
    QAction *equillibriumFinderAction;
    QAction *aboutSystemAction;
    QAction *optionsAction;
    QAction *setParametersAction;
    QAction *createColormap;
    QAction *createWaveDrawer;
    //methods
    void setUpToolBar(void);
    void setUpDockWidget(void);
    void setUpStatusBar(void);
    void setUpMenu(void);
    void createActions(void);
signals:
    void newWindowModeTriggered(void);
    void editModeTriggered(void);
    void lockModeTriggered(void);
    void alignTopTriggered(void);
    void alignLeftTriggered(void);
    void closeAllWindowsTriggered(void);
    void deleteSelectedTriggered(void);
    void saveImageTriggered(void);
    void startIntegrationTriggered(void);
    void stopIntegrationTriggered(void);
    void setPhaseCoordinatesTriggered(void);
    void equillibriumFinderTriggered(void);
    void loadNewLibraryTriggered(void);
    void loadLibraryBuilderTriggered(void);
    void mainWindowClosed(void);
    void showOptionsTriggered(void);
    void aboutSystemTriggered(void);
    void aboutApplicationTriggered(void);
    void scaleTriggered(void);
    void resetTriggered(void);
    void setParametersTriggered(void);
    void keyPressed(QKeyEvent *);
    void createColormapTriggered();
    void createWaveDrawerTriggered();
};

#endif // DSPAINTER_H
