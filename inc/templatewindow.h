﻿#ifndef TEMPLATEWINDOW_H
#define TEMPLATEWINDOW_H

#include "dsglobals.h"
#include "ui_templatewindow.h"
/////
/////This class provides a template class for all
///// standart windows, that are to be placed on graphical area and
///// meant to have standart settings.
/////
#define MAX_COORD_NUMBER 2
#define LINE_COLOR 0
#define BGD_COLOR 1
#define WARNING_COLOR 2

#define NO_INT 0x00
#define H_RIGHT 0x01
#define H_LEFT 0x02
#define V_UPPER 0x04
#define V_BOTTOM 0x08

#define LABEL_FIXED_SIZE QSize(50,15)
#define MIN_WIDTH 300
#define MIN_HEIGHT 300
#define SIZE_SHIFT_X 50
#define SIZE_SHIFT_Y 20
#define MAX_POINTS_IN_BUFFERS 100000

namespace Ui {
class templateWindowForm;
}

class TemplateWindow : public QWidget{
    Q_OBJECT
public:
    // constructors and destructors
    explicit TemplateWindow(uint32_t newDimension,
                            std::vector<QString> newCoordinateIDs,
                            QWidget *parent = 0);
    virtual ~TemplateWindow();
    // public methods
    QImage *getImage(void) const;
    WindowSettings getSettings(void) const;
    virtual void setSettings(const WindowSettings &set);
    void setPointsAttached(bool value);
    void drawPoint(QPointF point);
    void drawPointNow(QPointF point);
    void drawLine(QPointF start, QPointF end);
    void drawLineNow(QPointF start, QPointF end);
    virtual void draw(const std::vector<std::vector<double> > &);
    virtual void init_before_drawing(std::vector<double> coordinates);
    void setLineWidth(uint32_t width);
    void setPointSize(uint32_t size);
    void setWindowType(QString newType);
    void updateGeometryFromHolder(const QRectF &newGeometry);
private:
    stdFunctionMono userProcessingFunctionH;
    stdFunctionMono userProcessingFunctionV;
    bool resetFlag;
    bool maximumReachedFlag;
    void setUpImageLabel(void);
public slots:
    void reset(void);// reset image area to bg color and draw axis
    virtual void doscale(void);// scale image area
    void drawMarkMaximum(void);// draw mark, signalling that coordinates are out of range
    void breakTrajectory();// break trajectory for proper visualisation with periodic processing
    void clearImage(void);// utilitary function for reset()
    void updateImage(void);// update image when painter got enough points to draw
protected slots:
    virtual void updateRange(void);
    virtual void updateMultipliers(void);
    virtual void updateLabels(void);
protected:
    Ui::templateWindowForm *ui;
    QImage* pntr;
    QTimer timer;
//    uint32_t prepoints;
    uint32_t dimension;
    std::vector<QString> coordinateIDs;
    bool resetAttach;
    bool isRunningX;
    bool isRunningY;
    bool pointsAttached;
    std::vector<QPointF> pointBuffer;
    std::vector<QLineF> lineBuffer;
    std::vector<QPointF> pointBufferFast;
    std::vector<QLineF> lineBufferFast;
    QPointF previous;
    QPointF current;
    std::vector<double> oldCoordinates;
    WindowSettings settings;
    void resizeEvent(QResizeEvent *event);
    void paintEvent(QPaintEvent *);
    void setRunningX(bool);
    void setRunningY(bool);
    double processPhaseH(double value) const;
    double processPhaseV(double value) const;
    virtual void initialize(void);
    virtual void doscalePrivate(void);
    virtual void updateScales(std::vector<double>);
signals:
    void paintFinished();
};

#endif // TEMPLATEWINDOW_H
