#ifndef PARAMETERWIDGET_H
#define PARAMETERWIDGET_H

#include <QWidget>
#include "dsglobals.h"
#include "easyloggingpp.h"

namespace Ui {
class ParameterWidget;
}

class ParameterWidget : public QWidget
{
    Q_OBJECT
public:
    enum ActiveNumber {
        First,
        Second
    };
    enum Direction {
        Positive,
        Negative
    };
    explicit ParameterWidget(std::vector<QString> newParameterNames,
                             QWidget *parent = 0);
    ~ParameterWidget();
    void config(void);
    void setParameters(const std::vector<double> &newParameters);
    void setSettings(const ParameterSettings &settings);
    void setParameterNames(const std::vector<QString> &newNames);
    void multiplyStep(const ActiveNumber &actNumber, const Direction &direction);
    void shiftParameter(const ActiveNumber &actNumber, const Direction &direction);
    void incrementActiveParameter(const ActiveNumber &actNumber);
    ParameterSettings getParameterSettings(void) const;
    int32_t getParameterNumber(const ActiveNumber &actNumber) const;
private:
    Ui::ParameterWidget *ui;
    std::vector<QString> parameterNames;
    std::vector<double> parameters;
    void updateControls(void);
signals:
    void valueChanged(uint32_t actNumber, double value);
    void numberChanged(uint32_t actNumber, uint32_t number);
    void stepChanged(uint32_t actNumber, double step);
};

#endif // PARAMETERWIDGET_H
