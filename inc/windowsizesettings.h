#ifndef WINDOWSIZESETTINGS_H
#define WINDOWSIZESETTINGS_H

#include "DSGlobals.h"
#include "templatewindow.h"
namespace Ui {
class windowSizeSettings;
}
class templateWindow;
class windowSizeSettings : public QWidget
{
    Q_OBJECT

public:
    explicit windowSizeSettings(templateWindow *wnd, QWidget *parent = 0);
    ~windowSizeSettings();
private slots:
    void on_cancelBtn_clicked();
    void on_setBtn_clicked();

private:
    Ui::windowSizeSettings *ui;
    templateWindow *window;
};

#endif // WINDOWSIZESETTINGS_H
