#ifndef EQFINDERSETTINGS_H
#define EQFINDERSETTINGS_H

#include <QWidget>

namespace Ui {
class EqFinderSettings;
}

class EqFinderSettings : public QWidget
{
    Q_OBJECT
public:
    explicit EqFinderSettings(QWidget *parent = 0);
    ~EqFinderSettings();
private:
    Ui::EqFinderSettings *ui;
};

#endif // EQFINDERSETTINGS_H
