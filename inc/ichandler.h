#ifndef ICHANDLER_H
#define ICHANDLER_H

#include "dsglobals.h"
#include "pilineedit.h"
namespace Ui {
class ICHandler;
}
class ICHandler : public QWidget
{
    Q_OBJECT

public:
    explicit ICHandler(const uint32_t &newDimension,
                       const std::vector<double> &newCoordinates,
                       const std::vector<QString> &newCoordinateIDs,
                       QWidget *parent = 0);
    ~ICHandler();
private slots:
    void on_zeroBtn_clicked();
    void on_fromFileBtn_clicked();
    void on_setBtn_clicked();
    void on_randBtn_clicked();
    void on_toFileBtn_clicked();
signals:
    void icChanged(std::vector<double> coords);
    void icFixed(bool);
private:
    Ui::ICHandler *ui;
    QWidget expandingWidget;
    QGridLayout mainLT;
    std::vector<QLabel *> phaseLabels;
    std::vector<piLineEdit *> forValues;
    void readSettings(void);
    void writeSettings(void);
};

#endif // ICHANDLER_H
