#ifndef ACTIONHANDLER_H
#define ACTIONHANDLER_H

#include <QObject>
#include "easyloggingpp.h"
class actionHandler : public QObject{
    Q_OBJECT
public:
    explicit actionHandler(QObject *parent = 0);
    ~actionHandler();
    void loadInitialData(void);
signals:
public slots:
};

#endif // ACTIONHANDLER_H
