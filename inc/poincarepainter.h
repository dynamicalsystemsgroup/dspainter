﻿#ifndef POINCAREPAINTER_H
#define POINCAREPAINTER_H

#include "dsglobals.h"
#include "painter.h"
//#include "secantoptions.h"
#include "commonfunctions.h"

class PoincarePainter: public Painter
{
Q_OBJECT
public:
    PoincarePainter (
            uint32_t newDimension,
            std::vector<QString> newCoordinateIDs,
            QWidget *parent = 0);
    virtual void draw(const std::vector<std::vector<double> > &buffer);
    void set_secant(const std::vector<double> &new_secant);
    void init_before_drawing(std::vector<double> coordinates);
    void set_periodic_index(int number);
private:
    //members
    std::vector<double> secant;
    std::vector<double> secant_plus;
    std::vector<double> secant_minus;
    int periodic_coordinate_number;
    SecantContainer secant_container;
    //methods
    void updateSecants(void);
    void draw();
};

#endif // POINCAREPAINTER_H
