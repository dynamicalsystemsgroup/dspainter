#ifndef SPATIALPAINTER_H
#define SPATIALPAINTER_H

#include "templatewindow.h"
class SpatialPainter : public TemplateWindow
{
public:
    SpatialPainter(uint32_t newDimension,
                   std::vector<QString> newCoordinateIDs,
                   QWidget *parent = 0);
    //Methods
    virtual void draw(const std::vector<std::vector<double> > &buffer);
    virtual void setSettings(const WindowSettings &set);
    void setTimeLag(const double &newTimeLag);
    double getTimeLag(void) const;
    void setStep(const double &newStep);
private:
    double timeLag;
    double step;
protected:
    void resizeEvent(QResizeEvent *event);
    virtual void initialize(void);
    virtual void doscalePrivate(void);
    virtual void updateScales(std::vector<double> scales);
public slots:
    virtual void doscale();
    virtual void updateLabels(void);
    virtual void updateMultipliers(void);
private slots:
    virtual void updateRange(void);
};

#endif // SPATIALPAINTER_H
