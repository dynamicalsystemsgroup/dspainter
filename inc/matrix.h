#ifndef _MATRIX_H_
#define _MATRIX_H_
/// Abstract
/// Data in memory organazed in column-wize order: a11,a21,...,an1,a12,a22,...,an2,...
#include <vector>
#include <stdint.h>
#include <algorithm>
#include <math.h>
#include <iostream>
#include "easyloggingpp.h"
#include "Eigen/Dense"
#include "Eigen/Eigenvalues"

namespace mathematics{
class matrix
{
public:
    matrix();
    matrix(uint32_t rowNumber, uint32_t columnNumber);
    matrix(const matrix &m);
private:
    std::vector<double> data;
    uint32_t rows;
    uint32_t columns;
public:
    uint32_t Rows();
    uint32_t Columns();
    double *getDoubleData();
    std::vector<double> getStdVectorData(void);
    double norm();
    double &operator()(uint32_t row, uint32_t column);
    matrix &operator=(matrix right);
    friend matrix operator*(matrix left, double right){
        for(uint32_t i = 0;i<left.data.size(); ++i){
            left.data[i] *= right;
        }
        return left;
    }
    friend matrix operator*(double left, matrix right){
        for(uint32_t i = 0;i<right.data.size(); ++i){
            right.data[i] *= left;
        }
        return right;
    }
    matrix transpose();
    matrix inverse();
    static matrix random(uint32_t row, uint32_t column, double rangeMin, double rangeMax);
    static matrix eye(uint32_t row, uint32_t column);
    friend std::ostream& operator<<(std::ostream& stream, matrix m){
        for(uint32_t i = 0; i<m.Rows(); ++i){
            for(uint32_t j = 0; j<m.Columns(); ++j){
                stream<<m(i,j)<<" ";
            }
            stream<<std::endl;
        }
        return stream;
    }
    friend matrix operator+(matrix left,matrix right){
        matrix retM(left.rows, left.columns);
        for(uint32_t i = 0; i < retM.rows;++i){
            for(uint32_t j = 0; j < retM.columns;++j){
                retM(i,j) = left(i,j) + right(i,j);
            }
        }
        return retM;
    }
    friend matrix operator-(matrix left,matrix right){
        matrix retM(left.rows, left.columns);
        for(uint32_t i = 0; i < retM.rows;++i){
            for(uint32_t j = 0; j < retM.columns;++j){
                retM(i,j) = left(i,j) - right(i,j);
            }
        }
        return retM;
    }
    friend matrix operator*(matrix left, matrix right){
        matrix retM(left.rows, right.columns);
        for(uint32_t i = 0; i<retM.rows;++i){
            for(uint32_t j = 0; j<retM.columns;++j){
                for(uint32_t k = 0; k<left.columns;++k){
                    retM(i,j) += left(i,k) * right(k,j);
                }
            }
        }
        return retM;
    }
};
class eigenUtilary{
public:
    eigenUtilary(){
        data = 0;
        number = 0;
    }
    eigenUtilary(double value, uint32_t num){
        data = value;
        number = num;
    }
    double data;
    uint32_t number;
};
// out-of-class functions
void eigenData(matrix input, matrix &eigenValuesRe,
               matrix &eigenValuesIm, matrix &eigenVectors);
void sortEigenDescending(matrix &valuesRe, matrix &valuesIm, matrix &vectors);
matrix solve(matrix A, matrix B);//Solves AX = B
}
#endif //_MATRIX_H_
