#ifndef WINDOWCONFIGURATION_H
#define WINDOWCONFIGURATION_H

#include <QWidget>

namespace Ui {
class windowConfiguration;
}

class windowConfiguration : public QWidget
{
    Q_OBJECT
public:
    explicit windowConfiguration(uint32_t maxWindows, QWidget *parent = 0);
    ~windowConfiguration();
private slots:
    void on_setBtn_clicked();
    void on_cancelBtn_clicked();
private:
    Ui::windowConfiguration *ui;
signals:
    void maxWindowsChanged(uint32_t);
};

#endif // WINDOWCONFIGURATION_H
