﻿#ifndef WAVEDRAWER_H
#define WAVEDRAWER_H

#include <QWidget>
#include <dynamicalsystem.h>

namespace Ui {
class WaveDrawer;
}

class WaveDrawer : public QWidget
{
    Q_OBJECT

public:
    explicit WaveDrawer(DynamicalSystem *DS, QWidget *parent = 0);
    ~WaveDrawer();
public slots:
    void draw(std::vector<std::vector<double>> trajectory);
private slots:
    void on_sizeSpinBox_valueChanged(int arg1);
    void on_closePushButton_clicked();
    void on_joinCheckBox_clicked(bool checked);

private:
    Ui::WaveDrawer *ui;
    DynamicalSystem *ds;
    void setup_plot();
};

#endif // WAVEDRAWER_H
