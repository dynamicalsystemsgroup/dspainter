#ifndef VISUALSETTINGS_H
#define VISUALSETTINGS_H

#include <QWidget>
#include "pilineedit.h"
#include "ui_visualSettingsForm.h"

class visualSettings : public QWidget
{
    Q_OBJECT
public:
    explicit visualSettings(QWidget *parent = 0);
    ~visualSettings();
    void setDimensionAndIDs(const uint32_t &newDimension, const std::vector<QString> &IDs);
    void setSettings(const WindowSettings &set);
protected:
    void keyPressEvent(QKeyEvent *keyEvent);
private:
    Ui::visualSettingsForm *ui;
    QList<QString> coordList;
    QList<QString> paramList;
    QList<QString> methodsList;
    piLineEdit *vMin;
    piLineEdit *vMax;
    piLineEdit *hMin;
    piLineEdit *hMax;
    piLineEdit *periodH;
    piLineEdit *periodV;
    piLineEdit *timeLag;
    QPixmap *pMap;
    WindowSettings settings;
    //functions
    void configSecondaryLayout(void);
    void configPrimaryLayout(void);
    void updateWidgets(void);
    void updateControlValues(void);
    void updateControlValuesPrivate(void);
    void configForSpecialWindow(WindowSettings set);
private slots:
    void on_selectBgColorBtn_clicked();
    void on_selectColorBtn_clicked();
    void bgColorChanged(QColor);
    void lineColorChanged(QColor);
    void changePhaseH(int);
    void changePhaseV(int);
    void changeProcH(QString);
    void changeProcV(QString);
    void changeHMin(double value);
    void changeHMax(double value);
    void changeVMin(double value);
    void changeVMax(double value);
    void changePeriodH(double value);
    void changePeriodV(double value);
    void changePointSize(int value);
    void changeLineWidth(int value);
    void changeJoinPoints(bool value);
    void changeTimeLag(double value);
    void on_renderDerivativesBox_toggled(bool checked);
    void on_fromBox_valueChanged(int arg1);
    void on_toBox_valueChanged(int arg1);
signals:
    void propertiesChanged(WindowSettings settings);
    void keyPressed(QKeyEvent *keyEvent);
};

#endif // VISUALSETTINGS_H
