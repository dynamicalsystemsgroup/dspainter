#ifndef EQFINDERSETWIDGET_H
#define EQFINDERSETWIDGET_H

#include <QWidget>

namespace Ui {
class EqFinderSetWidget;
}

class EqFinderSetWidget : public QWidget
{
    Q_OBJECT

public:
    explicit EqFinderSetWidget(QWidget *parent = 0);
    ~EqFinderSetWidget();

private:
    Ui::EqFinderSetWidget *ui;
};

#endif // EQFINDERSETWIDGET_H
