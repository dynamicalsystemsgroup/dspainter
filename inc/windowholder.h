#ifndef WINDOWHOLDER_H
#define WINDOWHOLDER_H

#include <QFrame>
#include <QMouseEvent>
#include <QPushButton>
#include <QGridLayout>
#include "painter.h"
#include "spatialpainter.h"
#include "poincarepainter.h"
#include "qcustomplot.h"
enum localWindowType{
    noType,
    flowDrawer,
    cascadeDrawer,
    spatialDrawer,
    poincareDiagram
};
class WindowHolder: public QFrame{
    Q_OBJECT
public:
    WindowHolder(uint32_t newDimension, std::vector<QString> newCoordinateIDs,
                 localWindowType type,QWidget *parent = 0);
    ~WindowHolder();
    QWidget *widget(void);
    QString WindowName(void);
    localWindowType WindowType(void);
    void configureGeometry(void);
private:
    QVBoxLayout layout;
    QPushButton phasePortraitButton;
    QPushButton spatialButton;
    QPushButton PoincareSectionButton;
    QWidget *interriorWidget;
    localWindowType windowType;
    uint32_t dimension;
    std::vector<QString> coordinateIDs;
    void configure(void);
    void clearLayout(void);
protected:
    void resizeEvent(QResizeEvent *event);
signals:
    void paintFinished(void);
public slots:
    void setAsPhasePortrait(void);
    void setAsPoincareSection(void);
    void setAsPoincareDiagram(void);
    void setAsSpatial(void);
    void paintFinishedSlot(void);
};

#endif // WINDOWHOLDER_H
