#ifndef GENERALSETTINGS
#define GENERALSETTINGS
#include <QObject>
#include <stdint-gcc.h>
/// This class meant to store general application settings:
/// 1) Integration step: step of RK4-5 method of numerical integration,
/// 2) Maximum integrating time: maximum arbitrary time range for integrator,
/// 3) Maximum coordinate value: max value of phase coordinates,
/// 4) Fixed coordinates flag: if coordinates are fixed then after every parameter shift
/// integration starts with these fixed values,
/// 5) Integration buffer size: for proper adjustment of visualisation
class GeneralSettings : QObject{
Q_OBJECT
public:
    GeneralSettings(QObject *parent = 0);
    GeneralSettings(double iS, double mT,
                    double mCV, bool fixed,
                    uint32_t intBufferSize, QObject *parent = 0);
    GeneralSettings(const GeneralSettings &settings);
    void setIntegrationStep(const double &integStep);
    void setMaximumTime(const double &maxTime);
    void setMaximumCoordinateValue(const double &maxCoordinate);
    void setUseFixedCoordinates(const bool &value);
    void setIntegratorBufferSize(const uint32_t &size);
    double getIntegrationStep(void) const;
    double getMaximumTime(void) const;
    double getMaximumCoordinateValue(void) const;
    bool getUseFixedCoordinates(void) const;
    uint32_t getIntegratorBufferSize(void) const;
    GeneralSettings& operator=(const GeneralSettings &right){
        this->integratingStep = right.integratingStep;
        this->maximumTime = right.maximumTime;
        this->maximumCoordinateValue = right.maximumCoordinateValue;
        this->useFixedCoordinates = right.useFixedCoordinates;
        this->integratorBufferSize = right.integratorBufferSize;
        return *this;
    }
private:
    double integratingStep;
    double maximumTime;
    double maximumCoordinateValue;
    bool useFixedCoordinates;
    uint32_t integratorBufferSize;
signals:
    void parametersChanged(void);
};
#endif // GENERALSETTINGS

