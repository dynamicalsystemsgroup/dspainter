#ifndef PILINEEDIT_H
#define PILINEEDIT_H

#include <QLineEdit>
#include <QKeyEvent>
#include "dsglobals.h"

class piLineEdit : public QLineEdit
{
    Q_OBJECT
public:
    explicit piLineEdit(QWidget *parent = 0);
    piLineEdit(double initValue,QWidget *parent = 0);
    void setValue(double);
    double getValue(void);
private:
    double expression;
    void procString(void);
protected:
    void keyPressEvent(QKeyEvent *);
signals:
    void newValue(double);
public slots:
    void editingFinishedSlot(void);

};

#endif // PILINEEDIT_H
