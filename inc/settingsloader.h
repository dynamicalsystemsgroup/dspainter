#ifndef SETTINGSLOADER_H
#define SETTINGSLOADER_H
#include "dsglobals.h"

class SettingsLoader
{
public:
    SettingsLoader();
    void loadPathSettings(void);
    void savePathSettings(void);
    QString getLibraryPath(void) const;
    QString getLibBuilderPath(void) const;
    void setLibraryPath(const QString &libPath);
    void setLibBuilderPath(const QString &libBuilderPath);
    void load(QString systemName);
    void save(const std::vector<double> &coord, const ParameterSettings &set,
              const std::vector<WindowSettings> &wind);
    std::vector<double> getCoordinates(void) const;
    std::vector<double> getParameters(void) const;
    std::vector<WindowSettings> getWindows(void) const;
    GeneralSettings getGeneralSettings(void) const;
    void setCoordinates(const std::vector<double> &newCoordinates);
    void setParameters(const std::vector<double> &newParameters/*, parameterSettings set*/);
    void setWindows(const std::vector<WindowSettings> &newWindows);
    void setGeneralSettings(const GeneralSettings &newGeneral);
    void loadGeneral(void);
    void saveGeneral(const GeneralSettings &generalSet);
    void setDSName(const QString &name);
    ParameterSettings getParameterSettings(void) const;
    void configureLoader(const uint32_t &newDimension, const uint32_t &newParameterDimension);
    void set_path_settings(const pathSettings& settings);
    pathSettings get_path_settings(void);
private:
    // Load methods
    void loadCoordinates(void);
    void loadParameters(void);
    void loadWindows(void);
    // Save methods
    void saveCoordinates(const std::vector<double> &coord);
    void saveParameters(const ParameterSettings &set);
    void saveWindows(const std::vector<WindowSettings> &wind);
    // Members
    QString applicationPath;
    QString dynamicalSystemName;
    std::vector<double> coordinates;
    std::vector<double> parameters;
    std::vector<WindowSettings> windows;
    GeneralSettings general;
    pathSettings pathSet;
    ParameterSettings parameterSet;
    uint32_t dimension;
    uint32_t parameterDimension;
};

#endif // SETTINGSLOADER_H
