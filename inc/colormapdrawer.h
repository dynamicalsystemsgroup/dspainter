﻿#ifndef COLORMAPDRAWER_H
#define COLORMAPDRAWER_H

#include <QWidget>
#include "qcustomplot.h"
#include "dynamicalsystem.h"
#include "easyloggingpp.h"
#include <QSettings>
#include <QFileDialog>
#include <math.h>

namespace Ui {
class ColormapDrawer;
}

class ColormapDrawer : public QWidget
{
    Q_OBJECT
public:
    explicit ColormapDrawer(DynamicalSystem *DS, QWidget *parent = 0);
    ~ColormapDrawer();

private slots:
    void on_closeButton_clicked();

    void on_resetScalePushButton_clicked();

    void on_calculatePushButton_clicked();

    void on_saveImagePushButton_clicked();

private:
    Ui::ColormapDrawer *ui;
    DynamicalSystem *ds;
    void setup_plot();
    void rescale_plot();
    void read_settings();
    void write_settings();
    void calculate_spatiotemporal_figure();

signals:

public slots:

};

#endif // COLORMAPDRAWER_H
