﻿#ifndef GRAPHICALAREA_H
#define GRAPHICALAREA_H

#include "dsglobals.h"
#include "windowholder.h"

namespace GraphicalAreaNS {

enum EditState{
    started,
    finished
};
enum EditStateResize{
    startedTop,
    startedBottom,
    startedLeft,
    startedRight,
    startedTopLeft,
    startedTopRight,
    startedBottomLeft,
    startedBottomRight,
    resizeFinished
};

#define MIN_WIDTH_LOCAL 200
#define MIN_HEIGHT_LOCAL 200
class GraphicalArea : public QWidget{
    Q_OBJECT
public:
    //constructor/destructor
    explicit GraphicalArea(QWidget *parent = 0);
    ~GraphicalArea();
    //methods
    QList<WindowHolder*> getSelectionList(void) const;
    QList<WindowHolder*> getWindowsList(void) const;
    void createWindow(const QPoint &pos, const QSize &size, localWindowType type);
    void createWindow(const WindowSettings &settings);
    void breakTrajectories(void);
    void init_before_drawing(std::vector<double> coordinates);
public slots:
    void setNewWindowMode(void);
    void setEditMode(void);
    void setLockMode(void);
    void alignTop(void);
    void alignLeft(void);
    void deleteSelected(void);
    void closeAllWindows(void);
    void collectFinishedSignals(void);
    void rescale(void);
    void reset(void);
    void dublicateSelectedWindow(void);
    void drawBuffer(std::vector<std::vector<double> > &buffer);
    void setDSProperties(uint32_t newDimension, std::vector<QString> newCoordinateIDs);
protected:
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void keyPressEvent(QKeyEvent *event);
    void paintEvent(QPaintEvent *);
private:
    //members
    graphicalMode mode;
    EditState drawingState;
    EditState relocatingState;
    EditStateResize resizeState;
    QRect currentRectangle;
    QList<WindowHolder*> listOfWindows;
    QList<WindowHolder*> selectionList;
    QPoint relocateStartPosition;
    QPoint resizeStartPosition;
    uint32_t dimensionForWindows;
    std::vector<QString> coordinateIDForWindows;
    //methods
signals:
    void graphicalModeChanged(GraphicalAreaNS::graphicalMode);
    void selectionChanged(void);
    void paintFinished(void);
    void keyPressed(QKeyEvent*);
};

}

#endif // GRAPHICALAREA_H
