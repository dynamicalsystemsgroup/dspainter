#ifndef SETPARAMETERSWIDGET_H
#define SETPARAMETERSWIDGET_H

#include "dsglobals.h"

namespace Ui {
class SetParametersWidget;
}
#define INTERWIDGET_SPACE 15
class SetParametersWidget : public QWidget
{
    Q_OBJECT

public:
    explicit SetParametersWidget(uint32_t parameterDimension,
                                 std::vector<double> parameters,
                                 std::vector<QString> parameterIDs,
                                 QWidget *parent = 0);
    ~SetParametersWidget();
    void setParameters(const std::vector<double> &newParameters);
private:
    Ui::SetParametersWidget *ui;
    std::vector<QLabel*> parameterNameLabel;
    std::vector<QDoubleSpinBox*> parameterValueBoxes;
    QWidget *expandingWidget;
    QGridLayout *layout;
private slots:
    void indicateParameterChange(double);
signals:
    void parametersChanged(std::vector<double> newParameters);
};

#endif // SETPARAMETERSWIDGET_H
