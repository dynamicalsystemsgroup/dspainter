#ifndef ARCHIVE_H
#define ARCHIVE_H

#include <QWidget>
#include <QFileDialog>

namespace Ui {
class Archive;
}

class Archive : public QWidget
{
    Q_OBJECT

public:
    explicit Archive(QWidget *parent = 0);
    ~Archive();

private slots:
    void on_cancelButton_clicked();

    void on_browseButton_clicked();

private:
    Ui::Archive *ui;
};

#endif // ARCHIVE_H
