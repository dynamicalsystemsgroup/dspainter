#ifndef EQFINDER_H
#define EQFINDER_H

#include "dsglobals.h"
#include "pilineedit.h"
#include "dynamicalsystem.h"
namespace Ui {
class EqFinder;
}
#define SPACING 10
#define MAX_HEIGHT 25
class EqFinder : public QWidget
{
    Q_OBJECT

public:
    explicit EqFinder(const uint32_t &newDimension,
                      const std::vector<double> &newCoordinates,
                      const std::vector<QString> &coordinateIDs,
                      DynamicalSystem *DSPointer,
                      QWidget *parent = 0);
    ~EqFinder();
public slots:
    void config();
    void parametersChanged(const std::vector<double> &newParameters);
private slots:
    void on_calcBtn_clicked();
    void paramChanged(void);
    void on_eigVectorBox_currentIndexChanged(int index);
    void on_invertBtn_clicked();
    void on_setBtn_clicked();
    void on_setFixedBtn_clicked();
    void on_addToListBtn_clicked();
    void on_deleteAllEntriesBtn_clicked();
    void on_entryNumberBox_currentIndexChanged(int value);
    void on_setDirectionBox_currentIndexChanged(const QString &arg1);
    void on_setMaxTimeBox_valueChanged(double);
    void on_setStepBox_valueChanged(double);
    void on_commentEdit_textChanged();
private:
    Ui::EqFinder *ui;
    DynamicalSystem *DS; // ����������� ����, �.�. ����� ������� (�� �� ������� ��������������� �������)
    uint32_t dimension;
    std::vector<QString> coordinateIDs;
    std::vector<double> coordinates;
    std::vector<double> parameters;
    std::vector<QLabel *> coordLabels;
    QWidget ICWidget;
    QWidget eigenWidget;
    QWidget eigenVWidget;
    std::vector<piLineEdit *> ICCoordBoxes;
    std::vector<QDoubleSpinBox *> eigenIm;
    std::vector<QDoubleSpinBox *> eigenRe;
    std::vector<QDoubleSpinBox *> eigenV;
    mathematics::matrix eigenVectorRight;
    QGridLayout ICLayout;
    QGridLayout eigenLayout;
    QGridLayout eigenVLayout;
    std::vector<specialTrajectoryEntry> setOfInitialConditions;
    //methods
    void readSettings(void);
    void writeSettings(void);
    void calculate(void);
    void setFixed(bool value);
    //Set of IC related
    QWidget coordinatesScrollAreaWidget;
    std::vector<piLineEdit *> coordinateList;
    std::vector<QLabel *> coordinateLabelList;
    QGridLayout coordinatesLayout;
    void configListOfEntriesControls(void);
    void clearListOfEntries(void);
    void reconfigEntries(void);
    void addNewEntry(specialTrajectoryEntry);
private slots:
    void coordinatesChanged(double);
    void on_deleteEntryBtn_clicked();
    void on_setEntriesBtn_clicked();
    void on_setSaveBtn_clicked();
    void on_setLoadBtn_clicked();
    void on_setCurrentEntry_clicked();
    void on_fixedBox_toggled(bool checked);
    void on_p1UpButton_clicked();
    void on_p1DownButton_clicked();
    void on_p2UpButton_clicked();
    void on_p2DownButton_clicked();
signals:
    void keyPressed(QKeyEvent *event);
    void newSingleCoordinates(specialTrajectoryEntry);
    void newSetOfCoordinates(std::vector<specialTrajectoryEntry>);
};

#endif // EQFINDER_H
