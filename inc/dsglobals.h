#ifndef GLOBLALS
#define GLOBLALS
#include <QDebug>
#include <QScrollArea>
#include <QMessageBox>
#include <stdint-gcc.h>
#include <QLibrary>
#include <QSettings>
#include <QFile>
#include <QTime>
#include <QDir>
#include <QColorDialog>
#include <QColor>
#include <QPixmap>
#include <QMenu>
#include <QTextStream>
#include <QAction>
#include <QFileDialog>
#include <QGridLayout>
#include <QTimer>
#include <QMenuBar>
#include <QThread>
#include <QDoubleSpinBox>
#include <QResizeEvent>
#include <QKeyEvent>
#include <QObject>
#include <QApplication>
#include <QPainter>
#include <QPen>
#include <QDesktopWidget>
#include "commonfunctions.h"
#include "matrix.h"
#include "types.h"
#include "easyloggingpp.h"

using stdFunctionMono = double (*)(double);
using stdFunctionDuo = double (*)(double,double);
using stdFunctionPeriod = double (*)(double,double,double);
#endif // GLOBLALS

