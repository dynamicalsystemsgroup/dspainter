#ifndef ABOUTSYSTEM_H
#define ABOUTSYSTEM_H

#include <QWidget>
#include <DSGlobals.h>
namespace Ui {
class aboutSystem;
}
class aboutSystem : public QWidget
{
    Q_OBJECT
public:
    explicit aboutSystem(QWidget *parent = 0);
    ~aboutSystem();
private:
    Ui::aboutSystem *ui;
    void config(void);
};

#endif // ABOUTSYSTEM_H
