﻿#ifndef CORE_H
#define CORE_H

// Summary about this module see in the end of the file ->

#include <QObject>
#include <QList>
#include <QProgressDialog>
#include "dspainter.h"
#include "keyboardhandler.h"
#include "graphicalarea.h"
#include "settingsloader.h"
#include "eqfinder.h"
#include "parameterwidget.h"
#include "collapsibledockwidget.h"
#include "windowSettingsWidget.h"
#include "ichandler.h"
#include "dynamicalsystem.h"
#include "setparameterswidget.h"
#include "archive.h"
#include "options.h"
#include "colormapdrawer.h"
#include "wavedrawer.h"
#include "commonfunctions.h"

/// This section enumerates all possible floating widgets
/// TODO: move this section to new header
enum FloatingWidgets{
    NO_WIDGET,
    IC_WIDGET,// this related to initial conditions widget
    EQ_WIDGET,// this related to equillibrium finder widget
    PARAM_WIDGET,// this related to set parameters widget
    OPTIONS_WIDGET,
    ABOUT_SYSTEM_WIDGET
};

class FloatingWidgetsContainer
{

public:
    FloatingWidgetsContainer(){
        widget = 0;
        widgetType = NO_WIDGET;
    }
    FloatingWidgetsContainer(FloatingWidgets newWidgetType, QWidget *newWidget){
        if(newWidget == nullptr) LOG(WARNING)<<"FloatingWidgetsContainer: constructing null pointer widget.";
        if(newWidgetType == NO_WIDGET) LOG(WARNING)<<"FloatingWidgetsContainer: constructing no type widget.";
        widget = newWidget;
        widgetType = newWidgetType;
    }
    friend bool operator==(const FloatingWidgetsContainer& right,
                           const FloatingWidgetsContainer& left){
        if(right.widgetType == left.widgetType) return true;
        else return false;
    }
    QWidget *getWidget() const{
        return widget;
    }
    FloatingWidgets getType() const{
        return widgetType;
    }
private:
    QWidget *widget;
    FloatingWidgets widgetType;
};
///

class Core : public QObject{
    Q_OBJECT
public:
    explicit Core(QObject *parent = 0);
    ~Core();
    void InitializeCore(void);
private:
    //members
    DSPainter *mainWindow;
    KeyboardHandler *keyboard;
    GraphicalAreaNS::GraphicalArea *graphicalArea;
    DynamicalSystem *DS;
    SettingsLoader *loader;
    EqFinder *equllibriumFinder;
    GeneralSettings localGeneralSettings;
    ParameterWidget *parameterWidget;
    CollapsibleDockWidget *windowSettingsDock;
    WindowSettingsWidget *windowSettingsWidget;
    ICHandler *initialConditionsHandler;
    SetParametersWidget *setParametersWidget;
    QList<FloatingWidgetsContainer> openedWidgets;
    QTimer autoSaveTimer;
    Archive *archiveWidget;
    Options *optionsWidget;
    //methods
    void saveConfiguration(void);
    void initMainWindow(void);
    void initKeyboardHandler(void);
    void initGraphicalArea(void);
    void initDynamicalSystem(void);
    void initSettingsLoader(void);
    void initParameterSettings(void);
    void initParameterWidget(void);
    void initWindowSettingsDock(void);
    void makeConnections(void);
    void closeOpenedWidgets(void);
    void showSetParameterWidget(void);
    void handleMultipleInitialConditions(std::vector<specialTrajectoryEntry> &newSet);
private slots:
    void showEqullibriumFinder(void);
    void showICHandler(void);
    void showOptionsWidget(void);
signals:
};

#endif // CORE_H

/// Class Core is the central class of the application. It constructs main components and
/// provide proper interconnection between modules by dispatching the signals. Core is
/// inhereted from QObject and thus can send and receive signals by itself.
///
///                 Members of Core:
///
/// DSPainter *mainWindow - pointer to a QMainWindow derived class, that represets the
/// main window of application with all necessary control widgets. Detailed description
/// see in DSPainter.h
///
/// KeyboardHandler *keyboard - pointer to a class that handles keyboard events. All internal
/// widgets are supposed to send signals when catch any keyboard button event, KeyboardHandler
/// instance accumulates all theese events and dispatches them to Core.
///
/// GraphicalAreaNS::GraphicalArea *graphicalArea - central part of the main window where
/// almost all the graphics takes place. The main function of graphical area is to handle
/// windows in form of WindowHolder widgets, reshape them, move them around, create\destroy
/// windows and align windows.
///
/// DynamicalSystem *DS - this class provides interface to communicate with dynamical system object.
/// DynamicalSystem class dials with the loading new libraries, holding coordinates and parameters
/// data, integrating equations e.t.c. Notice, that this class is inhereted from the QThread
/// class and virtual function run() is reimplemented to make steps using numerical
/// integrating algorythm. So DS actually performs maximum of the calculations inside the thread
/// different to graphical thread.
///
/// SettingsLoader *loader - this class provides interface to save\load application
/// settings at shutdown\startup of the app. Settings to be strored: windows from previous
/// working session, integrator settings, coordinates and parameters, paths and so on.
///
/// CollapsibleDockWidget *windowSettingsDock - this widget is a modification of Qt`s
/// QDockWidget and could be collapsed and reopened by mouse events: enter and leave.
/// Inside this widget we keep widgets that control windows in graphical area.
///
/// ICHandler *initialConditionsHandler - this widget provides the interface to set, save
/// and load initial conditions of the current dynamical system.
///
/// uint32_t openedWidgets - dummy way to check some control widgets are opened.
///
///                 Member functions of core.
///
/// All functions with prefix init- just create and initialize objects.
/// Function void makeConnections(void) is meant to dispatch signals from objects to proper slots.
