
QT       += core printsupport # note that gui module is included by default
greaterThan(QT_MAJOR_VERSION , 4){
    QT += widgets
}
TARGET = DSPainter
TEMPLATE = app
QMAKE_CXXFLAGS += -std=c++11

MOC_DIR = moc/
OBJECTS_DIR = obj/
UI_DIR = ui_generated/

INCLUDEPATH += eigen/
INCLUDEPATH += inc/
INCLUDEPATH += qcustomplot/
INCLUDEPATH += inc/

RC_FILE = icon.rc

SOURCES += src/collapsibledockwidget.cpp \
    src/commonfunctions.cpp \
    src/dynamicalsystem.cpp \
    src/eqfinder.cpp \
    src/eqfindersettings.cpp \
    src/eqfindersetwidget.cpp \
    src/generalsettings.cpp \
    src/graphicalarea.cpp \
    src/keyboardhandler.cpp \
    src/matrix.cpp \
    src/painter.cpp \
    src/parameterwidget.cpp \
    src/pilineedit.cpp \
    src/settingsloader.cpp \
    src/templatewindow.cpp \
    src/windowholder.cpp \
    src/windowSettingsWidget.cpp \
    src/ichandler.cpp \
    src/secantoptions.cpp \
    src/setparameterswidget.cpp \
    src/spatialpainter.cpp \
    src/visualsettings.cpp \
    src/windowsettings.cpp \
    src/main.cpp \
    src/core.cpp \
    src/poincarepainter.cpp \
    src/dspainter.cpp \
    src/options.cpp \
    src/archive.cpp \
    qcustomplot/qcustomplot.cpp \
    src/colormapdrawer.cpp \
    src/wavedrawer.cpp

HEADERS  += inc/dspainter.h \
    inc/DSGlobals.h \
    inc/easyloggingpp.h \
    inc/core.h \
    inc/keyboardhandler.h \
    inc/graphicalarea.h \
    inc/windowholder.h \
    inc/types.h \
    inc/templatewindow.h \
    inc/painter.h \
    inc/commonfunctions.h \
    inc/dynamicalsystem.h \
    inc/settingsloader.h \
    inc/eqfinder.h \
    inc/eqfindersetwidget.h \
    inc/eqfindersettings.h \
    inc/matrix.h \
    inc/pilineedit.h \
    inc/parameterwidget.h \
    inc/generalsettings.h \
    inc/collapsibledockwidget.h \
    inc/windowSettingsWidget.h \
    inc/visualsettings.h \
    inc/secantoptions.h \
    inc/windowsettings.h \
    inc/spatialpainter.h \
    inc/poincarepainter.h \
    inc/ichandler.h \
    inc/setparameterswidget.h \
    inc/options.h \
    inc/archive.h \
    qcustomplot/qcustomplot.h \
    inc/colormapdrawer.h \
    inc/wavedrawer.h

FORMS    += \
    ui/dspainter.ui \
    ui/templatewindow.ui \
    ui/eqfinder.ui \
    ui/eqfindersetwidget.ui \
    ui/eqfindersettings.ui \
    ui/parameterwidget.ui \
    ui/visualSettingsForm.ui \
    ui/secantoptions.ui \
    ui/ichandler.ui \
    ui/setparameterswidget.ui \
    ui/options.ui \
    ui/archive.ui \
    ui/colormapdrawer.ui \
    ui/wavedrawer.ui

#RESOURCES += \
#    resource.qrc

DISTFILES += \
    styles/default.qss
